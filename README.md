# Calculat'IF #

Équipe de projet : __Hexanôme 4213__

Timothée DURAND, Pascal CHIU,  Danh Lap NGUYEN, Cyril POTTIEZ, Lucas POISSE, Manuel AMOUROUX, Julien EMMANUEL

## Description du compilateur développé ##

### Fonctionnalités implémentées ###

Toutes les fonction prévues par le sujet ont été implémentées, à l'exception de la liste suivante, essentiellement par manque de temps : 
* Boucles for
* Déclarations multiples sur une ligne (par exemple `char a, b, c;`)
* Certains opérateurs binaires comme les décalages de bits (`<<` et `>>`)
* Les opérations avec affectation immédiate (comme `+=`, `-=`, etc.)

Parmi les fonctionnalités implémentées, nous avons toujours quelques limitations, dont voici la liste :
* Les XOR bit à bit s'écrit `var1 $ var2` et pas `var1 ^ var2` comme le prévoit le C. En effet dans ce cas Antlr ne reconnait pas la règle pour une raison qui nous échappe
* L'optimiseur que nous avons tenté de réaliser à la toute fin du projet souffre de nombreux problèmes. En particulier dans de nombreux cas il occasionne des problèmes dans l'assembleur généré. La partie `Utilisation > Exécution` spécifie comment l'activer ou le désactiver. 

Nous avons également implémenté certaines fonctionnalités particulières, qui ne sont pas nécessairement prévues par le sujet, comme la génération d'assembleur pour Windows ou Linux (en fonction des variables d'environnement). Cela a été nécessaire pour que chacun puisse développer sur le système avec lequel il est le plus à l'aise.

Parmis les fonctionnalités les plus avancées que propose notre compilateur, nous pouvons noter :
* Les variables globales pleinement fonctionnelles (variables simples et tableaux)
* Le passage de tableau en paramètre des fonctions (même quand la fonction a plus de 6 paramètres)
* Notre vérification de porté et notre analyse statique permettent de vérifier les règles suivantes :
    * Déclaration multiple des variables (_Erreur_)
    * Paramètre de même nom pour une même fonction (_Erreur_)
    * Variables utilisées sans avoir été déclarées (_Erreur_)
    * Variables déclarées mais pas utilisées (_Warning_)
    * Variables déclarées mais utilisées avant d'avoir été initialisées (_Warning_)

## Utilisation ##

### Pré-requis pour compiler le compilateur ###

cmake, make, g++, pkg-config, uuid-de, et peut être d'autres que j'oublie, cmake les réclamera si c'est le cas

Toutes les commandes sont à lancer depuis la racine du projet. Pour un aperçu des commandes taper `make` ou `make help`

### Antlr ###

Pour générer la grammaire, faire :

```bash
make antlr
```

Le code est généré dans le dossier ```src/```

### Cmake ###

Pour compiler le compilateur, lancer

```bash
make cmake
```

Le binaire généré est ```bin/calculatif```

Pour lancer antlr et cmake en une commande, faire 

```bash
make build
```

### Exécution ###

Notre programme accepte un certain nombre d'options, dont voici le détail :

```bash
./bin/compilatif [-t] [-a] [-g] [-c] [-v] [-o] [<input>]
```

Avec :
* `-t` : Réalise la suite de tests du Frontend (utilisé en intégration continue sur GitLab) __/!\\ Si cette option est activée seule la suite de tests est lancée, les autres options ne sont pas prises en compte__ 
* `-a` : Réalise l'analyse statique du programme d'entrée
* `-g` : Génère le graphe correspondant à l'AST du programme d'entrée, dans AST.pdf
* `-c` : Génère le fichier assembleur compilé à partir du programme d'entrée, de même nom que le fichier d'entrée mais avec l'extension `.s`
* `-v` : Quand cette option est activée, le compilateur affiche certaines opérations intermédiaires et structures de données dans la console, et l'assembleur comporte des commentaires et des sauts de lignes pour améliorer la lisibilité
* `-o` : Réalise l'optimisation du programme __/!\\ L'optimisation ne fonctionne que sur très peu de programmes, selon des règles qui nous échappent, à utiliser à vos risques et périls__ 
* `<input>` : Le fichier C d'entrée, la valeur par défaut est `testInput.c`

### Assemblage / Link avec gcc ###

Pour assembler et linker un fichier `testInput.s` généré par notre compilateur, il suffit de lancer la commande 

```bash
make exec && ./a.out ; echo "  → Code retour : " $?
```

Cette commande lance notre compilateur sur le fichier d'entrée `testInput.c`, assemble et link l'assembleur généré, lance l'exécutable final et affiche le code de retour du main

En remplaçant `make exec` par `make exec_o` on a la même chose avec l'optimisation (toujours à vos risques et périls), et l'option `-v`