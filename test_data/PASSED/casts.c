#include <stdio.h>
#include <inttypes.h>

int32_t main() { 
	char a1 = 'a';
    int32_t a2 = (int32_t)a1;
    int64_t a3 = (int64_t)a2;
    putchar(a1);
    putchar(a2);
    putchar(a3);
    putchar('\n');

    int32_t b1 = 'b';
    char b2 = (char)'b';
    int64_t b3 = (int64_t)b2;
    putchar(b1);
    putchar(b2);
    putchar(b3);
    putchar('\n');

    int64_t c1 = 'c';
    char c2 = (char)c1;
    int32_t c3 = (int32_t)c2;
    putchar(c1);
    putchar(c2);
    putchar(c3);
    putchar('\n');

    return 0;
}
