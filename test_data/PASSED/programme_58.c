#include <stdio.h>
#include <inttypes.h>
int32_t main() {
	int32_t n = 5;
	int32_t res = multiplyNumbers(n);
	return res;
}
int64_t multiplyNumbers(int32_t n)
{
	if (n >= 1)
		return n*multiplyNumbers(n-1);
	else
		return 1;
}