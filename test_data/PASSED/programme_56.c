#include <stdio.h>
#include <inttypes.h>

//NOTA: programme testé en essayant plusieurs tests,
//		dont des tests avec des variables et des entiers négatifs		

void main() {
	if (1<2) {
		putchar('V');
	}
	else {
		putchar('F');
	}
	putchar('\n');
}