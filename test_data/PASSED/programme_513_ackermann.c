#include <stdio.h>
#include <inttypes.h>

int32_t ackermann(int32_t m, int32_t n) {
	int32_t res = -1;
	if(m == 0) {
		res = n + 1;
	}
	else {
		if(n == 0) {
			res = ackermann(m-1, 1);
		}
		else {
			res = ackermann(m-1, ackermann(m, n-1));
		}
	}
	return res;
}
int32_t main() {
	int32_t a = 3;
	int32_t b = 0;

	int32_t resultat = ackermann(a,b);

	return resultat;
}