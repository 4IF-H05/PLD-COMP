#include <stdio.h>
#include <inttypes.h>

void displayArr(char arr[], int32_t n)
{
	int32_t i = 0;
    while(i < n)
        putchar(arr[i++]);
}

void bubbleSort(char arr[], int32_t n)
{
    int32_t i = 0;
    int32_t j = 0;
    char temp; 
    while(i<n-1) {
        while(j<n-i-1) {
            if (arr[j] > arr[j+1]) {
                temp = arr[j];
                arr[j] = arr[j+1];
                arr[j+1] = temp;
            }
            j++;
        }
        i++;
    }
}

int32_t main() { 
	char arr[] = {'z','m','o','p'};
	displayArr(arr, 4);
	putchar('\n');
    bubbleSort(arr, 4);
	displayArr(arr, 4);
	putchar('\n');
    return 0;
}
