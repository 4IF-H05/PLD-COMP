#include <stdio.h>
#include <inttypes.h>

void displayArr(char arr[], int32_t n)
{
	int32_t i = 0;
    while(i < n)
        putchar(arr[i++]);
}

int32_t main() {
    char tab[] = { 'P', 'L', 'D', ' ', 'C', 'O', 'M', 'P' };
	displayArr(tab, 8);	// sizeof(tab) / sizeof(tab[0])
	putchar('\n');
    return 0;
}
// -o ERREUR DE TYPE