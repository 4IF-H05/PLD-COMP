#include <stdio.h>
#include <inttypes.h>

int32_t syracuse(int32_t a) {
	if (a%2 == 0) {
		a = a/2;
	}
	else {
		a = a*3+1;
	}
	return a;
}

int32_t main() {
	int32_t res = 7;
	
	putchar(res + 48);
	while (res != 1) {
		res = syracuse(res);
		putchar(res+48);
	}
	putchar('\n');
	return 1;
}
// -o ERREUR DE TYPE