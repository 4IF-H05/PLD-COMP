#include <stdio.h>
#include <inttypes.h>

/* Explication :
		Décompte à partir du chiffre entré
*/
int32_t main() {
	char a = getchar();
	while(a >= '0') {
		putchar(a);
		if(a-- != '0')
			putchar(',');
		putchar(' ');
	}
	putchar('\n');
    return 0;
}
// -o ERREUR DE TYPE