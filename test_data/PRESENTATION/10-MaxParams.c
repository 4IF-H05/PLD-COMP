#include <stdio.h>
#include <inttypes.h>

// 12 PARAMETERS
int32_t test(int32_t a, int32_t b, int32_t c, int32_t d, int32_t e, int32_t f, int32_t g, int32_t h, int32_t i, int32_t j, int32_t k, int32_t l)
{
	return a+b+c+d+e+f+g+h+i+j+k+l;
}

int32_t main()
{
    return test(1,1,1,1,1,1,1,1,1,1,1,1);
}	
// -o OK