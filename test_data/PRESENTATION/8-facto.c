#include <stdio.h>
#include <inttypes.h>


int32_t facto(int32_t n) {
    if(n == 0) {
        return 1;
    }
    return facto(n-1)*n;
}

int32_t main() {
    return facto(4);
}
// -o OK