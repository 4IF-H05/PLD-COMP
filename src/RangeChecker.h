#pragma once

#include "StructureDonnees/Programme.h"
#include <map>
#include <list>

using namespace std;

class RangeChecker
{
public:
	// DESCRIPTION:
	// performs variable scope checking and create links between used variables and their declarations
	// detect various errors such as:
	// - two variables / functions declared with the same name
	// - two function parameters with the same name
	// - variable used but never declared
	//
	// RETURNS:
	// a list of strings describing the errors, if any
	list<string> programRangeChecking (Programme &programme);

	// DESCRIPTION:
	// performs static analysis of the programm, to detect errors such as :
	// - variable declared but never used
	// - variable used but never initialized
	// - unused loops while(0)					NOT IMPLEMENTED
	// - infinite loops while(!0)				NOT IMPLEMENTED
	// - division by 0							NOT IMPLEMENTED
	//
	// Attention: this function must be called after the function programRangeChecking
	// otherwise, the data structures contained in programme won't be initialised correctly
	//
	// RETURNS:
	// a list of strings describing the warnings, if any
	list<string> programStaticAnalysis (Programme &programme);

private:
	list<string> anticipatedWarnings;
};