#!/bin/bash

echo "==== CMake ====="
echo ""
cmake -G "Unix Makefiles" -Wno-dev -DCMAKE_BUILD_TYPE=Debug
echo ""
echo "==== Make ====="
echo ""
make