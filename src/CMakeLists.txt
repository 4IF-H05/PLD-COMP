cmake_minimum_required(VERSION 2.8 FATAL_ERROR)

set(EXECUTABLE_OUTPUT_PATH ../bin/)
set(CMAKE_CXX_STANDARD 11)

project(compilatif)

add_subdirectory(../antlr/runtime_src ../antlr/runtime_src EXCLUDE_FROM_ALL)

# get all *.cpp files
file(GLOB SRC_FILES ${PROJECT_SOURCE_DIR}/*.cpp ${PROJECT_SOURCE_DIR}/StructureDonnees/*.cpp ${PROJECT_SOURCE_DIR}/Backend/*.cpp)
file(GLOB HEADER_FILES ${PROJECT_SOURCE_DIR}/*.h ${PROJECT_SOURCE_DIR}/StructureDonnees/*.h ${PROJECT_SOURCE_DIR}/Backend/*.h)

add_executable(compilatif ${SRC_FILES} ${HEADER_FILES})

if (MSVC_VERSION)
	# Needed to have the good directory tree in the Visual studio project
	foreach(source IN LISTS SRC_FILES)
		get_filename_component(source_path "${source}" PATH)
		file(RELATIVE_PATH source_path_rel "${CMAKE_CURRENT_SOURCE_DIR}" "${source_path}")
		string(REPLACE "/" "\\" source_path_msvc "${source_path_rel}")
		source_group("${source_path_msvc}" FILES "${source}")
	endforeach()
	
	foreach(header IN LISTS HEADER_FILES)
		get_filename_component(header_path "${header}" PATH)
		file(RELATIVE_PATH header_path_rel "${CMAKE_CURRENT_SOURCE_DIR}" "${header_path}")
		string(REPLACE "/" "\\" header_path_msvc "${header_path_rel}")
		source_group("${header_path_msvc}" FILES "${header}")
	endforeach()
	
	# COPY DLL TO EXECUTABLE DIRECTORY
	
#	add_custom_command(TARGET compilatif POST_BUILD
 #   COMMAND ${CMAKE_COMMAND} -E copy_if_different  
  #      "${PROJECT_SOURCE_DIR}/dist/$<CONFIGURATION>/antlr4-runtime.dll"
   #     ${EXECUTABLE_OUTPUT_PATH})
		
	# COPY TEST FILE TO EXECUTABLE DIRECTORY
		
#	add_custom_command(TARGET compilatif POST_BUILD
   # COMMAND ${CMAKE_COMMAND} -E copy_if_different  
    #    "${PROJECT_SOURCE_DIR}/../testInput.c"     
      #  ${EXECUTABLE_OUTPUT_PATH})         

	set_target_properties(compilatif PROPERTIES VS_DEBUGGER_WORKING_DIRECTORY "${EXECUTABLE_OUTPUT_PATH}")
	
	if (MSVC_VERSION GREATER_EQUAL "1900")
		include(CheckCXXCompilerFlag)
		CHECK_CXX_COMPILER_FLAG("/std:c++17" _cpp_latest_flag_supported)
		if (_cpp_latest_flag_supported)
			add_compile_options("/std:c++17")
		endif()
	endif()	
endif()

target_include_directories(compilatif PRIVATE ../antlr/runtime_src/runtime/src)
target_link_libraries(compilatif antlr4_shared)
