
// Generated from Grammaire.g4 by ANTLR 4.7.1

#pragma once


#include "antlr4-runtime.h"
#include "GrammaireParser.h"



/**
 * This class defines an abstract visitor for a parse tree
 * produced by GrammaireParser.
 */
class  GrammaireVisitor : public antlr4::tree::AbstractParseTreeVisitor {
public:

  /**
   * Visit parse trees produced by GrammaireParser.
   */
    virtual antlrcpp::Any visitProgc(GrammaireParser::ProgcContext *context) = 0;

    virtual antlrcpp::Any visitProgDecl(GrammaireParser::ProgDeclContext *context) = 0;

    virtual antlrcpp::Any visitProgFunc(GrammaireParser::ProgFuncContext *context) = 0;

    virtual antlrcpp::Any visitProgEOF(GrammaireParser::ProgEOFContext *context) = 0;

    virtual antlrcpp::Any visitParamV(GrammaireParser::ParamVContext *context) = 0;

    virtual antlrcpp::Any visitParamTab(GrammaireParser::ParamTabContext *context) = 0;

    virtual antlrcpp::Any visitParamSimple(GrammaireParser::ParamSimpleContext *context) = 0;

    virtual antlrcpp::Any visitParamVide(GrammaireParser::ParamVideContext *context) = 0;

    virtual antlrcpp::Any visitFonction(GrammaireParser::FonctionContext *context) = 0;

    virtual antlrcpp::Any visitTypeRetType(GrammaireParser::TypeRetTypeContext *context) = 0;

    virtual antlrcpp::Any visitTypeRetVoid(GrammaireParser::TypeRetVoidContext *context) = 0;

    virtual antlrcpp::Any visitParamAppelV(GrammaireParser::ParamAppelVContext *context) = 0;

    virtual antlrcpp::Any visitParamAppel(GrammaireParser::ParamAppelContext *context) = 0;

    virtual antlrcpp::Any visitAppelFonction(GrammaireParser::AppelFonctionContext *context) = 0;

    virtual antlrcpp::Any visitLvalSimple(GrammaireParser::LvalSimpleContext *context) = 0;

    virtual antlrcpp::Any visitLvalTab(GrammaireParser::LvalTabContext *context) = 0;

    virtual antlrcpp::Any visitElementTabV(GrammaireParser::ElementTabVContext *context) = 0;

    virtual antlrcpp::Any visitElementsTab(GrammaireParser::ElementsTabContext *context) = 0;

    virtual antlrcpp::Any visitDeclDef(GrammaireParser::DeclDefContext *context) = 0;

    virtual antlrcpp::Any visitDeclLval(GrammaireParser::DeclLvalContext *context) = 0;

    virtual antlrcpp::Any visitDeclTabDef(GrammaireParser::DeclTabDefContext *context) = 0;

    virtual antlrcpp::Any visitDef(GrammaireParser::DefContext *context) = 0;

    virtual antlrcpp::Any visitInstrReturn(GrammaireParser::InstrReturnContext *context) = 0;

    virtual antlrcpp::Any visitInstrExp(GrammaireParser::InstrExpContext *context) = 0;

    virtual antlrcpp::Any visitInstrDecl(GrammaireParser::InstrDeclContext *context) = 0;

    virtual antlrcpp::Any visitInstrControle(GrammaireParser::InstrControleContext *context) = 0;

    virtual antlrcpp::Any visitExprOrBit(GrammaireParser::ExprOrBitContext *context) = 0;

    virtual antlrcpp::Any visitExprLt(GrammaireParser::ExprLtContext *context) = 0;

    virtual antlrcpp::Any visitExprLdec(GrammaireParser::ExprLdecContext *context) = 0;

    virtual antlrcpp::Any visitExprAnd(GrammaireParser::ExprAndContext *context) = 0;

    virtual antlrcpp::Any visitExprVar(GrammaireParser::ExprVarContext *context) = 0;

    virtual antlrcpp::Any visitExprNegBin(GrammaireParser::ExprNegBinContext *context) = 0;

    virtual antlrcpp::Any visitExprFunc(GrammaireParser::ExprFuncContext *context) = 0;

    virtual antlrcpp::Any visitExprMultDiv(GrammaireParser::ExprMultDivContext *context) = 0;

    virtual antlrcpp::Any visitExprDecR(GrammaireParser::ExprDecRContext *context) = 0;

    virtual antlrcpp::Any visitExprLeq(GrammaireParser::ExprLeqContext *context) = 0;

    virtual antlrcpp::Any visitExprNeg(GrammaireParser::ExprNegContext *context) = 0;

    virtual antlrcpp::Any visitExprLinc(GrammaireParser::ExprLincContext *context) = 0;

    virtual antlrcpp::Any visitExprEq(GrammaireParser::ExprEqContext *context) = 0;

    virtual antlrcpp::Any visitExprNeq(GrammaireParser::ExprNeqContext *context) = 0;

    virtual antlrcpp::Any visitExprChar(GrammaireParser::ExprCharContext *context) = 0;

    virtual antlrcpp::Any visitExprConst(GrammaireParser::ExprConstContext *context) = 0;

    virtual antlrcpp::Any visitExprDecL(GrammaireParser::ExprDecLContext *context) = 0;

    virtual antlrcpp::Any visitExprXorBit(GrammaireParser::ExprXorBitContext *context) = 0;

    virtual antlrcpp::Any visitExprGt(GrammaireParser::ExprGtContext *context) = 0;

    virtual antlrcpp::Any visitExprInc(GrammaireParser::ExprIncContext *context) = 0;

    virtual antlrcpp::Any visitExprSizeE(GrammaireParser::ExprSizeEContext *context) = 0;

    virtual antlrcpp::Any visitExprPar(GrammaireParser::ExprParContext *context) = 0;

    virtual antlrcpp::Any visitExprGeq(GrammaireParser::ExprGeqContext *context) = 0;

    virtual antlrcpp::Any visitExprCast(GrammaireParser::ExprCastContext *context) = 0;

    virtual antlrcpp::Any visitExprOr(GrammaireParser::ExprOrContext *context) = 0;

    virtual antlrcpp::Any visitExprAndBit(GrammaireParser::ExprAndBitContext *context) = 0;

    virtual antlrcpp::Any visitExprDec(GrammaireParser::ExprDecContext *context) = 0;

    virtual antlrcpp::Any visitExprTer(GrammaireParser::ExprTerContext *context) = 0;

    virtual antlrcpp::Any visitExprDef(GrammaireParser::ExprDefContext *context) = 0;

    virtual antlrcpp::Any visitExprAddMoins(GrammaireParser::ExprAddMoinsContext *context) = 0;

    virtual antlrcpp::Any visitExprMod(GrammaireParser::ExprModContext *context) = 0;

    virtual antlrcpp::Any visitExprSizeT(GrammaireParser::ExprSizeTContext *context) = 0;

    virtual antlrcpp::Any visitExprNon(GrammaireParser::ExprNonContext *context) = 0;

    virtual antlrcpp::Any visitControleWhile(GrammaireParser::ControleWhileContext *context) = 0;

    virtual antlrcpp::Any visitControleIfElse(GrammaireParser::ControleIfElseContext *context) = 0;

    virtual antlrcpp::Any visitControleIf(GrammaireParser::ControleIfContext *context) = 0;

    virtual antlrcpp::Any visitBloc(GrammaireParser::BlocContext *context) = 0;

    virtual antlrcpp::Any visitContenuBloc(GrammaireParser::ContenuBlocContext *context) = 0;


};

