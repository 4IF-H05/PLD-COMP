#pragma once

#include <vector>
#include "StructureDonnees/Types.h"
#include "StructureDonnees/Expression.h"
#include "StructureDonnees/Operateurs.h"
#include "GrammaireBaseVisitor.h"

using namespace antlrcpp;

class MainVisitor : public GrammaireBaseVisitor {
public:
    // Prog
    Any visitProgc(GrammaireParser::ProgcContext *ctx) override;
    Any visitProgDecl(GrammaireParser::ProgDeclContext *ctx) override;
    Any visitProgFunc(GrammaireParser::ProgFuncContext *ctx) override;
    Any visitProgEOF(GrammaireParser::ProgEOFContext *ctx) override;

    // Fonction
    Any visitFonction(GrammaireParser::FonctionContext *ctx) override;
    Any visitTypeRetType(GrammaireParser::TypeRetTypeContext *ctx) override;
    Any visitTypeRetVoid(GrammaireParser::TypeRetVoidContext *ctx) override;

    // Parametres
    Any visitParamV(GrammaireParser::ParamVContext *ctx) override;
    Any visitParamSimple(GrammaireParser::ParamSimpleContext *ctx) override;
    Any visitParamTab(GrammaireParser::ParamTabContext *ctx) override;
    Any visitParamVide(GrammaireParser::ParamVideContext *ctx) override;

	// Blocs
	Any visitBloc(GrammaireParser::BlocContext *ctx) override;
	Any visitContenuBloc(GrammaireParser::ContenuBlocContext *ctx) override;

	// Instructions
	Any visitInstrReturn(GrammaireParser::InstrReturnContext *ctx) override;
	Any visitInstrExp(GrammaireParser::InstrExpContext *ctx) override;
	Any visitInstrDecl(GrammaireParser::InstrDeclContext *ctx) override;
	Any visitInstrControle(GrammaireParser::InstrControleContext *ctx) override;


	//Contrôles
	Any visitControleWhile(GrammaireParser::ControleWhileContext *ctx) override;
	Any visitControleIf(GrammaireParser::ControleIfContext *ctx) override;
	Any visitControleIfElse(GrammaireParser::ControleIfElseContext *ctx) override;

	//Appel de fonction
	Any visitParamAppelV (GrammaireParser::ParamAppelVContext *ctx) override;
	Any visitParamAppel (GrammaireParser::ParamAppelContext *ctx) override;
	Any visitAppelFonction (GrammaireParser::AppelFonctionContext *ctx) override;
	
	//définition
	Any visitDef (GrammaireParser::DefContext *ctx) override;

	//Expressions
	Any visitExprFunc (GrammaireParser::ExprFuncContext *ctx) override;
	Any visitExprDef (GrammaireParser::ExprDefContext *ctx) override;
	Any visitExprVar(GrammaireParser::ExprVarContext *ctx) override;
	Any visitExprAddMoins(GrammaireParser::ExprAddMoinsContext *ctx) override;
	Any visitExprMultDiv (GrammaireParser::ExprMultDivContext *ctx) override;
	Any visitExprMod(GrammaireParser::ExprModContext *ctx) override;
	Any visitExprLt(GrammaireParser::ExprLtContext *ctx) override;
	Any visitExprLeq(GrammaireParser::ExprLeqContext *ctx) override;
	Any visitExprNeq (GrammaireParser::ExprNeqContext *ctx) override;
	Any visitExprEq (GrammaireParser::ExprEqContext *ctx) override;
	Any visitExprGt(GrammaireParser::ExprGtContext *ctx) override;
	Any visitExprGeq(GrammaireParser::ExprGeqContext *ctx) override;
	Any visitExprOrBit(GrammaireParser::ExprOrBitContext *ctx) override;
	Any visitExprXorBit(GrammaireParser::ExprXorBitContext *ctx) override;
	Any visitExprAnd(GrammaireParser::ExprAndContext *ctx) override;
	Any visitExprOr(GrammaireParser::ExprOrContext *ctx) override;
	Any visitExprAndBit(GrammaireParser::ExprAndBitContext *ctx) override;
	Any visitExprDec(GrammaireParser::ExprDecContext *ctx) override;
	Any visitExprInc(GrammaireParser::ExprIncContext *ctx) override;
	Any visitExprSizeE(GrammaireParser::ExprSizeEContext *ctx) override;
	Any visitExprSizeT(GrammaireParser::ExprSizeTContext *ctx) override;
	Any visitExprNeg(GrammaireParser::ExprNegContext *ctx) override;
	Any visitExprNegBin(GrammaireParser::ExprNegBinContext *ctx) override;
	Any visitExprNon(GrammaireParser::ExprNonContext *ctx) override;
	Any visitExprLinc(GrammaireParser::ExprLincContext *ctx) override;
	Any visitExprLdec(GrammaireParser::ExprLdecContext *ctx) override;
	Any visitExprTer(GrammaireParser::ExprTerContext *ctx) override;
	Any visitExprPar(GrammaireParser::ExprParContext *ctx) override;
	Any visitExprCast(GrammaireParser::ExprCastContext *ctx) override;

	//Variables
	Any visitLvalSimple(GrammaireParser::LvalSimpleContext *ctx) override;
	Any visitLvalTab(GrammaireParser::LvalTabContext *ctx) override;

	//Constantes
	Any visitExprConst(GrammaireParser::ExprConstContext *ctx) override;
	Any visitExprChar(GrammaireParser::ExprCharContext *ctx) override;

    // Declaration
    Any visitDeclDef(GrammaireParser::DeclDefContext *ctx) override;
    Any visitDeclLval(GrammaireParser::DeclLvalContext *ctx) override;
    Any visitDeclTabDef(GrammaireParser::DeclTabDefContext *ctx) override;
	Any visitElementTabV(GrammaireParser::ElementTabVContext *ctx) override;
	Any visitElementsTab(GrammaireParser::ElementsTabContext *ctx) override;

    Any aggregateResult(Any aggregate, const Any &nextResult);
    Any defaultResult();

private:    
    vector<Any> unwrapVector(vector<Any> &v);
	template<typename T> bool isType (Any a);
    Type getType(string text);
	pair<Expression*, Expression*> getOperandesBinaires(vector<Any> & vect);
	string getTypeNamePar(antlr4::ParserRuleContext* ctx);
	Expression* getExpressionUnaire(OpUnaire op, GrammaireParser::ExprContext* ctx);
	Expression* getExpressionBinaire(OpBinaire op, GrammaireParser::ExprContext* ctx);
	Expression* getSizeOfType(OpUnaire op, GrammaireParser::ExprContext* ctx);
};