/*
 * Guide de style : 
 
 nom de la règle
    : option1   # label1
    | option2   # label2
    | optionN   # labelN
    ;

 */

grammar LexerIdentifiers;

WS  // Gestion des espaces et tabulations => Doit être en premier for some reason
    : [ \t\r\n]+ -> skip 
    ;

// ===== Commentaires =====

COMMENT_MULT
    : '/*' (.|EOL)*? '*/' 
    -> skip
    ;

COMMENT_LINE
    : '//' .*? EOL 
    -> skip
    ;

DP  // Directive préprocesseur, non interprétée donc incluse comme commentaire
    : ([^#] | EOL '#') .+? EOL // Début de ligne quelconque
    -> skip
    ;

// ===== /Commentaires =====

EOL // Fin de lignes
    : ('\r'? '\n') | EOF
    ;

TYPE
    : 'char' | 'int32_t' | 'int64_t'
    ;

/*
 Important de définir VAR après TYPE, car les pattern correspondant 
 à TYPE sont aussi capturés par VAR (TYPE est plus précis et prioritaire
 -> mots clefs du langage)
 */
VAR 
    : [a-zA-Z] ([a-z] | [A-Z] | [0-9] | '_')* 
    ;

CHAR
    : '\'' (~['\\] | '\\\'' | '\\r' | '\\n' | '\\t') '\''
    ;

INT 
    : [0-9]+
    ;