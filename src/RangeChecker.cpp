#include "RangeChecker.h"
#include "utils.h"

#include <list>

#include "StructureDonnees/Affectation.h"
#include "StructureDonnees/ExpBinaire.h"
#include "StructureDonnees/ExpTernaire.h"
#include "StructureDonnees/AppelFonction.h"
#include "StructureDonnees/Lvalue.h"
#include "StructureDonnees/ExpUnaire.h"

using namespace std;

list<string> RangeChecker::programRangeChecking (Programme & programme)
{
	list<string> errorList;

	// first we will find all the declarations in the program and :
	// - find out which variable are declared global, local, or as parameters
	// - check if there are some naming conflicts (in variables or functions)

	// RECORD ALL GLOBAL VARS DECLARATIONS
	for (auto it : programme.declarations)
	{
		if (programme.variablesGlobales.find (it->getNomVariable ()) == programme.variablesGlobales.end ())
		{
			programme.variablesGlobales.insert
			(
				{
					it->getNomVariable (),
					make_tuple (it, false)
				}
			);
		}
		else
		{
			//error : deux var globales meme nom
			std::string str = "ERROR : global variable \'" + it->getNomVariable () + "\' declared twice.";
			errorList.push_back (str);
		}
	}

	// RECORD ALL FUNCTIONS DEFINITIONS
	vector<Fonction*> fonctions = programme.getFonctions ();
	for (auto itf : fonctions)
	{
		if (programme.mapFonctions.find (itf->getNom ()) == programme.mapFonctions.end ())
		{
			programme.mapFonctions.insert ({ itf->getNom (), make_pair (itf, false) });
		}
		else
		{
			//error : deux fonctions meme nom
			std::string str = "ERROR : fonction \'" + itf->getNom () + "\' declared twice.";
			errorList.push_back (str);
		}
	}

	map<string, std::pair<Fonction*, bool>>& fonctionsMap = programme.getMapFonctions ();
	for (auto itfp : fonctionsMap)
	{
		Fonction* func = itfp.second.first;
		// RECORD ALL PARAMETERS DEFINITIONS
		for (auto it : func->getParams ())
		{
			if (func->parametres.find (it->getNomVariable ()) == func->parametres.end ())
			{
				func->parametres.insert (
					{
						it->getNomVariable (),
						make_tuple (it, false)
					}
				); 
			}
			else
			{
				//error : 2 paramètre même nom
				std::string str = "ERROR : parameter \'" + it->getNomVariable () + "\' of function \'" + func->getNom () + "\' declared twice.";
				errorList.push_back (str);
			}
		}

		int MAX = numeric_limits<int>::max ();
		// RECORD ALL LOCAL VARS DEFINITIONS
		for (auto it : func->corps.getInstructions ())
		{
			if (it->isDeclaration ())
			{
				Declaration *r = static_cast<Declaration*>(&(*it));
				if (func->variablesLocales.find (r->getNomVariable ()) == func->variablesLocales.end ())
				{
					bool initialized = r->getValeur () != nullptr;
					int index = MAX;
					if (initialized)
					{
						index = 0;
					}
					func->variablesLocales.insert
					(
						{
							r->getNomVariable (),
							make_tuple (r, make_pair (initialized, index), false)
						}
					);
				}
				else
				{
					//error : 2 local vars même nom
					std::string str = "ERROR : local variable \'" + r->getNomVariable () +
						"\' in function \'" + func->getNom () + "\' declared twice.";
					errorList.push_back (str);
				}
			}
		}
	}

	// Now we will link all variables usages (class LValue) in the programm with their declarations
	// and we also link all function calls (class AppelFonction) in the programm with functions objects
	for (auto itf : fonctionsMap)
	{
		Fonction* func = itf.second.first;
		int exprCounter = 0;
		for (auto inst : func->corps.getInstructions ())
		{
			for (auto expr : inst->getSubExpressions ())
			{
				exprCounter++;
				// LVALUES USAGE LINKED TO DECLARATION
				if (expr->isLvalue ())
				{
					Lvalue *lvalue = static_cast<Lvalue*>(expr);
					string nomVar = lvalue->getNomVariable ();

					// PARAMS > LOCAL_VARS > GLOBAL_VARS

					// we check if the variable exists as a parameter
					auto entryP = func->parametres.find (nomVar);
					if (entryP != func->parametres.end ())
					{
						// we store the pointer to the declaration in the lvalue
						Declaration* param = std::get<0> (entryP->second);
						lvalue->setDeclaration (param);
						// we report that the parameter is used at least once
						bool& used = std::get<1> (entryP->second);
						used = true;
					}
					else
					{
						// we check if the variable exists as a local variable
						auto entryL = func->variablesLocales.find (nomVar);
						if (entryL != func->variablesLocales.end ())
						{
							// we store the pointer to the declaration in the lvalue
							Declaration* decl = std::get<0> (entryL->second);
							lvalue->setDeclaration (decl);
							// we report that the variable is used at least once
							bool& used = std::get<2> (entryL->second);
							used = true;

							std::pair<bool, int>& pair = std::get<1> (entryL->second);
							if (pair.second > exprCounter && !lvalue->isVariableTableau ())
							{
								if (!decl->isDeclarationTableau ())
								{
									// variable used but not initialized
									string str;
									str += "WARNING: variable \'";
									str += lvalue->getNomVariable ();
									str += "\' in function \'";
									str += func->getNom ();
									str += "\' is used but is not initialized";
									anticipatedWarnings.push_back (str);
								}
							}
						}
						else
						{
							// we check if the variable exists as a global variable
							auto entryG = programme.variablesGlobales.find (nomVar);
							if (entryG != programme.variablesGlobales.end ())
							{
								// we store the pointer to the declaration in the lvalue
								Declaration* decl = std::get<0> (entryG->second);
								lvalue->setDeclaration (decl);
								// we report that the variable is used at least once
								bool& used = std::get<1> (entryG->second);
								used = true;
							}
							else
							{
								//error : variable not found
								std::string str = "ERROR : variable \'" + lvalue->getNomVariable () +
									"\' in function \'" + func->getNom () + "\' used but never declared.";
								errorList.push_back (str);
							}
						}
					}
				}
				// FUNCTIONS CALLS LINKED TO FUNCTION DEFINITION
				else if (expr->isAppelFonction ())
				{
					AppelFonction *appelFct = static_cast<AppelFonction*>(expr);
					string nomFonction = appelFct->getNomFonction ();

					if (nomFonction == "putchar")
					{
						// we check if there are more or less than 1 parameter						
						if (appelFct->getsParams ().size () != 1)
						{
							// error : incorrect number of parameters
							std::string str = "ERROR : fonction \'" + nomFonction + "(";
							int i = 0;
							int size = (appelFct->getsParams ().size ());
							for (i = 0; i < (size - 1); i++)
							{
								str += Types[appelFct->getsParams ()[i]->typer ()] + ',';
							}
							if (size > 0)
								str += Types[appelFct->getsParams ()[i]->typer ()];
							str += ")\' called with incorrect parameters";
							errorList.push_back (str);
						}
					}
					else if (nomFonction == "getchar")
					{
						if (!appelFct->getsParams ().empty ())
						{
							// error : incorrect number of parameters
							std::string str = "ERROR : fonction \'" + nomFonction + "(";
							int i = 0;
							int size = (appelFct->getsParams ().size ());
							for (i = 0; i < (size - 1); i++)
							{
								str += Types[appelFct->getsParams ()[i]->typer ()] + ',';
							}
							if (size > 0)
								str += Types[appelFct->getsParams ()[i]->typer ()];
							str += ")\' called with incorrect parameters";
							errorList.push_back (str);
						}
					}
					else
					{
						auto entry = fonctionsMap.find (nomFonction);
						// we check if the function exists
						if (entry != fonctionsMap.end ())
						{
							// we check if the number of parameters provided is correct
							if (entry->second.first->parametres.size () == appelFct->getsParams ().size ())
							{
								// OK
								appelFct->setFonction (entry->second.first);
								// we report that the function is used at least once
								auto pair = make_pair (entry->second.first, true);
								entry->second = pair;
							}
							else
							{
								// error : incorrect number of parameters
								std::string str = "ERROR : fonction \'" + nomFonction + "(";
								int i = 0;
								int size = (appelFct->getsParams ().size ());
								for (i = 0; i < (size - 1); i++)
								{
									str += Types[appelFct->getsParams ()[i]->typer ()] + ',';
								}
								if (size > 0)
									str += Types[appelFct->getsParams ()[i]->typer ()];
								str += ")\' called with an incorrect number of parameters";
								errorList.push_back (str);
							}

						}
						else
						{
							// error : called an uninitialized fonction
							std::string str = "ERROR : fonction \'" + nomFonction + "(";
							int i = 0;
							int size = (appelFct->getsParams ().size ());
							for (i = 0; i < (size - 1); i++)
							{
								str += Types[appelFct->getsParams ()[i]->typer ()] + ',';
							}
							if (size > 0)
								str += Types[appelFct->getsParams ()[i]->typer ()];
							str += ")\' used but never declared";
							errorList.push_back (str);
						}
					}
				}
				// record affectation (used in static analysis to see if a variable is affected before use)
				else if (expr->isAffectation ())
				{
					Affectation* affectation = static_cast<Affectation*>(expr);

					Lvalue *lvalue = static_cast<Lvalue*>(affectation->getVariable ());
					string nomVar = lvalue->getNomVariable ();

					// we check if the variable exists as a local variable
					auto entryL = func->variablesLocales.find (nomVar);
					if (entryL != func->variablesLocales.end ())
					{
						// we report that the variable is initialized in this expression
						pair<bool, int>& pair = std::get<1> (entryL->second);
						if (pair.second > exprCounter)
						{
							pair.first = true;
							pair.second = exprCounter;
						}
					}
					else
					{
						// we check if the variable exists as a global variable
						auto entryG = programme.variablesGlobales.find (nomVar);
						if (entryG != programme.variablesGlobales.end ())
						{
							// we report that the variable is initialized in this expression
							bool& used = std::get<1> (entryG->second);
							used = true;
						}
					}
				}
			}
		}
	}
	return errorList;
}

list<string> RangeChecker::programStaticAnalysis (Programme & programme)
{
	list<string> warningList;

	// CHECKING IF THERE ARE UNUSED FUNCTIONS
	for (auto entry : programme.mapFonctions)
	{
		if (!entry.second.second && entry.second.first->getNom () != "main")
		{
			//function never used
			string str;
			str += "WARNING: Function \'";
			str += entry.second.first->getNom ();
			str += "\' is never used";
			warningList.push_back (str);
		}
	}

	// CHECKING IF THERE ARE UNUSED GLOBAL VARIABLES
	for (auto entry : programme.variablesGlobales)
	{
		if (!std::get<1> (entry.second))
		{
			// global variable never used
			string str;
			str += "WARNING: Global variable \'";
			str += std::get<0> (entry.second)->getNomVariable ();
			str += "\' is never used";
			warningList.push_back (str);
		}
	}

	for (auto fctentry : programme.mapFonctions)
	{
		// CHECKING IF THERE ARE UNUSED PARAMETERS
		for (auto entry : fctentry.second.first->parametres)
		{
			if (!std::get<1> (entry.second))
			{
				// parameter never used
				string str;
				str += "WARNING: parameter \'";
				str += std::get<0> (entry.second)->getNomVariable ();
				str += "\' of function \'";
				str += fctentry.second.first->getNom ();
				str += "\' is never used";
				warningList.push_back (str);
			}
		}
		// CHECKING IF THERE ARE UNUSED LOCAL VARIABLES
		for (auto entry : fctentry.second.first->variablesLocales)
		{
			if (!get<2> (entry.second))
			{
				// local variable never used
                string str;
                str += "WARNING: Local variable \'";
                str += std::get<0>(entry.second)->getNomVariable();
                str += "\' declared in function \'";
                str += fctentry.second.first->getNom();
                str += "\' is never used";
                warningList.push_back(str);
			}
		}
	}

	for (auto warning : anticipatedWarnings)
	{
		warningList.push_back (warning);
	}


	return warningList;
}
