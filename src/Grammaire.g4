/*
 * Guide de style : 
 
 nom de la règle
    : option1   # label1
    | option2   # label2
    | optionN   # labelN
    ;

 */

grammar Grammaire;

import LexerIdentifiers;

progc // Axiome 
    : prog
    ;

prog
    : decl prog         #progDecl
    | fonction prog     #progFunc
    | EOF               #progEOF
    ;

paramV
    : ',' param
    |
    ;

param
	: TYPE VAR '[' ']' paramV 	# paramTab
    | TYPE VAR paramV           # paramSimple
    |                           # paramVide
    ;

fonction
    : typeRet VAR '(' param ')' bloc
    ;

typeRet
    : TYPE      # typeRetType
    | 'void'    # typeRetVoid
    ;

paramAppelV
    : ',' paramAppel
    |
    ;

paramAppel
    : expr paramAppelV
    |
    ;

appelFonction
    : VAR '(' paramAppel ')'
    ;

lval
    : VAR               # lvalSimple
    | VAR '[' expr ']'  # lvalTab
    ;

elementTabV
    : ',' expr elementTabV
    |
    ;

elementsTab
    : expr elementTabV
    |
    ;
	
decl
    : TYPE def ';'                                  # declDef
    | TYPE lval ';'                                 # declLval
    | TYPE VAR '[' ']' '=' '{' elementsTab '}' ';'  # declTabDef
    ;

def
    : lval '=' expr
    ;

instr
    : 'return' expr ';'				#instrReturn
    | expr ';'						#instrExp
    | decl							#instrDecl
    | controle						#instrControle
    ;
    
expr
    : appelFonction             # exprFunc
    | def                       # exprDef
    | '(' expr ')'              # exprPar
    | 'sizeof' expr             # exprSizeE
    | 'sizeof' '(' TYPE ')'     # exprSizeT
    | '-' expr                  # exprNeg
    | '~' expr                  # exprNegBin
    | '!' expr                  # exprNon
    | '++' lval                 # exprLinc
    | '--' lval                 # exprLdec
    | lval '--'                 # exprDec
    | lval '++'                 # exprInc
    | '(' TYPE ')' expr         # exprCast
    | expr op=('*'|'/') expr	# exprMultDiv
	| expr op=('+'|'-') expr 	# exprAddMoins
    | expr '<<' expr            # exprDecL
    | expr '>>' expr            # exprDecR
    | expr '%' expr             # exprMod
    | expr '==' expr            # exprEq
    | expr '!=' expr            # exprNeq
    | expr '<' expr             # exprLt
    | expr '>' expr             # exprGt
    | expr '<=' expr            # exprLeq
    | expr '>=' expr            # exprGeq
    | expr '&&' expr            # exprAnd
    | expr '&' expr             # exprAndBit
    | expr '|' expr             # exprOrBit
    | expr '$' expr             # exprXorBit
    | expr '||' expr            # exprOr
    | expr '?' expr ':' expr    # exprTer
    | INT                       # exprConst
    | lval                      # exprVar
    | CHAR                      # exprChar
    ;
    
controle
    : 'while' '(' expr ')' bloc				    # controleWhile
    | 'while' '(' expr ')' instr				# controleWhile
	| 'if' '(' expr ')' bloc 'else' bloc		# controleIfElse
    | 'if' '(' expr ')' instr 'else' instr		# controleIfElse
	| 'if' '(' expr ')' bloc 'else' instr		# controleIfElse
    | 'if' '(' expr ')' instr 'else' bloc		# controleIfElse
    | 'if' '(' expr ')' bloc					# controleIf
    | 'if' '(' expr ')' instr					# controleIf
    ;

bloc
    : '{' contenuBloc '}'
    ;

contenuBloc
    : instr contenuBloc			
    | bloc contenuBloc
    |
    ;