#pragma once

using namespace std;

#include <string>

string testFilesValid[] = {
    "moodle/FrontEndTests/ValidPrograms/01_SimpleDeclaration.c",
    "moodle/FrontEndTests/ValidPrograms/02_IncludeStdint.c",
    "moodle/FrontEndTests/ValidPrograms/03_OperatorPlus.c",
    "moodle/FrontEndTests/ValidPrograms/04_OperatorMinus.c",
    "moodle/FrontEndTests/ValidPrograms/05_OperatorDiv.c",
    "moodle/FrontEndTests/ValidPrograms/06_OperatorMult.c",
    "moodle/FrontEndTests/ValidPrograms/07_OperatorInf.c",
    "moodle/FrontEndTests/ValidPrograms/08_OperatorSup.c",
    "moodle/FrontEndTests/ValidPrograms/09_OperatorLeq.c",
    "moodle/FrontEndTests/ValidPrograms/10_OperatorGeq.c",
    "moodle/FrontEndTests/ValidPrograms/11_OperatorEqu.c",
    "moodle/FrontEndTests/ValidPrograms/12_OperatorDiff.c",
    "moodle/FrontEndTests/ValidPrograms/13_OperatorMod.c",
    "moodle/FrontEndTests/ValidPrograms/14_OperatorAnd.c",
    "moodle/FrontEndTests/ValidPrograms/15_OperatorOr.c",
    "moodle/FrontEndTests/ValidPrograms/16_OperatorBOr.c",
    "moodle/FrontEndTests/ValidPrograms/17_OperatorBAnd.c",
    "moodle/FrontEndTests/ValidPrograms/18_OperatorNeg.c",
    "moodle/FrontEndTests/ValidPrograms/19_OperatorLeftShift.c",
    "moodle/FrontEndTests/ValidPrograms/20_OperatorRightShift.c",
    "moodle/FrontEndTests/ValidPrograms/21_OperatorIncPost.c",
    "moodle/FrontEndTests/ValidPrograms/22_OperatorIncPre.c",
    "moodle/FrontEndTests/ValidPrograms/23_OperatorDecPost.c",
    "moodle/FrontEndTests/ValidPrograms/24_OperatorDecPre.c",
    "moodle/FrontEndTests/ValidPrograms/25_OperatorNot.c",
    // Unsupported : "moodle/FrontEndTests/ValidPrograms/26_OperatorXor.c",
    "moodle/FrontEndTests/ValidPrograms/27_OperatorComp.c",
    // Unsupported : "moodle/FrontEndTests/ValidPrograms/28_OperatorPlusAssign.c",
    // Unsupported : "moodle/FrontEndTests/ValidPrograms/29_OperatorMinusAssign.c",
    // Unsupported : "moodle/FrontEndTests/ValidPrograms/30_OperatorMultAssign.c",
    // Unsupported : "moodle/FrontEndTests/ValidPrograms/31_OperatorDivAssign.c",
    // Unsupported : "moodle/FrontEndTests/ValidPrograms/32_OperatorModAssign.c",
    // Unsupported : "moodle/FrontEndTests/ValidPrograms/33_OperatorBitAndAssign.c",
    // Unsupported : "moodle/FrontEndTests/ValidPrograms/34_OperatorBitOrAssign.c",
    // Unsupported : "moodle/FrontEndTests/ValidPrograms/35_OperatorBitXorAssign.c",
    // Unsupported : "moodle/FrontEndTests/ValidPrograms/36_OperatorComma.c",
    "moodle/FrontEndTests/ValidPrograms/37_Par.c",
    "moodle/FrontEndTests/ValidPrograms/38_ArithmeticExpression.c",
    "moodle/FrontEndTests/ValidPrograms/39_ArithmeticExpressionWithPar.c",
    "moodle/FrontEndTests/ValidPrograms/40_If.c",
    "moodle/FrontEndTests/ValidPrograms/41_IfElse.c",
    "moodle/FrontEndTests/ValidPrograms/42_While.c",
    "moodle/FrontEndTests/ValidPrograms/44_IfElseIf.c",
    "moodle/FrontEndTests/ValidPrograms/46_ArrayDec.c",
    "moodle/FrontEndTests/ValidPrograms/47_ArrayUse.c",
    // Unsupported (For) : "moodle/FrontEndTests/ValidPrograms/49_ArrayFibo.c",
    "moodle/FrontEndTests/ValidPrograms/50_CharConst.c",
    "moodle/FrontEndTests/ValidPrograms/51_CharConstSpecial.c",
    "moodle/FrontEndTests/ValidPrograms/52_Int64Dec.c",
    "moodle/FrontEndTests/ValidPrograms/53_FunctionCallPutChar.c",
    "moodle/FrontEndTests/ValidPrograms/54_FunctionCallCustom.c",
    "moodle/FrontEndTests/ValidPrograms/55_Return.c",
    // Unsupported (For) : "moodle/FrontEndTests/ValidPrograms/56_ArrayAsParam.c",
    "moodle/FrontEndTests/ValidPrograms/57_VarInitialization.c"
    // Unsupported : "moodle/FrontEndTests/ValidPrograms/58_MultipleVarDeclarations.c",
    // Unsupported : "moodle/FrontEndTests/ValidPrograms/59_MixedDeclarationsInitializations.c"
};

string testFilesInvalid[] = {
    // Lexer
    "moodle/FrontEndTests/LexError/01_LexError_InvalidCharInStream.c",
    "moodle/FrontEndTests/LexError/02_LexError_InvalidCharInStreamWithinProgram.c",

    // Syntax
    "moodle/FrontEndTests/SyntaxError/01_SyntaxError_TwoOperators.c",
    "moodle/FrontEndTests/SyntaxError/02_SyntaxError_TwoOperands.c",
    "moodle/FrontEndTests/SyntaxError/03_SyntaxError_MissingSemicolon.c",
    "moodle/FrontEndTests/SyntaxError/04_SyntaxError_MissingPar.c",
    "moodle/FrontEndTests/SyntaxError/05_SyntaxError_TooManyClosingPar.c",

	// Semantic
	"moodle/FrontEndTests/SemanticError/01_MissingVarDeclaration.c"
};