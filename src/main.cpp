#include <iostream>
#include "antlr4-runtime.h"
#include "GrammaireLexer.h"
#include "GrammaireParser.h"
#include "dotexport.h"
#include "MainVisitor.h"
#include "StructureDonnees/Programme.h"
#include "RangeChecker.h"
#include "tests.h"
#include "testData.h"
#include "Backend/BackendManager.h"

using namespace std;
using namespace antlr4;

int main(int argc, char *argv[]) {
    string inputText;
    bool optim          = false;
    bool verifStatique  = false;
    bool makeAsm        = false;
    bool makeGraph      = false;
    bool makeTest       = false;
    bool verbose        = false;

    string inputFile = "testInput.c";

    // c'est sale mais ça marche
    for (int i = 1; i < argc; i++) {
        string b = argv[i];
        if (b == "-o") {
            optim = true;
        } else if (b == "-a") {
            verifStatique = true;
        } else if (b == "-c") {
            makeAsm = true;
        } else if (b == "-g") {
            makeGraph = true;
        } else if (b == "-t") {
            // /!\ L'option t exécute les tests et s'arrête (pas de compilation en assembleur, etc.)
            // -> Incompatible avec les autres options (à part -v)
            makeTest = true;
        }  else if (b == "-v") {
            verbose = true;
        } else {
            inputFile = argv[i];
        }
    }

    ifstream t(inputFile);
    stringstream buffer;
    buffer << t.rdbuf();
    inputText = buffer.str();

    if (makeTest) {
        if(verbose)
            cout << endl;
        for(int i = 0; i < sizeof(testFilesValid)/sizeof(testFilesValid[0]); i++) {
            int test = testFile(testFilesValid[i], verbose);
            if(test != 0) {
                cerr << "Failed test (valid program) : " << testFilesValid[i] << endl;
                return test;
            }
        }
        for(int i = 0; i < sizeof(testFilesInvalid)/sizeof(testFilesInvalid[0]); i++) {
            int test = testFile(testFilesInvalid[i], verbose);
            if(test == 0) {
                cerr << "Failed test (invalid program) : " << testFilesInvalid[i] << endl;
                return 1;
            }
        }
        return 0;
    }

    ANTLRInputStream input(inputText);
    GrammaireLexer lexer(&input);
    CommonTokenStream tokens(&lexer);

    GrammaireParser parser(&tokens);
    tree::ParseTree *tree = parser.progc();

	if (lexer.getNumberOfSyntaxErrors () > 0) {
		cerr << "Lexer syntax error" << endl;
		return 1;
	}

    MainVisitor visitor;
    Programme p = visitor.visit(tree);

    if(verbose) {
        cout << endl << "    *-----------------------*";
        cout << endl << "    |   Visite de l'arbre   |";
        cout << endl << "    *-----------------------*";
        cout << endl << endl;
        cout << p << endl;
    }

	RangeChecker checker;
	list<string> errors = checker.programRangeChecking (p);
	if (verbose) {
		cout << endl << "    *-----------------------*";
		cout << endl << "    | Résolution de portée  |";
		cout << endl << "    *-----------------------*";
		cout << endl << endl;

		if (!errors.empty ())
		{
			for (auto errorStr : errors)
				cerr << errorStr << endl;
			return 2;
		}
	}
	else
	{
		if (!errors.empty ())
		{
			cerr << errors.front () << endl;
			return 2;
		}
	}
    
    if(verifStatique) {
	    list<string> warnings = checker.programStaticAnalysis (p);

        if(verbose) {
            cout << endl << "    *-----------------------*";
            cout << endl << "    | Vérification statique |";
            cout << endl << "    *-----------------------*";
            cout << endl << endl;

            for (auto warningStr : warnings)
                cerr << warningStr << endl;
        }
    }

    // Visualisation Graphviz du futur en PDF
    if(makeGraph) {
        DotExport dotexport(&parser);
        tree::ParseTreeWalker::DEFAULT.walk(&dotexport, tree);
        ofstream out("AST.dot");
        out << dotexport.getDotFile();
        out.close();
        system("dot -Tpdf -o AST.pdf AST.dot");
    }

    // Partie Backend
    if(makeAsm) {
        string outFile = inputFile.substr (0,inputFile.length() - 2) + ".s";
        ofstream output(outFile);
        BackendManager bm(output, p, verbose, optim);
        bm.gen_asm(inputFile);

        output.close();
    }
    
    return 0;
}