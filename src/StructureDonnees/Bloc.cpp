#include "Bloc.h"

Bloc::Bloc(vector<Instruction*> instructions) 
{
	this->instructions = instructions;
}

Bloc::Bloc()
{
}


vector<Instruction*>& Bloc::getInstructions ()
{
	return instructions;
}

void Bloc::setInstructions (vector<Instruction*>& instructions)
{
	this->instructions = instructions;
}

void Bloc::addInstruction (Instruction * instr)
{
	instructions.push_back (instr);
}

ostream& operator <<(ostream& Stream, const Bloc& Obj)
{
	for(int i = 0; i < Obj.instructions.size(); i++) {
		Stream << "  --> " << *Obj.instructions[i] << endl;
	}

	return Stream;
}

vector<Expression*> Bloc::getSubExpressions ()
{
	vector<Expression*> res;
	for (auto it : instructions)
	{
		auto resTmp = it->getSubExpressions ();
		res.insert (res.end (), resTmp.begin (), resTmp.end ());
	}

	return res;
}

string Bloc::print () const
{
	string str = "Bloc (";
	for (auto instr : instructions)
	{
		str += instr->print () + ' ';
	}
	str += ')';
	return str;
}

string Bloc::buildIR(CFG* cfg) {
	string result;
	for(auto it : instructions) {
		result = it->buildIR(cfg);
	}

	return result;
}

void Bloc::setTypeRetourFonction(Type type) {
	for(auto instr: instructions){
		instr->setTypeRetourFonction(type);
	}
}
