#pragma once

using namespace std;

#include "Expression.h"
#include "Operateurs.h"
#include "../Backend/IR.h"

// opérateur ternaire
class ExpTernaire : public Expression {
private:
    Expression* condition;
    Expression* valueTrue;
	Expression* valueFalse;

public:
	ExpTernaire(Expression * cond, Expression * valueTrue, Expression * valueFalse);		
	~ExpTernaire();
	Expression* getCondition();
	void setCondition(Expression * condition);
	Expression* getValueTrue();
	void setValueTrue(Expression * valueTrue);
	Expression* getValueFalse();
	void setValueFalse(Expression * valueFalse);
	Type typer() const override {
		return (valueTrue->typer() > valueFalse->typer()) ? valueTrue->typer() : valueFalse->typer();
	}

	virtual string print () const;
	virtual vector<Expression*> getSubExpressions () override;
	virtual string buildIR(CFG * cfg);
};
