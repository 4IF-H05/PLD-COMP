#pragma once

using namespace std;

#include "Expression.h"
#include "Lvalue.h"

// Affectation d'une valeur à une variable

class Affectation : public Expression {
public: 
	Affectation(Lvalue* variable, Expression* exp, Type expected);
	Affectation(Lvalue* variable, Expression* exp);
	void setValeur(Expression* exp);
	Expression* getValeur() ;
	void setVariable(Lvalue* lvalue);
	Lvalue* getVariable() ;

	virtual Type typer () const override;
	virtual string print () const override;
	virtual bool isAffectation () override { return true; }
	virtual vector<Expression*> getSubExpressions () override;
	virtual string buildIR(CFG* cfg);
private:
    Expression *valeur;
    Lvalue *variable;
	Type expected = ERROR_TYPE;

};
