#include "Parametre.h"

Parametre::Parametre(Type & type) {
	this->type = type;
}

Parametre::Parametre(Type & type, string nom) {
	this->type = type;
	this->nom = nom;
}

Type & Parametre::getType()
{
	return type;
}

void Parametre::setType(const Type & type)
{
	this->type = type;
}

string & Parametre::getNom()
{
	return nom;
}

void Parametre::setNom(string & nom)
{
	this->nom = nom;
}

ostream& operator <<(ostream& Stream, const Parametre& Obj)
{
	Stream << Obj.nom << "(" << Types[Obj.type] << ")";
	return Stream;
}