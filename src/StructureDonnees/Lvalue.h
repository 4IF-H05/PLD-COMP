#pragma once

using namespace std;

#include <string>
#include "Expression.h"
#include "Declaration.h"
#include "Parametre.h"
#include "Types.h"

// Variable en tant que lValue

class Declaration;
class Parametre;

class Lvalue : public Expression {
public:
	//Lvalue(string & nomVariable);
	virtual Type typer () const override;
	string& getNomVariable();
	void setNomVariable(string & nom);
	Lvalue();
	Lvalue(string &);
	virtual bool isLvalue() override { return true; }
	virtual bool isVariableTableau () { return false; }
	virtual vector<Expression*> getSubExpressions () override;

	void setDeclaration(Declaration* decl);

	//TODO: this needs to be changed (see comment line 32)
	Declaration* getDeclaration () { return declaration; }
	//TODO: this needs to be changed (see comment line 32)
	virtual string buildIR(CFG * cfg) = 0;
protected:
    string nomVariable;


	//TODO : Parametre and Declaration SHOULD BE THE SAME THING
	Declaration* declaration = nullptr;	// where the lvalue is declared
};