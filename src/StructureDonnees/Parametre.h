#pragma once

using namespace std;

#include <iostream>
#include <string>
#include "Types.h"

// Paramètre défini lors de la déclaration d'une fonction

class Parametre {
friend ostream& operator <<(ostream&, const Parametre&); 

public:
	Parametre(Type & type);
	Parametre(Type & type, string nom);

	Type& getType();
	void setType(const Type& type);

	string & getNom ();
	void setNom (string& nom);

private:
    Type type;
    string nom;

};