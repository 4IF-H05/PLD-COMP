#pragma once

class Expression;
class CFG;

using namespace std;

#include <vector>
#include <iostream>
#include <string>
#include "Types.h"

class Instruction {
	friend ostream& operator <<(ostream&, const Instruction&); 

public:
	virtual string print () const;

	virtual bool isDeclaration () { return false; }

	virtual string buildIR(CFG* cfg) { return ""; throw invalid_argument("Can't build IR for this object"); }

	// DESCRIPTION:
	// this function returns all the "leaf" expressions that appear in the instruction.
	// "Leaf" expressions are either Constants or Lvalues.
	//
	// RETURNS:
	// a vector containing the pointers to the expressions.
	virtual vector<Expression*> getSubExpressions ();

	// this function set the type of the return instruction
	virtual void setTypeRetourFonction(Type type) = 0;
};