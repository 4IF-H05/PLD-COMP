#pragma once

using namespace std;

#include "Expression.h"
#include "Operateurs.h"

// Expression à deux opérandes
class ExpBinaire : public Expression {
private:
    OpBinaire op;
    Expression* membreGauche = nullptr;
    Expression* membreDroit = nullptr;

public:
	ExpBinaire(Expression * membreGauche, OpBinaire  op, Expression * membreDroit);
	~ExpBinaire();

	virtual long eval();

	OpBinaire getOperateur() ;
	void setOperateur( OpBinaire  op);

	Expression* getMembreGauche() ;
	void setMembreGauche( Expression * membreGauche);

	Expression* getMembreDroit() ;
	void setMembreDroit( Expression * membreDroit);
	Type typer() const override { return (membreGauche->typer() > membreDroit->typer()) ? membreGauche->typer() : membreDroit->typer(); }

	virtual string print () const;
	virtual vector<Expression*> getSubExpressions () override;
	virtual string buildIR(CFG * cfg) override;
};
