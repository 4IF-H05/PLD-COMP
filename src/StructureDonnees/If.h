#pragma once

using namespace std;

#include "Controle.h"
#include "Instruction.h"

// Test conditionnel

class  If : public Controle {
private:
    Instruction* instrElse = nullptr;

public:
	If(Expression* clause, Instruction* corps, Instruction* instrElse);
	If(Expression* clause, Instruction* corps);	//Pas de else
	~If();

	Instruction* getInstrElse() ;
	void setInstrElse(Instruction* intrElse);
	virtual void setTypeRetourFonction(Type type);
	virtual vector<Expression*> getSubExpressions () override;

	virtual string print () const;

	virtual string buildIR(CFG * cfg);
};
