#include "VariableTableau.h"
#include "../Backend/IR.h"
#include "ExpBinaire.h"
#include "ConstanteI64.h"
#include "../Backend/asmTypes.h"
#include "../Backend/registers.h"
#include "ExpCast.h"
#include "DeclarationTableau.h"

VariableTableau::VariableTableau(string nom, Expression * indice) : Lvalue(nom), indice(indice)
{
}

string VariableTableau::print () const
{
	string str = "VarTab_";
	str += Types[this->typer()];
	str += "(" + nomVariable + ")";
	return str;
}

vector<Expression*> VariableTableau::getSubExpressions ()
{
	vector<Expression *> ret = indice->getSubExpressions();
	ret.push_back(this);
	return ret;
}

Expression *VariableTableau::getIndice() const {
	return indice;
}

void VariableTableau::setIndice(Expression *indice) {
	VariableTableau::indice = indice;
}

string VariableTableau::buildIR(CFG *cfg) {
	Type type = this->declaration->getType();

	// on récupère la valeur de l'indice dans une variable;
	string offset;
	if(indice->typer() != INT64_T) {
		ExpCast cast(indice, INT64_T);
		offset = cast.buildIR(cfg);
	} else {
		offset = indice->buildIR(cfg);
	}
    string adresse;
    string indice = cfg->create_new_tempvar(INT64_T);
    vector<string> vec = { indice, offset };
    cfg->current_bb->add_IRInstr(new Copy(cfg->current_bb, INT64_T, vec));
    string indiceFoisTaille = cfg->create_new_tempvar(INT64_T);
    vec = { indiceFoisTaille, indice, string("$")+to_string(asmSizes[type])};
		
    cfg->current_bb->add_IRInstr(new Mul(cfg->current_bb, INT64_T, vec));

	if(cfg->get_var_index(getDeclaration()->getNomVariable()) != 0) {

		DeclarationTableau* decTab = static_cast<DeclarationTableau*>(this->declaration);
		adresse = cfg->create_new_tempvar (INT64_T);
        vec = { adresse, indiceFoisTaille, decTab->getArrayAddress ()};
        cfg->current_bb->add_IRInstr(new Add(cfg->current_bb, INT64_T, vec));

	} else {
		// Si on arrive là c'est qu'on a une variable globale. 
		// Il ne faut pas commencer à pleurer tout de suite, 
		// on va s'en sortir
        string adresseGlobale = cfg->create_new_tempvar(INT64_T);
        vec = { adresseGlobale, string("$")+this->declaration->getNomVariable()};
        cfg->current_bb->add_IRInstr(new Copy(cfg->current_bb, INT64_T, vec));
        adresse = cfg->create_new_tempvar(INT64_T);
        vec = { adresse, indiceFoisTaille, adresseGlobale};
        cfg->current_bb->add_IRInstr(new Add(cfg->current_bb, INT64_T, vec));

	}
    if(isRvalue) {
        string valeur = cfg->create_new_tempvar(type);
        vector<string> vec = { valeur, adresse };
        cfg->current_bb->add_IRInstr(new Rmem(cfg->current_bb, type, vec));
        return valeur;
    }else{
        return adresse;
    }
}

void VariableTableau::setIsRvalue(bool isRvalue) {
    VariableTableau::isRvalue = isRvalue;
}

