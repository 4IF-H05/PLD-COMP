#pragma once
using namespace std;

#include "Constante.h"

//Constante nommée ou anonyme 64b

class ConstanteI64 : public Constante {

private:
	long int value;

public:
	ConstanteI64();
	ConstanteI64(long int value);
	virtual long eval();
	virtual Type typer() const override { return INT64_T; };
	virtual string print () const;
	string buildIR(CFG * cfg) override;
};