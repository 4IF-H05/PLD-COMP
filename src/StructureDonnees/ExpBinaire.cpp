#include "ExpBinaire.h"
#include "ExpCast.h"
#include "../Backend/IR.h"
#include "../Backend/registers.h"

ExpBinaire::ExpBinaire(Expression * membreGauche, OpBinaire  op, Expression * membreDroit) : Expression()
{
	setMembreDroit(membreDroit);
	setMembreGauche(membreGauche);
	setOperateur(op);
}

ExpBinaire::~ExpBinaire()
{
	//delete membreGauche;
	//delete membreDroit;
}

long ExpBinaire::eval() 
{
	switch (this->op) {
		case PLUS:
			return membreGauche->eval() + membreDroit->eval();
			break;
		case MOINS:
			return membreGauche->eval() - membreDroit->eval();
			break;
		case MULT:
			return membreGauche->eval() * membreDroit->eval();
			break;
		case DIV:
			return membreGauche->eval() / membreDroit->eval();
			break;
		case MODULO:
			return membreGauche->eval() % membreDroit->eval();
			break;
		case LEQ:
			return membreGauche->eval() <= membreDroit->eval();
			break;
		case GEQ:
			return membreGauche->eval() >= membreDroit->eval();
			break;
		case EQ:
			return membreGauche->eval() == membreDroit->eval();
			break;
		case GT:
			return membreGauche->eval() > membreDroit->eval();
			break;
		case LT:
			return membreGauche->eval() < membreDroit->eval();
			break;
		case ORB:
			return membreGauche->eval() | membreDroit->eval();
			break;
		case ANDB:
			return membreGauche->eval() & membreDroit->eval();
			break;
		case XORB:
			return membreGauche->eval() ^ membreDroit->eval();
			break;
		case OR:
			return membreGauche->eval() || membreDroit->eval();
			break;
		case AND:
			return membreGauche->eval() && membreDroit->eval();
			break;
	}

	Expression::eval();
}

OpBinaire ExpBinaire::getOperateur()
{
	return this->op;
}

void ExpBinaire::setOperateur(OpBinaire  op)
{
	this->op = op;
}

Expression * ExpBinaire::getMembreGauche()
{
	return this->membreGauche;
}

void ExpBinaire::setMembreGauche(Expression * membreGauche)
{
	this->membreGauche = membreGauche;
}

Expression * ExpBinaire::getMembreDroit()
{
	return this->membreDroit;
}

void ExpBinaire::setMembreDroit(Expression * membreDroit)
{
	this->membreDroit = membreDroit;
}

string ExpBinaire::print () const
{
	string str = "ExpBinaire_";
	str += OpBinaires[op];
	str += "(" + membreGauche->print() + ", " + membreDroit->print() + ')';
	return str;
}

vector<Expression*> ExpBinaire::getSubExpressions()
{
	auto res = membreDroit->getSubExpressions();
	auto res2 = membreGauche->getSubExpressions();
	res.insert (res.end(), res2.begin(), res2.end());
	return res;
}

string ExpBinaire::buildIR(CFG * cfg)
{
	Type type = typer ();
	string varLeft;
	string varRight;
	string varBin = cfg->create_new_tempvar (type);	

	if (this->op == OR || this->op == AND)
	{
		BasicBlock * testVarRightBB = new BasicBlock (cfg, cfg->new_BB_name ());
		BasicBlock * exprTrueBB = new BasicBlock (cfg, cfg->new_BB_name ());
		BasicBlock * exprFalseBB = new BasicBlock (cfg, cfg->new_BB_name ());
		BasicBlock * afterBB = new BasicBlock (cfg, cfg->new_BB_name ());

		if (membreGauche->typer () != type) {
			// attention, le destructeur de cast est appelé à la sortie de ce contexte
			ExpCast cast (membreGauche, type);
			varLeft = cast.buildIR (cfg);
		}
		else {
			varLeft = membreGauche->buildIR (cfg);
		}

		BasicBlock * testVarLeftBB = cfg->current_bb;

		vector<string> vecTestVarLeft = { string ("%") + workReg1[type], varLeft, "$0" };
		testVarLeftBB->add_IRInstr (new Cmp_eq (testVarLeftBB, type, vecTestVarLeft));

		if (this->op == OR)
		{
			testVarLeftBB->exit_false = testVarRightBB;
			testVarLeftBB->exit_true = exprTrueBB;
		}
		else if (this->op == AND)
		{
			testVarLeftBB->exit_false = exprFalseBB;
			testVarLeftBB->exit_true = testVarRightBB;
		}

		cfg->add_bb (testVarRightBB);

		if (membreDroit->typer () != type) {
			ExpCast cast (membreDroit, type);
			varRight = cast.buildIR (cfg);
		}
		else {
			varRight = membreDroit->buildIR (cfg);
		}

		testVarRightBB = cfg->current_bb;

		vector<string> vecTestVarRight = { string ("%") + workReg1[type], varRight, "$0" };
		testVarRightBB->add_IRInstr (new Cmp_eq (testVarRightBB, type, vecTestVarRight));
		testVarRightBB->exit_false = exprFalseBB;
		testVarRightBB->exit_true = exprTrueBB;

		cfg->add_bb (exprTrueBB);

		exprTrueBB->add_IRInstr (new Copy (exprTrueBB, type, { varBin, "$1" }));
		exprTrueBB->exit_false = nullptr;
		exprTrueBB->exit_true = afterBB;

		cfg->add_bb (exprFalseBB);
		
		exprFalseBB->add_IRInstr (new Copy (exprTrueBB, type, { varBin, "$0" }));
		exprFalseBB->exit_false = nullptr;
		exprFalseBB->exit_true = afterBB;
			
		cfg->add_bb (afterBB);

		return varBin;
	}

	// Casts implicites
	if(this->op != MODULO || type != CHAR) {
		if(membreGauche->typer() != type) {
			// attention, le destructeur de cast est appelé à la sortie de ce contexte
			ExpCast cast(membreGauche, type);
			varLeft = cast.buildIR(cfg);
		} else {
			varLeft = membreGauche->buildIR(cfg);
		}
		if(membreDroit->typer() != type) {
			ExpCast cast(membreDroit, type);
			varRight = cast.buildIR(cfg);
		} else {
			varRight = membreDroit->buildIR(cfg);
		}
	}

	BasicBlock* bb = cfg->current_bb;
	vector<string> vec = { varBin, varLeft, varRight };
	IRInstr* instr;
	switch (this->op) {
	case PLUS:
		instr = new Add(bb, type, vec);
		break;
	case MOINS:
		instr = new Sub(bb, type, vec);
		break;
	case MULT:
		instr = new Mul(bb, type, vec);
		break;
	case DIV:
		instr = new Div(bb, type, vec);
		break;
	case MODULO:
		if(type == CHAR) {
			// Jamais rien fait d'aussi horrible, mais MOD ne marche pas 
			// si les 2 opérandes sont des char for some reason
			ExpCast cast(membreDroit, INT64_T);
			varRight = cast.buildIR(cfg);
			ExpCast cast2(membreGauche, INT64_T);
			varLeft = cast2.buildIR(cfg);
			vec = { varBin, varLeft, varRight };
		}
		instr = new Mod(bb, type, vec);
		break;
	case LEQ:
		instr = new Cmp_le(bb, type, vec);
		break;
	case NEQ:
		instr = new Cmp_neq(bb, type, vec);
		break;
	case GEQ:
		instr = new Cmp_ge(bb, type, vec);
		break;
	case EQ:
		instr = new Cmp_eq(bb, type, vec);
		break;
	case GT:
		instr = new Cmp_gt(bb, type, vec);
		break;
	case LT:
		instr = new Cmp_lt(bb, type, vec);
		break;
	case ORB:
		instr = new Orbit(bb, type, vec);
		break;
	case ANDB:
		instr = new Andbit(bb, type, vec);
		break;
	case XORB:
		instr = new Xorbit(bb, type, vec);
		break;
	}
	cfg->current_bb->add_IRInstr(instr);
	return varBin;
}
