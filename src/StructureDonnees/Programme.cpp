#include "Programme.h"

Programme::Programme() {
}

vector<Fonction*> Programme::getFonctions ()
{
	return fonctions;
}

vector<Declaration*> Programme::getDeclarations ()
{
	return declarations;
}

map<string, std::pair<Fonction*, bool>> & Programme::getMapFonctions ()
{
	return mapFonctions;
}

void Programme::addFonction(Fonction * fonction) {
	fonctions.push_back (fonction);
}
	
void Programme::addDeclaration(Declaration * declaration) {
    declarations.push_back(declaration);
}

ostream& operator <<(ostream& Stream, const Programme& Obj) 
{ 
	Stream << "Programme {" << endl;
    Stream << "    Fonctions : " << endl;
    for(auto fonction : Obj.fonctions) {
		
        Stream << "        - " << *fonction << endl;
    }
    Stream << "    Déclarations : " << endl;
    for(int i = 0; i < Obj.declarations.size(); i++) {
        Stream << "        - " << *Obj.declarations[i] << endl;     
    }
    Stream << "}";
    return Stream;
} 