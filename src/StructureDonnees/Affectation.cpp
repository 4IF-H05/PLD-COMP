#include "Affectation.h"
#include "../Backend/IR.h"
#include "VariableSimple.h"
#include "../MainVisitor.h"
#include "ExpCast.h"
#include "VariableTableau.h"

Affectation::Affectation(Lvalue *variable, Expression *exp, Type expected) {
    this->expected = expected;
    this->variable = variable;
    valeur = exp;
}

Affectation::Affectation(Lvalue *variable, Expression *exp) {
    this->variable = variable;
    valeur = exp;
}

void Affectation::setValeur(Expression *exp) {
    valeur = exp;
}

Expression *Affectation::getValeur() {
    return valeur;
}

void Affectation::setVariable(Lvalue *lvalue) {
    variable = lvalue;
}

Lvalue *Affectation::getVariable() {
    return variable;
}

Type Affectation::typer() const {
    return (expected == ERROR_TYPE) ? variable->typer() : expected;
}

string Affectation::print() const {
    return "Affectation (" + variable->print() + ", " + valeur->print() + ")";
}

vector<Expression *> Affectation::getSubExpressions() {
    auto res = valeur->getSubExpressions();
    res.push_back(this);
    auto res2 = variable->getSubExpressions();
    res.insert(res.end(), res2.begin(), res2.end());

    return res;
}

string Affectation::buildIR(CFG *cfg) {
    Type type = this->typer();

    string right;

    // III.5.2
    VariableSimple *variableSimple = dynamic_cast<VariableSimple *>(this->variable);
    if (variableSimple) {
        string left = this->variable->buildIR(cfg);
        if (valeur->typer() != type) {
            ExpCast cast(valeur, type);
            right = cast.buildIR(cfg);
        } else {
            right = valeur->buildIR(cfg);
        }
        vector<string> vec = {left, right};
        cfg->current_bb->add_IRInstr(new Copy(cfg->current_bb, type, vec));
    } else {
        // variable tableau
        // III.5.3
        VariableTableau *variabletableau = dynamic_cast<VariableTableau *>(this->variable);
        variabletableau->setIsRvalue(false);
        type = this->typer();
        if (valeur->typer() != type) {
            ExpCast cast(valeur, type);
            right = cast.buildIR(cfg);
        } else {
            right = valeur->buildIR(cfg);
        }
        string left = variable->buildIR(cfg);
        vector<string> vec = {left, right};
        cfg->current_bb->add_IRInstr(new Wmem(cfg->current_bb, type, vec));
    }

    return right;
}
