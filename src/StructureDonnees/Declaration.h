#pragma once

using namespace std;

#include <iostream>
#include "Types.h"
#include "Instruction.h"
#include "Expression.h"
#include "Lvalue.h"

class Lvalue;

// Déclaration d'une variable
class Declaration : public Instruction {
	friend ostream& operator <<(ostream&, const Declaration&);

public:
	Declaration(Type & type);
	Declaration(Type & type, string variable, Expression * valeur = nullptr);
	~Declaration();

	Expression* getValeur();
	void setValeur(Expression * valeur);
	string getNomVariable();
	void setNomVariable(string variable);
	Type getType();
	void setType(Type & type);
	virtual vector<Expression*> getSubExpressions () override;
	virtual string print () const;
	virtual bool isDeclaration () override { return true; }
	virtual bool isDeclarationTableau () { return false; }
	virtual void setTypeRetourFonction(Type type){}
	virtual string buildIR(CFG* cfg);

protected:
	Type type;
	Expression * valeur;
	string nomVar;

};