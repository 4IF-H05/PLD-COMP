#include "Instruction.h"
#include "Expression.h"

ostream& operator <<(ostream& Stream, const Instruction& Obj)
{
	Stream << Obj.print();

	return Stream;
}

string Instruction::print () const
{
	return "Instructions";
}

vector<Expression*> Instruction::getSubExpressions ()
{
	return vector<Expression*> ();
}

