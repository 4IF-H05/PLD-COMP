#pragma once

using namespace std;

#include "Expression.h"
#include "../Backend/IR.h"

//Constante nommée ou anonyme

class Constante : public Expression {
public:
	Constante();
	virtual long eval() = 0;
	virtual vector<Expression*> getSubExpressions () override;


};