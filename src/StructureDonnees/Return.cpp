#include "Return.h"
#include "Expression.h"
#include "../Backend/IR.h"
#include "ExpCast.h"
#include "../Backend/registers.h"


Return::Return(Expression * toReturn) : Instruction()
{
	this->toReturn = toReturn;
}

Return::~Return()
{
	//delete toReturn;
}

string Return::print () const
{
	return string("Returns ") + toReturn->print();
}

vector<Expression*> Return::getSubExpressions()
{
	return toReturn->getSubExpressions();
}

string Return::buildIR(CFG *cfg) {
	if (this->typeRetour != VOID)
	{
		string toReturn;
		vector<string> vec;

		if (this->toReturn->typer () != typeRetour) {
			ExpCast cast (this->toReturn, typeRetour);
			toReturn = cast.buildIR (cfg);
		}
		else {
			toReturn = this->toReturn->buildIR (cfg);
		}

		BasicBlock* bb = cfg->current_bb;

		vec = { string ("%") + workReg1[typeRetour], toReturn };
		bb->add_IRInstr (new Copy (bb, this->typeRetour, vec));
		vec = { cfg->ast->getNom () };
		bb->add_IRInstr (new Ret (bb, this->typeRetour, vec));

		return toReturn;
	}

	BasicBlock* bb = cfg->current_bb;
	vector<string> vec;
	vec = { string ("%") + workReg1[INT32_T], "$0" };
	bb->add_IRInstr (new Copy (bb, INT32_T, vec));
	vec = { cfg->ast->getNom () };
	bb->add_IRInstr (new Ret (bb, this->typeRetour, vec));
	return "$0";
}

void Return::setTypeRetourFonction(Type type) {
	this->typeRetour = type;
}
