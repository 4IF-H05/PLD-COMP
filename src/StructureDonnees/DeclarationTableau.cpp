#include "DeclarationTableau.h"
#include "VariableTableau.h"
#include "../Backend/IR.h"
#include "VariableSimple.h"
#include "Affectation.h"
#include "ConstanteI64.h"
#include "../Backend/registers.h"


DeclarationTableau::DeclarationTableau(Type &type) : Declaration(type) {
    this->type = type;
    valeur = NULL;
}


DeclarationTableau::DeclarationTableau(Type &type, VariableTableau *variable, vector<Expression *> initialisation)
        : Declaration(type) {
    this->nomVar = variable->getNomVariable();
    this->longueurTableau = variable->getIndice();
    this->initialisations = initialisation;
    delete variable;
}

DeclarationTableau::DeclarationTableau(Type &type, VariableTableau *variable) : Declaration(type) {
    this->nomVar = variable->getNomVariable();
    this->longueurTableau = variable->getIndice();
}

DeclarationTableau::~DeclarationTableau() {
    //if (valeur) delete valeur;
}

Expression *DeclarationTableau::getValeur() {
    return valeur;
}

void DeclarationTableau::setValeur(Expression *valeur) {
    this->valeur = valeur;
}

vector<Expression *> DeclarationTableau::getSubExpressions() {
	auto res = longueurTableau->getSubExpressions ();
	for (auto init : initialisations)
	{
		auto res2 = init->getSubExpressions ();
		res.insert (res.end (), res2.begin (), res2.end ());
	}
    return res;
}

string DeclarationTableau::print() const {
    string str = "DeclarationTab_";
    str += Types[type];
    str += '(';
    str += nomVar;
    str += ')';
    // commenté car la longueur peut être null
    str += "[" + ((Instruction *) longueurTableau)->print() + "]";

    return str;
}

ostream &operator<<(ostream &Stream, const DeclarationTableau &Obj) {
    Stream << "DeclarationTableau [type : " << Types[Obj.type] << "]";
    return Stream;
}

string DeclarationTableau::buildIR(CFG *cfg) {
    Type type = this->type;
    cfg->add_to_symbol_table(this->getNomVariable(), type, longueurTableau->eval());
	arrayAddress = cfg->create_new_tempvar (INT64_T);
	Leaq *leaq = new Leaq (cfg->current_bb, INT64_T, { arrayAddress, this->getNomVariable ()});
	cfg->current_bb->add_IRInstr (leaq);

    int i = 0;
    for (auto var : initialisations) {
        VariableTableau *vt = new VariableTableau(getNomVariable(), new ConstanteI64(i));
        vt->setDeclaration(this);
        Affectation affectation = Affectation(vt, var);
        affectation.buildIR(cfg);
        delete vt;
        i++;
    }

    return this->getNomVariable();
}

Expression *DeclarationTableau::getLongueurTableau() const {
    return longueurTableau;
}

const vector<Expression *> DeclarationTableau::getInitialisations() const {
    return initialisations;
}
