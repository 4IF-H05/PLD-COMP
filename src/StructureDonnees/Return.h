#pragma once

#include <vector>
#include <iostream>
#include "Instruction.h"
#include "Types.h"

class Expression;

using namespace std;

class Return : public Instruction{

public:
	Return(Expression* toReturn);
	~Return();
	virtual string print () const;
	virtual vector<Expression*> getSubExpressions() override;
	virtual string buildIR(CFG * cfg);
	virtual void setTypeRetourFonction(Type type);


private:
	Expression* toReturn;
	Type typeRetour;
};