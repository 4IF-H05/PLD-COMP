#pragma once

using namespace std;

#include <string>

enum OpUnaire { INC, DEC, SIZEE, SIZET, NEG, NEGBIN, NON, LINC, LDEC };
enum OpBinaire { PLUS, MOINS, MULT, DIV, MODULO, LEQ, GEQ, NEQ, EQ, GT, LT, ORB, ANDB, XORB, OR, AND };

const string OpUnaires[] = { "++", "--" , "sizeof", "sizeof", "-", "~", "!", "++", "--" };
const string OpBinaires[] = { "+", "-", "*", "/", "%", "<=", ">=", "!=", "==", ">", "<", "|", "&","^", "||", "&&"};
