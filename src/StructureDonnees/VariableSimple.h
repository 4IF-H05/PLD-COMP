#pragma once

using namespace std;

#include "Lvalue.h"

class VariableSimple : public Lvalue {

public:
	VariableSimple(string nom);

	virtual string print () const;
	virtual string buildIR(CFG * cfg);
	
};