#pragma once
using namespace std;

#include "Constante.h"

//Constante nommée ou anonyme 32b

class ConstanteI32 : public Constante {

private:
	int value;

public:
	ConstanteI32();
	ConstanteI32(int value);
	virtual long eval();
	virtual Type typer() const override { return INT32_T; };

	virtual string print () const;
	string buildIR(CFG * cfg) override;
};