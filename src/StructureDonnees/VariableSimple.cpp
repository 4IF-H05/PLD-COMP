#include "VariableSimple.h"
#include "../Backend/IR.h"

VariableSimple::VariableSimple(string nom) : Lvalue(nom)
{

}

string VariableSimple::print () const
{
	string str = "VarSimple";
	str += "(";
	str += nomVariable;
	str += ')';

	return str;
}

string VariableSimple::buildIR(CFG * cfg)
{
    return nomVariable;
}

