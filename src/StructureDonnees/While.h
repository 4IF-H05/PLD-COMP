#pragma once

using namespace std;

#include "Controle.h"

class  While : public Controle {
public:
	While(Expression* clause, Instruction* corps);
	virtual string print () const;

	virtual string buildIR(CFG * cfg);
};
