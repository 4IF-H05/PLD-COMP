#include "ExpUnaireType.h"
#include "../Backend/IR.h"
#include "../Backend/asmTypes.h"

ExpUnaireType::ExpUnaireType(OpUnaire  opUnaire,Type argument)
{
	setOperateur(opUnaire);
	setArgument(argument);
}


OpUnaire ExpUnaireType::getOperateur()
{
	return op;
}

void ExpUnaireType::setOperateur(OpUnaire operateur)
{
	this->op = operateur;
}

Type ExpUnaireType::getArgument()
{
	return argument;
}

void ExpUnaireType::setArgument(Type argument)
{
	this->argument = argument;
}

string ExpUnaireType::print () const
{
	string str = "ExprUnType_";
	str += OpUnaires[op];
	str += '(';
	this->typer ();
	str += Types[typer()];
	str += ')';
	return string ();
}

string ExpUnaireType::buildIR(CFG * cfg) {

	Type type = this->argument;
	BasicBlock* bb = cfg->current_bb;
	string varName = cfg->create_new_tempvar(type);
	vector<string> vec = { varName, std::to_string(asmSizes[type]) };
	cfg->current_bb->add_IRInstr(new Ldconst(bb, INT32_T, vec));
	return varName;

}