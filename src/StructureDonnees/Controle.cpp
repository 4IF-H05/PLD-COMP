#include "Controle.h"

Controle::Controle(Instruction * instruction, Expression * clause) : instruction(instruction), clause(clause)
{
}

Controle::~Controle()
{
	//delete instruction;
	//delete clause;
}

void Controle::setInstruction(Instruction * instr)
{
	this->instruction = instr;
}

Instruction * Controle::getInstruction()
{
	return instruction;
}

void Controle::setClause(Expression * clause)
{
	this->clause = clause;
}

Expression * Controle::getClause()
{
	return clause;
}

vector<Expression*> Controle::getSubExpressions ()
{
	auto res = instruction->getSubExpressions();
	auto res2 = clause->getSubExpressions ();
	res.insert (res.end (), res2.begin (), res2.end ());
	
	return res;
}

void Controle::setTypeRetourFonction(Type type) {
	instruction->setTypeRetourFonction(type);
}
