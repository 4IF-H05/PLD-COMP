#include "Declaration.h"
#include "../Backend/IR.h"
#include "VariableSimple.h"
#include "Affectation.h"

Declaration::Declaration(Type & type)
{
	this->type = type;
	valeur = nullptr;
}


Declaration::Declaration(Type & type, string variable, Expression * valeur)
{
	this->type = type;
	this->valeur = valeur;
	this->nomVar = variable;
}

Declaration::~Declaration()
{
	//if (valeur)	delete valeur;
}

Expression * Declaration::getValeur()
{
	return valeur;
}

void Declaration::setValeur(Expression * valeur)
{
	this->valeur = valeur;
}


vector<Expression*> Declaration::getSubExpressions ()
{
	if(valeur==NULL) {
		return vector<Expression*>();
	}else{
		auto res = valeur->getSubExpressions();
		return res;
	}
}

string Declaration::getNomVariable() {
	return this->nomVar ;
}

void Declaration::setNomVariable(string variable) {
	this->nomVar = variable;
}

Type Declaration::getType()
{
	return type;
}

void Declaration::setType(Type & type)
{
	this->type = type;
}

string Declaration::print () const
{
	string str = "Declaration_";
	str += '(';
	str += nomVar;
	str += ')';

	return str;
}

ostream& operator <<(ostream& Stream, const Declaration& Obj)
{
	Stream << "Declaration [type : " << Types[Obj.type] << "]";
	if(Obj.valeur != nullptr) {
		Stream << " : " << Obj.valeur->eval();
	}
	return Stream;
}

string Declaration::buildIR(CFG* cfg) {
	cfg->add_to_symbol_table(this->getNomVariable(), this->type);
	if (valeur != nullptr) {
		VariableSimple* vs = new VariableSimple(getNomVariable());
		vs->setDeclaration(this);
		Affectation affectation(vs, valeur, type);
		affectation.buildIR(cfg);
		delete vs;
	}

	return this->getNomVariable();
}