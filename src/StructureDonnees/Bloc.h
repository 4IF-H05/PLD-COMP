#pragma once

using namespace std;

#include <iostream>
#include <vector>
#include "Instruction.h"

// Bloc d'instructions

class Bloc : public Instruction {
friend ostream& operator <<(ostream&, const Bloc&); 

public:
	Bloc(vector<Instruction*> instructions);
	Bloc();
	vector<Instruction*>& getInstructions() ;
	void setInstructions(vector<Instruction*> & instructions);
	void addInstruction(Instruction* instr);

	virtual vector<Expression*> getSubExpressions () override;
	virtual string print () const;

	virtual string buildIR(CFG* cfg);
	virtual void setTypeRetourFonction(Type type);

private:
    vector<Instruction*> instructions;
};