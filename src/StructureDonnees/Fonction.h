#pragma once

using namespace std;

#include <iostream>
#include <string>
#include <vector>
#include <map>
#include "Declaration.h"
#include "Bloc.h"
#include "Types.h"
#include "Parametre.h"

// Définition d'une fonction

class  Fonction {
friend ostream& operator <<(ostream&, const Fonction&); 
friend class RangeChecker; // Ce n'est pas légal dans un pays civilisé comme la France. Il aurait mieux valu implémenter les getters pas finis ...

public:
	Fonction() {}
	Fonction(string & nom);
	//Fonction(string & nom, Bloc & corps, Type & typeRetour, vector<Parametre> & params);

	string& getNom();
	void setNom(const string& nom);
	
	Bloc& getCorps();
	void setCorps(const Bloc& bloc);

	Type& getTypeRetour();
	void setTypeRetour(const Type& type);

	vector<Declaration*>& getParams();
	void setParams(const vector<Declaration*> & params);
	void addParam(Declaration* param);

	virtual string print () const;

private:
    string nom;
    Bloc corps;
    Type typeRetour;
    vector<Declaration*> params;

	// map<nomVariable, tuple<paramètre*, utilisé>>
	map<string, tuple<Declaration*, bool>> parametres;
	// map<nomVariable, tuple<declaration*, pair<initialisée, index>, utilisée>>
	map<string, tuple<Declaration*, pair<bool, int>, bool>> variablesLocales;

};
