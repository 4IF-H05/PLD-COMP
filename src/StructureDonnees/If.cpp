#include "If.h"
#include "../Backend/IR.h"
#include "../Backend/registers.h"

If::If(Expression * clause, Instruction * corps, Instruction * instrElse) : Controle(corps, clause), instrElse(instrElse)
{
}

If::If(Expression * clause, Instruction * corps): Controle(corps, clause)
{
}

If::~If()
{
	//delete instrElse;
}

Instruction * If::getInstrElse()
{
	return instrElse;
}

void If::setInstrElse(Instruction * intrElse)
{
	this->instrElse = intrElse;
}

string If::print() const
{
	string str = "if(";
	str += clause->print ();
	str += ")\n";
	str += instruction->print ();
	str += "\n";
	if (instrElse)
	{
		str += "else\n";
		str += instrElse->print ();
		str += "\n";
	}
	return str;
}

string If::buildIR(CFG * cfg) {
	bool doElse = instrElse != nullptr;
	BasicBlock * testBB = cfg->current_bb;
	BasicBlock * thenBB = new BasicBlock(cfg, cfg->new_BB_name());	
	BasicBlock * elseBB;
	if(doElse)
		elseBB = new BasicBlock(cfg, cfg->new_BB_name());
	BasicBlock * afterBB = new BasicBlock(cfg, cfg->new_BB_name());

	// Condition
	string cond = clause->buildIR(cfg);
	vector<string> vec = { string("%") + workReg1[clause->typer()], cond, "$0" };
	cfg->current_bb->add_IRInstr(new Cmp_eq(cfg->current_bb, clause->typer(), vec));
	
	// Branchement testBB
	cfg->current_bb->exit_true = thenBB;
	if(doElse)
		cfg->current_bb->exit_false = elseBB;
	else
		cfg->current_bb->exit_false = afterBB;

	// Gestion thenBB (dont branchement)
	thenBB->buildIR(cfg, getInstruction());
	cfg->current_bb->exit_true = afterBB;
	cfg->current_bb->exit_false = nullptr;

	// Gestion elseBB (dont branchement)
	if(doElse) {
		elseBB->buildIR(cfg, instrElse);
		cfg->current_bb->exit_true = afterBB;
		cfg->current_bb->exit_false = nullptr;
	}

	// afterBB
	cfg->add_bb(afterBB);

	return cond;
}

void If::setTypeRetourFonction(Type type) {
	instruction->setTypeRetourFonction(type);
	if(instrElse)
		instrElse->setTypeRetourFonction(type);
}

vector<Expression*> If::getSubExpressions ()
{
	auto res = instruction->getSubExpressions();
	if(instrElse) {
		auto res3 = instrElse->getSubExpressions();
		res.insert (res.end (), res3.begin (), res3.end ());
	}
	auto res2 = clause->getSubExpressions ();
	res.insert (res.end (), res2.begin (), res2.end ());
	
	return res;
}