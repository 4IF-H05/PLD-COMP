#include "ConstanteI64.h"
#include "Constante.h"

ConstanteI64::ConstanteI64() : Constante()
{
}

ConstanteI64::ConstanteI64(long int value) : Constante(), value(value)
{
}

long ConstanteI64::eval() 
{
	return (long) value;
}

string ConstanteI64::print () const
{
	string str = "I64(";
	str += std::to_string(value);
	str += ")";
	return str;
}

string ConstanteI64::buildIR(CFG * cfg) {
	Type type = this->typer();
	string varName = cfg->create_new_tempvar(type);
	BasicBlock* bb = cfg->current_bb;
	vector<string> vec = { varName, to_string(value) };
	cfg->current_bb->add_IRInstr(new Ldconst(bb, type, vec));
	return varName;
}
