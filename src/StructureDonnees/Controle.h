#pragma once

using namespace std;

#include "Expression.h"
#include "Instruction.h"

// Contrôle de boucle ou conditionnel
class  Controle  : public Instruction{
protected:
    Instruction* instruction;
    Expression* clause;

public:
	Controle(Instruction * instruction, Expression * clause);
	~Controle();
	void setInstruction(Instruction * instr);
	Instruction* getInstruction();

	void setClause(Expression * clause);
	Expression* getClause();
	virtual void setTypeRetourFonction(Type type);
	virtual vector<Expression*> getSubExpressions () override;
};
