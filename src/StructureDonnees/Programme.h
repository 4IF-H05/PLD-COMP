#pragma once

using namespace std;

#include <vector>
#include <tuple>
#include <map>
#include "Fonction.h"
#include "Declaration.h"

// Un programme C-- constitué de fonctions et de déclarations

class  Programme {
friend ostream& operator <<(ostream&, const Programme&); 
friend class RangeChecker;

public:
	//Programme(vector<Fonction> & fonctions, vector<Declaration> & declarations);
	Programme();

	vector<Fonction*> getFonctions() ;
	map<string, std::pair<Fonction*, bool>>  & getMapFonctions ();
	void setFonctions(const vector<Fonction> & fonctions);
	void addFonction(Fonction * fonction);

	vector<Declaration*> getDeclarations() ;
	void setDeclarations(const vector<Declaration> & declarations);
	void addDeclaration(Declaration * declaration);

private:
	vector<Fonction*> fonctions;

	// map<nomFonction, tuple<Fonction*, utilisée>>
	map<string, std::pair<Fonction*, bool>>  mapFonctions;
    vector<Declaration*> declarations;

	// map<nomVariable, tuple<declaration*, inital, utilisée>>
	map<string, tuple<Declaration*, bool>> variablesGlobales;

};
