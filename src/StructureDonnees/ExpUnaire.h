#pragma once

using namespace std;

#include "Expression.h"
#include "Operateurs.h"

//Expression à une seule opérande

class ExpUnaire : public Expression {
private:
    OpUnaire op;
    Expression* argument;

public:
	ExpUnaire(OpUnaire  opUnaire, Expression * argument);
	~ExpUnaire();

	OpUnaire getOperateur() ;
	void setOperateur( OpUnaire operateur);

	Expression* getArgument() ;
	void setArgument( Expression* argument);

	Type typer() const override {
		return argument->typer();
	}

	virtual string print () const;
	virtual vector<Expression*> getSubExpressions () override;
	string buildIR(CFG * cfg) override;
};
