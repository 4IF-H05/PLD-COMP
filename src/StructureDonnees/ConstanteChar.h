#pragma once

using namespace std;

#include "Constante.h"

//Constante nommée ou anonyme char

class ConstanteChar : public Constante {

private:
	char value;

public:
	ConstanteChar();
	ConstanteChar(char value);
	virtual long eval();
	virtual Type typer() const override { return CHAR; };

	virtual string print () const;
	string buildIR(CFG * cfg) override;
};