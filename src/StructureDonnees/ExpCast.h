#pragma once

using namespace std;

#include "Expression.h"
#include "Types.h"

//Expression de cast d'une expression

class ExpCast : public Expression {
private:
	Type toCast;
	Expression* argument;

public:
	ExpCast(Expression * argument, Type toCast);
	~ExpCast();

	virtual string print () const;

	Expression* getArgument();
	void setArgument(Expression* argument);

	Type typer() const override {
		return toCast;
	}

	virtual vector<Expression*> getSubExpressions() override {
		return argument->getSubExpressions();
	};

	virtual string buildIR(CFG * cfg);
};
