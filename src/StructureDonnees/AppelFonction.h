#pragma once

using namespace std;

#include <vector>
#include "Expression.h"
#include "Fonction.h"
#include "ParamAppel.h"

//Appel d'une fonction (pas sa définition)

class AppelFonction : public Expression {
friend ostream& operator <<(ostream&, const AppelFonction&);

public:
	AppelFonction (string nomFct) : nomFonction (nomFct) {}
	AppelFonction(Fonction * fonction, vector<Expression*>& parametres);
	Fonction& getFonction() ;
	void setFonction(Fonction* fonction);
	
	vector<Expression*>& getsParams() ;
	void setParams(vector<Expression*> params);
	void addParam(Expression* param);

	virtual string print () const;

	virtual Type typer () const override;

	virtual bool isAppelFonction () override { return true; }

	string getNomFonction () { return nomFonction; }

	virtual string buildIR(CFG * cfg);

	virtual vector<Expression*> getSubExpressions () override;
	
private:
	string nomFonction;
    Fonction *fonction;
    vector<Expression*> parametres;

};