#include "ConstanteI32.h"
#include "Constante.h"

ConstanteI32::ConstanteI32() : Constante()
{
}

ConstanteI32::ConstanteI32(int value) : value(value)
{
}

long ConstanteI32::eval() 
{
	return (long) value;
}

string ConstanteI32::print () const
{
	string str = "I32(";
	str += std::to_string(value);
	str += ')';
	return str;
}

string ConstanteI32::buildIR(CFG * cfg) {
	Type type = this->typer();
	string varName = cfg->create_new_tempvar(type);
	BasicBlock* bb = cfg->current_bb;
	vector<string> vec = { varName, to_string(value)};
	cfg->current_bb->add_IRInstr(new Ldconst(bb, type, vec));
	return varName;
}
