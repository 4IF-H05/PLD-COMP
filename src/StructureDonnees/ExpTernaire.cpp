#include "ExpTernaire.h"
#include "ExpCast.h"
#include "../Backend/registers.h"

ExpTernaire::ExpTernaire(Expression * cond, Expression * valueTrue, Expression * valueFalse) : 
	Expression(), condition(cond), valueTrue(valueTrue), valueFalse(valueFalse)
{
}

ExpTernaire::~ExpTernaire()
{
	//delete condition;
	//delete valueTrue;
	//delete valueFalse;
}

Expression * ExpTernaire::getCondition()
{
	return condition;
}

void ExpTernaire::setCondition(Expression * condition)
{
	this->condition = condition;
}

Expression * ExpTernaire::getValueTrue()
{
	return valueTrue;
}

void ExpTernaire::setValueFalse(Expression * valueFalse)
{
	this->valueFalse = valueFalse;
}

string ExpTernaire::print () const
{
	string str = "ExpTernaire_(";
	str += condition->print () + ", ";
	str += valueTrue->print () + ", ";
	str += valueFalse->print () + ')';
	return str;
}

vector<Expression*> ExpTernaire::getSubExpressions ()
{
	auto res = condition->getSubExpressions ();
	auto res2 = valueTrue->getSubExpressions ();
	auto res3 = valueFalse->getSubExpressions ();
	res.insert (res.end (), res2.begin (), res2.end ());
	res.insert (res.end (), res3.begin (), res3.end ());
	return res;
}

Expression * ExpTernaire::getValueFalse()
{
	return valueFalse;
}

void ExpTernaire::setValueTrue(Expression * valueTrue)
{
	this->valueTrue = valueTrue;
}

string ExpTernaire::buildIR(CFG * cfg) {
	Type type = typer();
	string varTrue;
	string varFalse;
	string out = cfg->create_new_tempvar(type);
	BasicBlock * testBB = cfg->current_bb;
	BasicBlock * trueBB = new BasicBlock(cfg, cfg->new_BB_name());
	BasicBlock * falseBB = new BasicBlock(cfg, cfg->new_BB_name());
	BasicBlock * afterBB = new BasicBlock(cfg, cfg->new_BB_name());
	
	string cond = condition->buildIR(cfg);

	vector<string> vec = { string ("%") + workReg1[type], cond, "$0" };
	cfg->current_bb->add_IRInstr (new Cmp_eq (cfg->current_bb, type, vec));
	cfg->current_bb->exit_false = falseBB;
	cfg->current_bb->exit_true = trueBB;

	cfg->add_bb(trueBB);

	if(valueTrue->typer() != type) {
		ExpCast cast(valueTrue, type);
		varTrue = cast.buildIR(cfg);
	} else {
		varTrue = valueTrue->buildIR(cfg);
	}

	cfg->current_bb->add_IRInstr(new Copy (cfg->current_bb, type, { out, varTrue }));
	cfg->current_bb->exit_false = nullptr;
	cfg->current_bb->exit_true = afterBB;

	cfg->add_bb(falseBB);

	if(valueFalse->typer() != type) {
		ExpCast cast(valueFalse, type);
		varFalse = cast.buildIR(cfg);
	} else {
		varFalse = valueFalse->buildIR(cfg);
	}

	cfg->current_bb->add_IRInstr (new Copy (cfg->current_bb, type, { out, varFalse }));
	cfg->current_bb->exit_false = nullptr;
	cfg->current_bb->exit_true = afterBB;

	cfg->add_bb(afterBB);

	return out;
}