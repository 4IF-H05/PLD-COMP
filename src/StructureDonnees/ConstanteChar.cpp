#include "ConstanteChar.h"
#include "Constante.h"

ConstanteChar::ConstanteChar() : Constante()
{
}

ConstanteChar::ConstanteChar(char value) : Constante(), value(value)
{
}

long ConstanteChar::eval()
{
	return (long) value;
}

string ConstanteChar::print () const
{
	string str = "Char(";
	str += value;
	str += ")";
	return str;
}

string ConstanteChar::buildIR(CFG * cfg) {
	Type type = this->typer();
	string varName = cfg->create_new_tempvar(type);
	BasicBlock* bb = cfg->current_bb;
	vector<string> vec = { varName, to_string(value)};
	cfg->current_bb->add_IRInstr(new Ldconst(bb, type, vec));
	
	return varName;
}
