#include "ExpUnaire.h"
#include "ExpCast.h"
#include "VariableTableau.h"
#include "VariableSimple.h"
#include "DeclarationTableau.h"
#include "../Backend/IR.h"
#include "../Backend/asmTypes.h"

ExpUnaire::ExpUnaire(OpUnaire  opUnaire, Expression* argument) : Expression()
{
	setOperateur(opUnaire);
	setArgument(argument);
}

ExpUnaire::~ExpUnaire()
{
	//delete argument;
}

OpUnaire ExpUnaire::getOperateur()
{
	return op;
}

void ExpUnaire::setOperateur(OpUnaire operateur)
{
	this->op = operateur;
}

Expression* ExpUnaire::getArgument()
{
	return argument;
}

void ExpUnaire::setArgument(Expression* argument)
{
	this->argument = argument;
}

string ExpUnaire::print () const
{
	string str = "ExprUn_";
	str += OpUnaires[op];
	str += '(';
	str += argument->print ();
	str += ')';
	return string();
}

vector<Expression*> ExpUnaire::getSubExpressions ()
{
	return argument->getSubExpressions ();
}

string ExpUnaire::buildIR(CFG * cfg)
{
	string variable;
	Type type = typer();
	string varUn;

	if(this->op != LINC && this->op != LDEC) // Même pas besoin de nouvelle variable dans ces cas
		varUn = cfg->create_new_tempvar(type);

	IRInstr* instr;

	if(argument->typer() != type) {
		ExpCast cast(argument, type);
		variable = cast.buildIR(cfg);
	} else {
		variable = argument->buildIR(cfg);
	}

	BasicBlock* bb = cfg->current_bb;

	switch (this->op) {
		case NEG:
			instr = new Sub(bb, type, { varUn, "$0", variable });
			break;
		case NON:
			instr = new Cmp_eq(bb, type, { varUn, "$0", variable });
			break;
		case NEGBIN:
			instr = new Negbit(bb, type, { varUn, variable });
			break;
		case INC: 
			instr = new Rinc(bb, type, { varUn, variable }); 
			break;
		case DEC: 
			instr = new Rdec(bb, type, { varUn, variable }); 
			break;
		case LINC: 
			instr = new Linc(bb, type, { variable }); 
			cfg->current_bb->add_IRInstr(instr);
			return variable;
			break;
		case LDEC: 
			instr = new Ldec(bb, type, { variable }); 
			cfg->current_bb->add_IRInstr(instr);
			return variable;
			break;
		case SIZEE:
			int size;
			VariableSimple* vs = dynamic_cast<VariableSimple*>(argument);
			if (vs) {
				Declaration* decl = vs->getDeclaration();
				if (DeclarationTableau* dt = dynamic_cast<DeclarationTableau*>(decl)) {		//variable tableau ?
					long arraySize = dt->getLongueurTableau()->eval();
					size = arraySize * asmSizes[decl->getType()];
				}
				else {
					//raté, variable simple ?
					size = asmSizes[vs->getDeclaration()->getType()];
				}
			}
			else {
				VariableTableau* vt = dynamic_cast<VariableTableau*>(argument);
				Declaration* decl = vt->getDeclaration();
				if (DeclarationTableau* dt = dynamic_cast<DeclarationTableau*>(decl)) {		//variable tableau ?
					long arraySize = dt->getLongueurTableau()->eval();
					size = asmSizes[decl->getType()];
				}

			}
			instr = new Ldconst(bb, type, { varUn, std::to_string(size)});
			break;
	}

	cfg->current_bb->add_IRInstr(instr);
	return varUn;
}

