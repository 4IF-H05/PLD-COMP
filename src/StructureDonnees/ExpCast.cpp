#include "ExpCast.h"
#include "../Backend/IR.h"

ExpCast::ExpCast(Expression * argument, Type toCast) : Expression(), toCast(toCast), argument(argument)
{
}

ExpCast::~ExpCast()
{
	//delete argument;
}

string ExpCast::print () const
{
	string str = "Cast_";
	str += Types[toCast];
	str += '(';
	str += argument->print ();
	str += ')';
	return str;
}

Expression * ExpCast::getArgument()
{
	return argument;
}

void ExpCast::setArgument(Expression * argument)
{
	this->argument = argument;
}

string ExpCast::buildIR(CFG * cfg) {
	string in = argument->buildIR(cfg);

	if(argument->typer() != toCast) {
		string out = cfg->create_new_tempvar(toCast);

		vector<string> params = {
			to_string(argument->typer()),
			in,
			out
		};

		cfg->current_bb->add_IRInstr(
			new Cast(
				cfg->current_bb,
				toCast,
				params
			)
		);

		return out;
	}

	return in;
}
