#pragma once

using namespace std;

#include "Expression.h"
#include "Operateurs.h"

//Expression dont la seule opérande est un type

class ExpUnaireType : public Expression {
private:
    OpUnaire op;
    Type argument;

public:
	ExpUnaireType(OpUnaire  opUnaire, Type argument);

	OpUnaire getOperateur() ;
	void setOperateur( OpUnaire operateur);

	Type getArgument() ;
	void setArgument(Type argument);

	virtual string print () const;

	Type typer() const override {
		return INT32_T;	// bon soyons réalistes, il y a simplement sizeof(type) qui sera concernée par cette méthode...
	}
	string buildIR(CFG * cfg) override;	
};
