#pragma once

using namespace std;

#include "Lvalue.h"

class VariableTableau : public Lvalue {

public:
	VariableTableau(string nom, Expression * expr);
	virtual string print () const;
	virtual vector<Expression*> getSubExpressions () override;
	Expression *getIndice() const;
	void setIndice(Expression *indice);
	void setIsRvalue(bool isRvalue);
	virtual string buildIR(CFG * cfg);
	virtual bool isVariableTableau () override { return true; }

private:
	Expression* indice;
	bool isRvalue = true;

};