#include "Lvalue.h"

Type Lvalue::typer () const
{
	if (declaration)
	{
		return declaration->getType ();
	}
	return ERROR_TYPE;
}

string & Lvalue::getNomVariable()
{
	return this->nomVariable;
}

void Lvalue::setNomVariable(string & nom)
{
	this->nomVariable = nom;
}
 
//Constructeur par défaut
Lvalue::Lvalue() {

}

Lvalue::Lvalue(string & nomVariable)
{
	this->setNomVariable(nomVariable);
}

vector<Expression*> Lvalue::getSubExpressions ()
{
	return { this };
}

void Lvalue::setDeclaration(Declaration* decl) {
	declaration = decl;
}
