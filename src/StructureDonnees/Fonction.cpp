#include "Fonction.h"

Fonction::Fonction(string & nom) {
	this->nom = nom;
}

string & Fonction::getNom ()
{
	return nom;
}

ostream& operator <<(ostream& Stream, const Fonction& Obj)
{
	Stream << "Fonction [nom : " << Obj.nom << "; type_retour : " << Types[Obj.typeRetour] << "]" << endl;
	Stream << "=> Paramètres : " << endl;

	for (size_t i = 0; i < Obj.params.size(); i++) {
		Stream << "  --> " << Obj.params[i]->getNomVariable() << " - " << Types[Obj.params[i]->getType()] << endl;
	}

	Stream << "=> Corps : " << endl << Obj.corps;

	return Stream;
}

Type & Fonction::getTypeRetour ()
{
	return typeRetour;
}

void Fonction::setTypeRetour(const Type& type) {
	typeRetour = type;
}

void Fonction::setCorps(const Bloc& bloc) {
	corps = bloc;
}

Bloc& Fonction::getCorps() {
	return corps;
}

vector<Declaration*>& Fonction::getParams()
{
	return params;
}

void Fonction::addParam(Declaration* param) {
	params.push_back(param);
}

string Fonction::print () const
{
	string str = "\nFonction_";
	str += Types[typeRetour];
	str += "_" + nom + "(";
	for (auto param : params)
	{
		str += Types[param->getType ()] + " ";
		str += param->getNomVariable();
		str += ", ";
	}
	str += ")\n{\n";
	str += corps.print ();
	str += "\n}\n";

	return str;
}
