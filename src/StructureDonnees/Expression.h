#pragma once

#include "Instruction.h"
#include "Types.h"

using namespace std;

// Expression générique
class Expression : public Instruction {
public:
    virtual Type typer() const = 0;
	Expression();
	virtual long eval() { throw invalid_argument("Non-const expression"); }

	virtual bool isLvalue () { return false; }
	virtual bool isAppelFonction () { return false; }
	virtual bool isAffectation () { return false; }
	virtual void setTypeRetourFonction(Type type);
	virtual string buildIR(CFG * cfg) { return ""; }
};