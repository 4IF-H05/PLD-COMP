#include "AppelFonction.h"
#include "../Backend/IR.h"
#include "../Backend/registers.h"
#include "../StructureDonnees/DeclarationTableau.h"
#include "ExpCast.h"

void AppelFonction::setFonction (Fonction * fonction)
{
	this->fonction = fonction;
}

vector<Expression*>& AppelFonction::getsParams ()
{
	return parametres;
}

void AppelFonction::addParam (Expression * param)
{
	parametres.push_back (param);
}

string AppelFonction::print () const
{
	string str = "AppelFunction_";
	str += nomFonction;
	str += '(';
	for (auto param : parametres)
	{
		str += param->print () + ' ';
	}
	str += ")";
	return str;
}

Type AppelFonction::typer () const
{
	return (nomFonction == "getchar") ? CHAR : ((nomFonction == "putchar") ? VOID : fonction->getTypeRetour ()); // OUI C'EST MOCHE, PAS PLUS QUE TA TÊTE
}

vector<Expression*> AppelFonction::getSubExpressions ()
{
	vector<Expression*> res;
	res.push_back (this);
	for (auto param : parametres)
	{
		auto resTmp = param->getSubExpressions ();
		res.insert (res.end (), resTmp.begin (), resTmp.end ());
	}
	return res;
}

ostream & operator<<(ostream &stream, const AppelFonction &fct)
{
	stream << fct.nomFonction << '(';

	for (auto expr : fct.parametres)
	{
		stream << *expr << ' ';
	}

	stream << ')' << endl;

	return stream;
}

string AppelFonction::buildIR(CFG * cfg) {
	vector<string> params;
	string dest = "";

	Type typeRetour = VOID; // putchar
	
	if(nomFonction == "getchar")
		typeRetour = CHAR; // getchar
	else if(nomFonction != "putchar")
		typeRetour = fonction->getTypeRetour(); // random
		
	if(typeRetour != VOID) 
		dest = cfg->create_new_tempvar(typeRetour);

	params.push_back(dest);
	params.push_back(nomFonction);

	int paramCount = 0;

	vector<string> tmpParameters;
	
	// on récupere les valeurs de tous les paramètres dans des variables temporaires.
	for(auto it : parametres) {

		string arg;
		if (it->typer () != INT64_T) {
			ExpCast cast (it, INT64_T);
			arg = cast.buildIR (cfg);
		}
		else {
			arg = it->buildIR (cfg);
		}

		if (it->isLvalue ()) // cas particulier des declarations de tableau
		{
			Lvalue* lvalue = dynamic_cast<Lvalue*>(it);
			if (lvalue->getDeclaration ()->isDeclarationTableau () && !lvalue->isVariableTableau())
			{
				DeclarationTableau* decTab = static_cast<DeclarationTableau*>(lvalue->getDeclaration ());
				arg = decTab->getArrayAddress ();
			}
		}

		string tmpParamVar = cfg->create_new_tempvar (it->typer ());
		cfg->current_bb->add_IRInstr (
			new Copy (
				cfg->current_bb,
				INT64_T,
				{ tmpParamVar, arg }
			)
		);
		tmpParameters.push_back (tmpParamVar);

		paramCount++;
	}

	// on copie tous les parametres dans les registres juste avant l'appel
	paramCount = 0;
	for (auto paramTemp : tmpParameters)
	{
		if (paramCount < PARAM_REG_COUNT)
		{
			// STORING PARAMS IN REGISTERS
			vector<string> params = {
				paramStorage[2][paramCount],
				paramTemp
			};

			cfg->current_bb->add_IRInstr (
				new Copy (
					cfg->current_bb,
					INT64_T,
					params
				)
			);
		}
		else
		{
			// STORING PARAMS IN THE STACK
			vector<string> params = {
				paramTemp
			};

			cfg->current_bb->add_IRInstr (
				new Push (
					cfg->current_bb,
					INT64_T,
					params
				)
			);
		}


		paramCount++;
	}

	cfg->current_bb->add_IRInstr(
		new Call(
			cfg->current_bb,
			typeRetour,
			params
		)
	);

	return dest;
}