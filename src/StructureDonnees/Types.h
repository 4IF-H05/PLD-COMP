#pragma once

using namespace std;

#include <string>

enum Type { CHAR, INT32_T, INT64_T, VOID, ERROR_TYPE };

const string Types[] = { "char", "int32_t", "int64_t", "void", "error-type" };