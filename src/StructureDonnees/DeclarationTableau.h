#pragma once

using namespace std;

#include <iostream>
#include "Types.h"
#include "Instruction.h"
#include "Expression.h"
#include "Lvalue.h"
#include "VariableTableau.h"

class Lvalue;

// Déclaration d'un tableau
class DeclarationTableau : public Declaration {
	friend ostream& operator <<(ostream&, const DeclarationTableau&);

public:
	DeclarationTableau(Type & type);
	DeclarationTableau(Type & type, VariableTableau * variable, vector<Expression *> initialisation);
	DeclarationTableau(Type & type, VariableTableau * variable);
	~DeclarationTableau();

	Expression* getValeur();
	void setValeur(Expression * valeur);

	virtual vector<Expression*> getSubExpressions () override;

	virtual string print () const;
	virtual string buildIR(CFG* cfg);
	Expression *getLongueurTableau() const;
	virtual bool isDeclarationTableau () override { return true; }
	const vector<Expression *> getInitialisations() const;
	string getArrayAddress () { return arrayAddress; }
	void setArrayAddress (string address) { arrayAddress = address; }

private:
	Expression * longueurTableau;
	vector<Expression*> initialisations;

	string arrayAddress;


};