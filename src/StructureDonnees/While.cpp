#include "While.h"
#include "../Backend/IR.h"
#include "../Backend/registers.h"

While::While(Expression * clause, Instruction * corps) : Controle(corps, clause)
{
}

string While::print() const
{
	string str = "while(";
	str += clause->print();
	str += ")\n";
	str += instruction->print();
	str += "\n";
	return str;
}

string While::buildIR(CFG * cfg) {
	BasicBlock * beforeBB = cfg->current_bb;
	BasicBlock * testBB = new BasicBlock (cfg, cfg->new_BB_name ());
	BasicBlock * thenBB = new BasicBlock (cfg, cfg->new_BB_name ());
	BasicBlock * afterBB = new BasicBlock (cfg, cfg->new_BB_name ());

	beforeBB->exit_false = nullptr;
	beforeBB->exit_true = testBB;
	
	string cond = testBB->buildIR(cfg, clause);
	
	vector<string> vec = { string("%") + workReg1[clause->typer()], cond, "$0" };
	cfg->current_bb->add_IRInstr(new Cmp_eq(testBB, clause->typer(), vec));

	cfg->current_bb->exit_true = thenBB;
	cfg->current_bb->exit_false = afterBB;

	string out = thenBB->buildIR(cfg, getInstruction());

	cfg->current_bb->exit_false = nullptr;
	cfg->current_bb->exit_true = testBB;

	cfg->add_bb(afterBB);

	return out;
}