#pragma once

using namespace std;

#include <string>
#include <iostream>
#include "antlr4-runtime.h"
#include "GrammaireLexer.h"
#include "GrammaireParser.h"

int testFile(string file, bool verbose) {
    try{
        if(verbose)
            cout << "Testing " << file << "... " << endl;
        ifstream t(file);
        stringstream buffer;
        buffer << t.rdbuf();
        string inputText = buffer.str();

        ANTLRInputStream input(inputText);
        GrammaireLexer lexer(&input);
        CommonTokenStream tokens(&lexer);

        GrammaireParser parser(&tokens);
        parser.setErrorHandler(make_shared<BailErrorStrategy>());
        tree::ParseTree *tree = parser.progc();


        if(lexer.getNumberOfSyntaxErrors() > 0) {
            cerr << "Lexer syntax error" << endl;
            return 1;
        }

		MainVisitor visitor;
		Programme p = visitor.visit (tree);

		RangeChecker checker;
		list<string> errors = checker.programRangeChecking (p);
		if (!errors.empty ())
		{
			for (auto errorStr : errors)
				cerr << errorStr << endl;
			return 5;
		}
		list<string> warnings = checker.programStaticAnalysis (p);
		for (auto warningStr : warnings)
			cerr << warningStr << endl;
    }
    catch (RecognitionException &re) {
        cerr << "Syntax error : expected " << re.getExpectedTokens().toString() << endl;
        return 2;
    }
    catch (ParseCancellationException &pe) {
        cerr << "Unrecognized character" << endl;
        return 3;
    }
    catch (exception &e) {
        cerr << "Unknown syntax error : " << e.what() << endl;
        return 4;
    }
    
    return 0;
}