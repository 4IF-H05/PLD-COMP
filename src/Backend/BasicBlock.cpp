using namespace std;

#include "IR.h"

void BasicBlock::add_IRInstr(IRInstr * instr) {
    instrs.push_back(instr);
}

void BasicBlock::gen_asm(ostream& o) {
    o << '.' << label << ':' << endl;
    for(auto it : instrs)
        it->gen_asm(o);

    if(exit_false)
        o << "\tje ." << exit_false->getLabel() << endl;
    if(exit_true)
        o << "\tjmp ." << exit_true->getLabel() << endl;
}

string BasicBlock::buildIR(CFG * cfg, Instruction * instr) {
    cfg->add_bb(this);
	return instr->buildIR(cfg);
}

void BasicBlock::print() {
    cout << "== BasicBlock " << label << " ==" << endl;
    for(auto it : instrs) {
        cout << "   ";
        it->print();
    }

    cout << "   ==> exit_true : ";
    if(exit_true != nullptr)
        cout << exit_true->getLabel();
    else 
        cout << "none";

    cout << endl << "   ==> exit_false : ";
    if(exit_false != nullptr)
        cout << exit_false->getLabel();
    else 
        cout << "none";
    cout << endl;
}