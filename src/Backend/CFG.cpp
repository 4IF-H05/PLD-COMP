using namespace std;

#include <string>
#include "IR.h"
#include "asmTypes.h"
#include "registers.h"
#include "../StructureDonnees/DeclarationTableau.h"

int roundTo(int n, int r) {
	return (n % r == 0) ? n : n + r - (n % r);
}

void CFG::generate_bbs() {
    int paramCount = 0;

    for (auto it : ast->getParams()) {
		
		Type type = INT64_T;
		if (!it->isDeclarationTableau ())
		{
			type = it->getType ();
		}

		add_to_symbol_table (it->getNomVariable (), type);
		vector<string> vec;
		if (paramCount < PARAM_REG_COUNT)
		{
			vec = { it->getNomVariable (), paramStorage[type][paramCount] };
		}
		else
		{
			int baseOffset = 16;
			int descente = 8 * (ast->getParams ().size () - paramCount-1);
			int offset = baseOffset + descente ;
			string test = to_string (offset) + "(%rbp)";
			vec = { it->getNomVariable (), test };
		}

		current_bb->add_IRInstr (new Copy (current_bb, type, vec));
		if (it->isDeclarationTableau ())
		{
			DeclarationTableau* decTab = static_cast<DeclarationTableau*>(it);
			decTab->setArrayAddress (it->getNomVariable ());
		}
		
		paramCount++;
    }

    for (auto it : ast->getCorps().getInstructions()) {
        it->buildIR(this);
    }
}

void CFG::gen_asm(ostream& o) {
    gen_asm_prologue(o);
    for(auto it : bbs) {
        if(verbose)
            o << endl;
        it->gen_asm(o);
    }
    gen_asm_epilogue(o);
}

int CFG::getNeededMemory() {
    return roundTo(nextFreeSymbolIndex - 1, 16);
}

void CFG::gen_asm_prologue(ostream& o) {
	o << "\t.globl " << ast->getNom () << endl;

#ifdef __unix__
	o << "\t.type " << ast->getNom () << ", @function" << endl;
#elif defined(_WIN32) || defined(WIN32)
	o << "\t.def\t" << ast->getNom () << ";\t.scl\t2;\t.type\t32;\t.endef" << endl;
#endif // WIN32
    if(verbose)
        o   << endl;

	o	<< ast->getNom() << ":" << endl
        << "\tpushq %rbp" << endl
        << "\tmovq %rsp, %rbp" << endl
        << "\tsubq $" << getNeededMemory() << ", %rsp" << endl;
    // Après ça on a notre mémoire réservée qui se trouve entre rsp et rbp
}

void CFG::gen_asm_epilogue(ostream& o) {
    o   << "." << ast->getNom() << "_e:" << endl
        << "\tleave" << endl 
        << "\tret" << endl;
}

void CFG::add_bb(BasicBlock* bb) {
    current_bb = bb;
    bbs.push_back(bb);
}

string CFG::new_BB_name() {
    int i = current_bb->getLabel().length() - 1;
    for (; current_bb->getLabel()[i] != '_'; i--);
    
    return current_bb->getLabel().substr(0, ++i) + to_string(++nextBBnumber);
}

void CFG::add_to_symbol_table(string name, Type t, int taille) {
    SymbolType.insert(make_pair(name, t));
    // Arrondissement de l'index pour satisfaire l'alignement mémoire
    nextFreeSymbolIndex = roundTo(nextFreeSymbolIndex, asmSizes[t]);
    nextFreeSymbolIndex += asmSizes[t]*taille;
    SymbolIndex.insert(make_pair(name, nextFreeSymbolIndex));
    SymbolSize.insert(make_pair(name, taille));
}

string CFG::create_new_tempvar(Type t) {
    string name = "!tmp";
    name += to_string(++currentTempIndex);
    add_to_symbol_table(name, t);

    return name;
}

int CFG::get_var_index (string name)
{
	return SymbolIndex[name];
}

Type CFG::get_var_type (string name)
{
	return SymbolType.find(name)->second;
}

void CFG::print() {
    cout << "===== CFG =====" << endl << endl;
    for(auto it : bbs) {
        it->print();
    }

    cout << endl << "===== Table des symboles =====" << endl << endl;
    int i = 0;
    for(auto it = SymbolType.cbegin(); it != SymbolType.cend(); ++it)
    {
        cout << it->first << " " << Types[it->second] << " " << SymbolIndex[it->first] << " (taille " << SymbolSize[it->first] <<")" << endl;
    }
}

void CFG::deleteVar(string name) {
    SymbolIndex.erase(name);
    SymbolType.erase(name);
}

