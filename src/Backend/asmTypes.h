#pragma once

#include <string>

const string asmTypes[] = { ".byte", ".long", ".quad" };
const int asmSizes[] = { 1, 4, 8 };
const char asmSuffix[] = { 'b', 'l', 'q' };