#pragma once

#include <string>

#ifdef __unix__
constexpr int PARAM_REG_COUNT = 6;
const string paramStorage[][PARAM_REG_COUNT] = {
	{ "%dil", "%sil", "%dl", "%cl", "%r8b", "%r9b" },
	{ "%edi", "%esi", "%edx", "%ecx", "%r8d", "%r9d" },
	{ "%rdi", "%rsi", "%rdx", "%rcx", "%r8", "%r9" }
};
#elif defined(_WIN32) || defined(WIN32)
constexpr int PARAM_REG_COUNT = 4;
const string paramStorage[][PARAM_REG_COUNT] = {
	{ "%cl", "%dl", "%r8b", "%r9b" },
	{ "%ecx", "%edx", "%r8d", "%r9d"},
	{ "%rcx", "%rdx", "%r8", "%r9"}
};
#endif // WIN32


const string workReg1[] = { "al", "eax", "rax" };
const string workReg2[] = { "dl", "edx", "rdx" };
const string workReg3[] = { "r10b", "r10d", "r10" };