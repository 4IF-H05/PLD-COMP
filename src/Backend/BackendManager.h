#pragma once

using namespace std;

#include <iostream>
#include <string>
#include "../StructureDonnees/Programme.h"
#include "../StructureDonnees/Types.h"
#include "asmTypes.h"

class BackendManager {

public:
    BackendManager(ostream& output, Programme& p, bool v, bool o) : output(output), p(p), verbose(v), optim(o) {}
    void gen_asm(string inputFile);

private:
    void declarations();
    void fonctions();

    bool verbose;
    bool optim;
    ostream& output;
    Programme& p;
};