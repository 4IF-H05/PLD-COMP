#pragma once

#include <vector>
#include <string>
#include <iostream>
#include <initializer_list>
#include <map>

#include "../StructureDonnees/Types.h"
#include "../StructureDonnees/Fonction.h"

class BasicBlock;
class CFG;

//! The class for one 3-address instruction
class IRInstr {

public:
	/**  constructor */
	IRInstr(BasicBlock* bb_, Type t, vector<string> params) : bb(bb_), t(t), params(params) {}
	vector<string> getParams() { return params; }
	void setParam(int index, string value) { params[index] = value; }
	/** Actual code generation */
	virtual void gen_asm(ostream &o) = 0; /**< x86 assembly code generation for this IR instruction */
	virtual void print() = 0;
	string getVarName(string var);

protected:
	BasicBlock* bb; /**< The BB this instruction belongs to, which provides a pointer to the CFG this instruction belong to */
	Type t;
	vector<string> params; /**< For 3-op instrs: d, x, y; for ldconst: d, c;  For call: label, d, params;  for wmem and rmem: choose yourself */
	// if you subclass IRInstr, each IRInstr subclass has its parameters and the previous (very important) comment becomes useless: it would be a better design. 
	void printParams();
};

class Ret		: public IRInstr { using IRInstr::IRInstr; public: void gen_asm(ostream &o); void print(); };
class Ldconst	: public IRInstr { using IRInstr::IRInstr; public: void gen_asm(ostream &o); void print(); };
class Cast		: public IRInstr { using IRInstr::IRInstr; public: void gen_asm(ostream &o); void print(); };
class Add		: public IRInstr { using IRInstr::IRInstr; public: void gen_asm(ostream &o); void print(); };
class Sub		: public IRInstr { using IRInstr::IRInstr; public: void gen_asm(ostream &o); void print(); };
class Mul		: public IRInstr { using IRInstr::IRInstr; public: void gen_asm(ostream &o); void print(); };
class Div		: public IRInstr { using IRInstr::IRInstr; public: void gen_asm(ostream &o); void print(); };
class Mod		: public IRInstr { using IRInstr::IRInstr; public: void gen_asm(ostream &o); void print(); };
class Wmem		: public IRInstr { using IRInstr::IRInstr; public: void gen_asm(ostream &o); void print(); };
class Call		: public IRInstr { using IRInstr::IRInstr; public: void gen_asm(ostream &o); void print(); };
class Cmp_eq	: public IRInstr { using IRInstr::IRInstr; public: void gen_asm(ostream &o); void print(); };
class Cmp_neq	: public IRInstr { using IRInstr::IRInstr; public: void gen_asm(ostream &o); void print(); };
class Cmp_lt	: public IRInstr { using IRInstr::IRInstr; public: void gen_asm(ostream &o); void print(); };
class Cmp_le	: public IRInstr { using IRInstr::IRInstr; public: void gen_asm(ostream &o); void print(); };
class Cmp_ge	: public IRInstr { using IRInstr::IRInstr; public: void gen_asm(ostream &o); void print(); };
class Cmp_gt	: public IRInstr { using IRInstr::IRInstr; public: void gen_asm(ostream &o); void print(); };
class Orbit		: public IRInstr { using IRInstr::IRInstr; public: void gen_asm(ostream &o); void print(); };
class Andbit	: public IRInstr { using IRInstr::IRInstr; public: void gen_asm(ostream &o); void print(); };
class Xorbit	: public IRInstr { using IRInstr::IRInstr; public: void gen_asm(ostream &o); void print(); };
class Negbit	: public IRInstr { using IRInstr::IRInstr; public: void gen_asm(ostream &o); void print(); };
class Copy		: public IRInstr { using IRInstr::IRInstr; public: void gen_asm(ostream &o); void print(); };
class Rinc		: public IRInstr { using IRInstr::IRInstr; public: void gen_asm(ostream &o); void print(); };
class Rdec		: public IRInstr { using IRInstr::IRInstr; public: void gen_asm(ostream &o); void print(); };
class Linc		: public IRInstr { using IRInstr::IRInstr; public: void gen_asm(ostream &o); void print(); };
class Ldec		: public IRInstr { using IRInstr::IRInstr; public: void gen_asm(ostream &o); void print(); };
class Leaq		: public IRInstr { using IRInstr::IRInstr; public: void gen_asm(ostream &o); void print(); };
class Rmem		: public IRInstr { using IRInstr::IRInstr; public: void gen_asm(ostream &o); void print(); };
class Push		: public IRInstr { using IRInstr::IRInstr; public: void gen_asm(ostream &o); void print(); };

/**  The class for a basic block */

/* A few important comments.
	 IRInstr has no jump instructions:
	 assembly jumps are generated as follows in BasicBlock::gen_asm():
     1/ a cmp_* comparison instructions, if it is the last instruction of its block, 
       generates an actual assembly comparison followed by a conditional jump to the exit_false branch
			 If it is not the last instruction of its block, it behaves as an arithmetic two-operand instruction (add or mult)
		 2/ BasicBlock::gen_asm() first calls IRInstr::gen_asm() on all its instructions, and then 
		    if  exit_true  is a  nullptr, it generates the epilogue
				if  exit_false is not a nullptr, and the last instruction is not a cmp, it generates two conditional branches based on the value of the last variable assigned 
        otherwise it generates an unconditional jmp to the exit_true branch 
*/

class BasicBlock {

public:
	BasicBlock(CFG* cfg, string entry_label) : label(entry_label), cfg(cfg), exit_true(nullptr), exit_false(nullptr) {}
	string buildIR(CFG * cfg, Instruction * instr);
	void gen_asm(ostream &o); /**< x86 assembly code generation for this basic block (very simple) */
	void add_IRInstr(IRInstr * instr);
	CFG* getCfg () { return cfg; }
	vector<IRInstr*> getIRinstrs() { return instrs; }
	string getLabel() { return label; }

	// No encapsulation whatsoever here. Feel free to do better.
	BasicBlock* exit_true = nullptr;  /**< pointer to the next basic block, true branch. If nullptr, return from procedure */
	BasicBlock* exit_false = nullptr; /**< pointer to the next basic block, false branch. If null_ptr, the basic block ends with an unconditional jump */
	void print();
protected:
	string label; /**< label of the BB, also will be the label in the generated code */
	CFG* cfg; /** < the CFG where this block belongs */
	vector<IRInstr*> instrs; /** < the instructions themselves. */
};

/** The class for the control flow graph, also includes the symbol table */

/* A few important comments:
	 The entry block is the one with the same label as the AST function name.
	   (it could be the first of bbs, or it could be defined by an attribute value)
	 The exit block is the one with both exit pointers equal to nullptr.
     (again it could be identified in a more explicit way)

 */
class CFG {

public:
	CFG(Fonction* ast, bool v) : ast(ast), verbose(v) { 
		nextBBnumber = 0;
		current_bb = new BasicBlock(this, ast->getNom() + '_' + to_string(nextBBnumber));
		bbs.push_back(current_bb);
		currentTempIndex = -1;
		nextFreeSymbolIndex = 8; // 0 = base pointer, on écrit pas là
	}

	Fonction* ast; /**< The AST this CFG comes from */
	
	void generate_bbs();
	void add_bb(BasicBlock* bb);

	// x86 code generation: could be encapsulated in a processor class in a retargetable compiler
	void gen_asm(ostream& o);
	string IR_reg_to_asm(string reg); /**< helper method: inputs a IR reg or input variable, returns e.g. "-24(%rbp)" for the proper value of 24 */
	void gen_asm_prologue(ostream& o);
	void gen_asm_epilogue(ostream& o);

	// symbol table methods
	void add_to_symbol_table(string name, Type t, int taille = 1);
	string create_new_tempvar(Type t);
	Type getVarType(string name) { return SymbolType.find(name)->second; }
	void deleteVar(string name);
	int get_var_index(string name);
	Type get_var_type(string name);
	vector <BasicBlock*> getBBs() { return bbs; }

	// basic block management
	string new_BB_name();
	BasicBlock* current_bb;
	void print();
	
	void realignSymbolTable(int index);
	
	bool verbose;

private:
	int getNeededMemory();

	map <string, Type> SymbolType; /**< part of the symbol table  */
	map <string, int> SymbolIndex; /**< part of the symbol table  */
	map <string, int> SymbolSize; /**< part of the symbol table  */
	int nextFreeSymbolIndex; /**< to allocate new symbols in the symbol table */
	int currentTempIndex;
	int nextBBnumber; /**< just for naming */

	vector <BasicBlock*> bbs; /**< all the basic blocks of this CFG*/
};