#include "Optimizer.h"
#include <algorithm>
#include <vector> 
#include "../StructureDonnees/Types.h"

template<typename T> bool inVector(vector<T> v, T e) {
    return find(v.begin(), v.end(), e) != v.end();
}

int Optimizer::parseBB(BasicBlock * bb, int c, vector<BasicBlock *> parsed, vector<BasicBlock *> parsedTwice) {
    // Beaucoup de merde juste pour s'assurer qu'on ne fait pas une boucle infinie :
    // On autorise un bb a être parsé au maximum 2 fois
    // Ca aurait été plus malin et évolutif de le faire avec une map plutôt que 2 vector
    // Mais tant pis ça marche comme ça c'était assez simple à faire
    if(find(parsedTwice.begin(), parsedTwice.end(), bb) == parsedTwice.end()) {
        for(auto i : bb->getIRinstrs()) {
            for(auto p : i->getParams()) {
                if(isVar(p)) {
                    auto var = varLife.find(p);
                    if(var == varLife.end())
                        varLife.insert(make_pair(p, make_pair(c, c)));
                    else
                        var->second.second = c;
                }
            }
            c++;
        }
        
        if(!inVector<BasicBlock *>(parsed, bb))
            parsed.push_back(bb);
        else 
            parsedTwice.push_back(bb);
        
        if(bb->exit_true && !inVector<BasicBlock *>(parsedTwice, bb->exit_true))
            c = parseBB(bb->exit_true, c, parsed, parsedTwice);
        if(bb->exit_false && !inVector<BasicBlock *>(parsedTwice, bb->exit_false))
            c = parseBB(bb->exit_false, c, parsed, parsedTwice);
    }

    return c;
}

void Optimizer::fillVarLife() {
    int c = 0;
    vector<BasicBlock*> parsed = {};
    vector<BasicBlock*> parsedTwice = {};
    BasicBlock * bb = cfg->getBBs()[0];
    parseBB(bb, c, parsed, parsedTwice);
}

void Optimizer::fillVarLifeStart() {
    // Transformation de nom, <naissance, mort> en naissance, <nom, mort>
    // pour pouvoir faire un parcourt dans l'ordre dans la suite
    for(auto it = varLife.cbegin(); it != varLife.cend(); ++it)
        varLifeStart.insert(make_pair(
            it->second.first, 
            make_pair(it->first, it->second.second)
        ));
}

void Optimizer::determineEquivalences() {
    for(auto it = varLife.cbegin(); it != varLife.cend(); ++it)
        varEq.insert(make_pair(it->first, "")); // Initialisation

    for(auto toReplace = varLifeStart.cbegin(); toReplace != varLifeStart.cend(); toReplace++) {
        for(auto candidate = varLifeStart.cbegin(); candidate != varLifeStart.cend() && candidate->first <= toReplace->first; candidate++) {
            if(candidate->second.second < toReplace->first && cfg->getVarType(candidate->second.first) == cfg->getVarType(toReplace->second.first)) { 
                // Le candidat est mort avant la naissance de toReplace, et ils sont compatibles (type)
                varEq.find(toReplace->second.first)->second = candidate->second.first; // pair(toReplace -> candidat)
                // On met à jour la mort du candidat du coup, je sais pas pourquoi c'est impossible de le faire plus simplement
                int birth = candidate->first;
                string name = candidate->second.first;
                int death = toReplace->second.second;
                varLifeStart.erase(candidate);
                varLifeStart.insert(make_pair(birth, make_pair(name, death)));
                break;
            }
        }
    }
}

void Optimizer::replaceVar(string source, string replace) {
    // Cherche les éventuelles tmp dépendant de source
    for(auto it = varEq.cbegin(); it != varEq.cend(); ++it)
        if(it->second == source)
            replaceVar(it->first, source);

    // Remplace source
    for(auto bb : cfg->getBBs()) {
        for(auto i : bb->getIRinstrs()) {
            int c = 0;
            for(auto p : i->getParams()) {
                if(isVar(p) && (p == source || i->getVarName(p) == source)) {
                    i->setParam(c, replace);
                }
                c++;
            }
        }
    }
    
    cfg->deleteVar(source);
}

void Optimizer::replaceVars() {
    for(auto it = varEq.cbegin(); it != varEq.cend(); ++it)
        if(it->second != "") {
            replaceVar(it->first, it->second);
        }
}

void Optimizer::replaceRegs() {
    for(auto it = varEq.cbegin(); it != varEq.cend(); ++it) {
        if(!freeRegisters.empty() && !freeRegisters[0].empty() && it->second == "") {
            Type type = cfg->getVarType(it->first);
            string reg = string("%") + freeRegisters[type][0];
            replaceVar(it->first, reg);
            freeRegisters[0].erase(freeRegisters[0].begin());
            freeRegisters[1].erase(freeRegisters[1].begin());
            freeRegisters[2].erase(freeRegisters[2].begin());
        }
    }
}

CFG * Optimizer::optimize() {
    /*
        fillVarLife, fillVarLifeStart, determineEquivalences :
            Remplit mes 3 structures de données. Globalement je suis assez 
            confiant que ça marche bien, même si l'estimation de la mort 
            des variables est pas très propre, je pense qu'elle ne peut 
            être que surestimée dans le pire des cas, et jamais sous-estimée,
            donc au pire l'optimisation sera moins bonne mais on introduira
            pas de bugs.
    */
    fillVarLife();
    fillVarLifeStart();
    determineEquivalences();
    /*
        replaceVars :
            Remplace les nouvelles variables tmp par les anciennes qui 
            ne sont plus utilisées, ça marche mais tout seul ça sert à rien
            EDIT : Après corrections d'autres bugs cette fonction crée des bugs
            dans certains cas (toujoursm moins souvent que replaceRegs)
    */
    replaceVars();
    /*
        replaceRegs :
            Remplace les variables tmp restantes par des registres dans
            la mesure des stocks disponibles. Ca marche sur des programmes 
            très simples, mais dès que le programme devient plus complexe
            soit ça fait des résultats incohérents soit un core dump. Je 
            soupçonne un bug dans la manipulation des adresses mais je suis
            sûr de rien, j'ai l'impression que ça foire quand on manipule 
            des tableaux, mais ça c'est pas nouveau que c'est foireux. Si
            quelqu'un veut investiguer ça qu'il ne se gêne pas.
    */
    replaceRegs();

    /*
        Ré-aligner la table des symboles pour enlever les trous ? 
        Très TRES dangereux (à cause des adresses stockées en dur)
    */

    return cfg;
}

void Optimizer::print() {
    cout << endl << "===== Durées de vie =====" << endl << endl;
    for(auto it = varLife.cbegin(); it != varLife.cend(); ++it)
        cout << it->first << " : " << it->second.first << " -> " << it->second.second << endl;

    cout << endl << "===== Durées de vie (naissance) =====";
    cout << endl << "Après détermination  des équivalences" << endl << endl;
    for(auto it = varLifeStart.cbegin(); it != varLifeStart.cend(); ++it)
        cout << it->first << " <- " << it->second.first << " -> " << it->second.second << endl;

    cout << endl << "===== Table des équivalences =====" << endl << endl;
    for(auto it = varEq.cbegin(); it != varEq.cend(); ++it)
        cout << it->first << " <-> " << it->second << endl;
}

bool Optimizer::isVar(string v) {
    return v[0] == '!';
}