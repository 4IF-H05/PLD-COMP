using namespace std;

#include <iostream>
#include <string>
#include "BackendManager.h"
#include "IR.h"
#include "Optimizer.h"
#include "../StructureDonnees/DeclarationTableau.h"

void BackendManager::gen_asm(string inputFile) {
    output << "\t.file	\"" << inputFile << "\"" << endl;
    declarations();
    fonctions();
}

void BackendManager::declarations() {
    if (p.getDeclarations().size() > 0) {
        if(verbose)
            output << endl << "# ==== declarations ====" << endl << endl;
        output << "\t.data" << endl;
        for (auto it : p.getDeclarations()) {
            string nom = it->getNomVariable();
            Type type = it->getType();
            DeclarationTableau *dt = dynamic_cast<DeclarationTableau *>(it);
            if (dt) {
                int taille = dt->getLongueurTableau()->eval();
                int tailleTotal = asmSizes[type] * taille;
                int alignement;
                // alignement mémoire, j'ai pas trouvé plus jolie
                if (tailleTotal >= 1 && tailleTotal < 4) {
                    alignement = 1;
                } else if (tailleTotal >= 4 && tailleTotal < 8) {
                    alignement = 4;
                } else if (tailleTotal >= 8 && tailleTotal < 16) {
                    alignement = 8;
                } else if (tailleTotal >= 16 && tailleTotal < 32) {
                    alignement = 16;
                } else if (tailleTotal >= 32) {
                    alignement = 32;
                }
                if (dt->getInitialisations().size() == 0) {
                    output << "\t.comm\t " << nom << "," << tailleTotal << "," << alignement << endl;
                } else {
                    output << "\t.globl\t" << nom << endl;
                    output << "\t.align\t" << alignement << endl;
#ifdef __unix__ // lignes inutiles sous windows
                    output << "\t.type\t" << nom << ", @object" << endl
                           << "\t.size\t" << nom << ", " << taille << endl;
#endif // __unix__
                    output << nom << ":" << endl;
                    for (auto it : dt->getInitialisations()) {
                        output << "\t" << asmTypes[type] << "\t" << it->eval() << endl;
                    }
                }
            } else {
                if (it->getValeur()) {
                    output << "\t.globl\t" << nom << endl;
                    if(it->getType()!=CHAR){
                        output << "\t.align\t" << asmSizes[type] << endl;
                    }
#ifdef __unix__ // lignes inutiles sous windows
                    output << "\t.type\t" << nom << ", @object" << endl
                           << "\t.size\t" << nom << ", " << asmSizes[type] << endl;
#endif // __unix__
                    output << nom << ":" << endl;
                    output << "\t" << asmTypes[type] << "\t" << it->getValeur()->eval() << endl;
                } else {
                    output << "\t.comm\t " << nom << "," << asmSizes[type] << "," << asmSizes[type] <<endl;
                }

            }
        }
    }
}

void BackendManager::fonctions() {
    if (p.getFonctions().size() > 0) {
        if(verbose) {
            cout << endl;
            output << endl << "# ==== functions ====" << endl  << endl;
        }
        output << "\t.text" << endl;
        for (auto it : p.getFonctions()) {
            CFG *cfg = new CFG(it, verbose);
            cfg->generate_bbs();
            if(verbose) {
                cout << endl << "    *-----------------------*";
                cout << endl << "    |      Optimisation     |";
                cout << endl << "    *-----------------------*";
            }
            Optimizer optimizer(cfg);
            if(optim)
                cfg = optimizer.optimize();
            if(verbose) {
                cout << endl;
                if(optim)
                    optimizer.print();
                cout << endl << "    *-----------------------*";
                cout << endl << "    |    Génération ASM     |";
                cout << endl << "    *-----------------------*";
                cout << endl << endl;
                cfg->print();
            }
            cfg->gen_asm(output);
            if(verbose)
                output << endl;
        }
    }
}
