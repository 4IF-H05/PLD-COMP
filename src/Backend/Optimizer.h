#pragma once

using namespace std;

#include "IR.h"
#include <map>
#include <utility> // pair

class Optimizer {
public:
    Optimizer(CFG * cfg) : cfg(cfg) {}
    CFG * optimize();
    void print();
private:
    void fillVarLife();
    void fillVarLifeStart();
    void determineEquivalences();
    void replaceVar(string source, string replace);
    void replaceVars();
    void replaceRegs();
    bool isVar(string v);
    int parseBB(BasicBlock * bb, int c, vector<BasicBlock *> parsed, vector<BasicBlock *> parsedTwice);

    CFG * cfg;
    map<string, pair<int, int>> varLife;
    // Je pense pas qu'une multimap soit vraiment nécessaire mais j'ai pas de preuves non plus, donc dans le doute ...
    multimap<int, pair<string, int>> varLifeStart;
    map<string, string> varEq;
    vector<vector<string>> freeRegisters = {
        // On va pas utiliser rax et rdx parce qu'il sont utilisés temporairement partout, ça compliquerait tout
        { /*"al",*/ "cl", /*"dl",*/ "r8b", "r9b", /*"r10b",*/ "r11b" },
        { /*"eax",*/ "ecx", /*"edx",*/ "r8d", "r9d", /*"r10d",*/ "r11d" },
        { /*"rax",*/ "rcx", /*"rdx",*/ "r8", "r9", /*"r10",*/ "r11" }
    };
};