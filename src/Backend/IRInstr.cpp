using namespace std;

#define MAP

#include <iostream>
#include "IR.h"
#include "asmTypes.h"
#include "registers.h"

string IRInstr::getVarName(string var) {
	if(var[0] == '%' || var[0] == '-' || var[0] == '$' || var[0] == '(')
		return var; // C'est un registre ou une constante ou un truc qu'on veut pas toucher

	int offset = this->bb->getCfg()->get_var_index(var);

	if(offset == 0)
		// Ca doit être une variable globale
		return string(var);// + "(%rip)"; // WTF is that (%rip) gcc, dunno if we need it

	return string("-") + to_string(offset) + "(%rbp)"; // C'est une variable locale
}

void IRInstr::printParams() {
	cout << "   | "; 
	for(auto it : params) 
		cout << it << "   " ;
}

void Ldconst::gen_asm(ostream &o) {
	if(bb->getCfg()->verbose)
		o << "# ldconst : " << params[1] << " -> " << params[0] << endl;
	
    o << "\tmov" << asmSuffix[t]
	  << "\t$" << params[1]
	  << ", " << getVarName(params[0]) << endl;
}

void Ldconst::print() {
    cout << "Ldconst"; 
	printParams(); 
	cout << endl;
}

void Cast::gen_asm(ostream &o) {
	Type from = (Type)stoi(params[0]);
	if(bb->getCfg()->verbose)
		o << "# cast : " << params[1] << " (" << Types[from] << " -> " << Types[t] << ")" << endl;
	
	if(from < t)
		o << "\tmovs" << asmSuffix[from] << asmSuffix[t]
		  << "\t" << getVarName(params[1])
		  << ", %" << workReg1[t] << endl;

	else if(from > t) {
		o << "\tmov" << asmSuffix[from]
		  << "\t" << getVarName(params[1])
		  << ", %" << workReg1[from] << endl;
	}

	o << "\tmov" << asmSuffix[t]
	  << "\t%" << workReg1[t]
	  << ", " << getVarName(params[2]) << endl;
}

void Cast::print() {
    cout << "Cast"; 
	printParams(); 
	cout << endl;
}

void Add::gen_asm(ostream &o) {
	if(bb->getCfg()->verbose)
		o << "# add : " << params[0] << " = " << params[1] << " + " << params[2]<< endl;
	
    o << "\tmov" << asmSuffix[t] << "\t" << getVarName(params[1]) << ", %" << workReg1[t] << endl
	  << "\tmov" << asmSuffix[t] << "\t" << getVarName(params[2]) << ", %" << workReg2[t] << endl
	  << "\tadd" << asmSuffix[t] << "\t%" << workReg2[t] << ", %" << workReg1[t] << endl
	  << "\tmov" << asmSuffix[t] << "\t%" << workReg1[t] << ", " << getVarName(params[0]) << endl;
}

void Add::print() {
    cout << "Add"; 
	printParams(); 
	cout << endl;
}

void Sub::gen_asm(ostream &o) {
	if(bb->getCfg()->verbose)
		o << "# sub : " << params[0] << " = " << params[1] << " - " << params[2]<< endl;
	
    o << "\tmov" << asmSuffix[t] << "\t" << getVarName(params[1]) << ", %" << workReg1[t] << endl
	  << "\tmov" << asmSuffix[t] << "\t" << getVarName(params[2]) << ", %" << workReg2[t] << endl
	  << "\tsub" << asmSuffix[t] << "\t%" << workReg2[t] << ", %" << workReg1[t] << endl
	  << "\tmov" << asmSuffix[t] << "\t%" << workReg1[t] << ", " << getVarName(params[0]) << endl;
}

void Sub::print() {
    cout << "Sub"; 
	printParams(); 
	cout << endl;
}

void Div::gen_asm(ostream &o) {
    if(bb->getCfg()->verbose)
		o << "# div : " << params[0] << " = " << params[1] << " / " << params[2]<< endl;
	
    o << "\txor \t%rdx, %rdx" << endl // Faut vider les registres avant pour être 
	  << "\txor \t%rax, %rax" << endl // sûrs de pas avoir de la merde à gauche
	  << "\tmov" << asmSuffix[t] << "\t" << getVarName(params[1]) << ", %" << workReg1[t] << endl
	  << "\tcqo" << endl
	  << "\tmov" << asmSuffix[t] << "\t" << getVarName(params[2]) << ", %" << workReg3[t] << endl
	  << "\tidiv \t%" << workReg3[t] << endl
	  << "\tmov" << asmSuffix[t] << "\t%" << workReg1[t] << ", " << getVarName(params[0]) << endl;
}

void Div::print() {
    cout << "Div"; 
	printParams(); 
	cout << endl;
}

void Mod::gen_asm(ostream &o) {
    if(bb->getCfg()->verbose)
		o << "# mod : " << params[0] << " = " << params[1] << " % " << params[2]<< endl;
	
    o << "\txor \t%rdx, %rdx" << endl // Faut vider les registres avant pour être 
	  << "\txor \t%rax, %rax" << endl // sûrs de pas avoir de la merde à gauche
	  << "\tmov" << asmSuffix[t] << "\t" << getVarName(params[1]) << ", %" << workReg1[t] << endl
	  << "\tcqo" << endl
	  << "\tmov" << asmSuffix[t] << "\t" << getVarName(params[2]) << ", %" << workReg3[t] << endl
	  << "\tidiv \t%" << workReg3[t] << endl
	  << "\tmov" << asmSuffix[t] << "\t%" << workReg2[t] << ", " << getVarName(params[0]) << endl;
}

void Mod::print() {
    cout << "Mod"; 
	printParams(); 
	cout << endl;
}

void Mul::gen_asm(ostream & o)
{
	if(bb->getCfg()->verbose)
		o << "# mult : " << params[0] << " = " << params[1] << " * " << params[2] << endl;
	
	o << "\tmov" << asmSuffix[t] << "\t" << getVarName(params[1]) << ", %" << workReg1[t] << endl
	  << "\tmov" << asmSuffix[t] << "\t" << getVarName(params[2]) << ", %" << workReg2[t] << endl
	  << "\timul\t%" << workReg2[t] << endl
	  << "\tmov" << asmSuffix[t] << "\t%" << workReg1[t] << ", " << getVarName(params[0]) << endl;
}

void Mul::print() {
	cout << "Mul"; 
	printParams(); 
	cout << endl;
}

void Wmem::gen_asm(ostream &o) {
if(bb->getCfg()->verbose)
    o << "# wmem : " << params[1] << " -> (" << params[0] << ")" << endl;

    o << "\tmovq\t" << getVarName(params[0]) << ", %" << workReg1[INT64_T] << endl
      << "\tmov" << asmSuffix[t] << "\t" << getVarName(params[1]) << ", %" << workReg2[t] << endl
      << "\tmov" << asmSuffix[t] << "\t%" << workReg2[t] << ", (%" << workReg1[INT64_T] << ")" << endl;
}

void Wmem::print() {
    cout << "Wmem"; 
	printParams(); 
	cout << endl;
}

void Call::gen_asm(ostream &o) {
	if(bb->getCfg()->verbose)
		o << "# call : " << params[1] << endl;
	
    o << "\tcall\t" << params[1] << endl;
	if(params[0] != "") // non VOID
		o << "\tmov" << asmSuffix[t]
		  << "\t%" << workReg1[t] << ", "
		  << getVarName(params[0]) << endl;
}

void Call::print() {
    cout << "Call"; 
	printParams(); 
	cout << endl;
}

void Cmp_eq::gen_asm(ostream &o) {
	if(bb->getCfg()->verbose)
		o << "# comp_eq : " << params[0] << " = " << params[1] << " == " << params[2] << endl;
	

	string destStr = getVarName(params[0]);
	string op1Str = getVarName(params[1]);
	string op2Str = getVarName(params[2]);

	o << "\tmov" << asmSuffix[t] << "\t" << op2Str << ", %" << workReg1[t] << endl
	  << "\tcmp" << asmSuffix[t] << "\t" << op1Str << ", %" << workReg1[t] << endl
	  << "\tsete\t%al" << endl;
	if (t > CHAR)
		o << "\tmovzb" << asmSuffix[t] << "\t%al, %" << workReg1[t] << endl;
	if(destStr != string("%") + workReg1[t])
		o << "\tmov" << asmSuffix[t] << "\t%" << workReg1[t] << ", " << destStr << endl;
}

void Cmp_eq::print() {
    cout << "Cmp_eq"; 
	printParams(); 
	cout << endl;
}

void Cmp_neq::gen_asm(ostream &o) {
	if(bb->getCfg()->verbose)
		o << "# comp_neq : " << params[0] << " = " << params[1] << " != " << params[2] << endl;
	

	string destStr = getVarName (params[0]);
	string op1Str = getVarName (params[1]);
	string op2Str = getVarName (params[2]);

	o	<< "\tmov" << asmSuffix[t] << "\t" << op2Str << ", %" << workReg1[t] << endl
		<< "\tcmp" << asmSuffix[t] << "\t" << op1Str << ", %" << workReg1[t] << endl
		<< "\tsetne\t%al" << endl;
	if (t > CHAR)
		o << "\tmovzb" << asmSuffix[t] << "\t%al, %" << workReg1[t] << endl;
	if(destStr != string("%") + workReg1[t])
		o << "\tmov" << asmSuffix[t] << "\t%" << workReg1[t] << ", " << destStr << endl;
}

void Cmp_neq::print() {
    cout << "Cmp_neq"; 
	printParams(); 
	cout << endl;
}

void Cmp_lt::gen_asm(ostream &o) {
	if(bb->getCfg()->verbose)
		o << "# comp_lt : " << params[0] << " = " << params[1] << " < " << params[2] << endl;
	

	string destStr = getVarName (params.at (0));
	string op1Str = getVarName (params.at (1));
	string op2Str = getVarName (params.at (2));

	o << "\tmov" << asmSuffix[t] << "\t" << op2Str << ", %" << workReg1[t] << endl
	  << "\tcmp" << asmSuffix[t] << "\t" << op1Str << ", %" << workReg1[t] << endl
	  << "\tsetg\t%al" << endl;
	if (t > CHAR)
		o << "\tmovzb" << asmSuffix[t] << "\t%al, %" << workReg1[t] << endl;
	if(destStr != string("%") + workReg1[t])
		o << "\tmov" << asmSuffix[t] << "\t%" << workReg1[t] << ", " << destStr << endl;
}

void Cmp_lt::print() {
    cout << "Cmp_lt"; 
	printParams(); 
	cout << endl;
}

void Cmp_le::gen_asm (ostream &o) {
	if(bb->getCfg()->verbose)
		o << "# comp_le : " << params[0] << " = " << params[1] << " <= " << params[2] << endl;
	

	string destStr = getVarName (params.at (0));
	string op1Str = getVarName (params.at (1));
	string op2Str = getVarName (params.at (2));

	o << "\tmov" << asmSuffix[t] << "\t" << op2Str << ", %" << workReg1[t] << endl
	  << "\tcmp" << asmSuffix[t] << "\t" << op1Str << ", %" << workReg1[t] << endl
	  << "\tsetge\t%al" << endl;
	if (t > CHAR)
		o << "\tmovzb" << asmSuffix[t] << "\t%al, %" << workReg1[t] << endl;
	if(destStr != string("%") + workReg1[t])
		o << "\tmov" << asmSuffix[t] << "\t%" << workReg1[t] << ", " << destStr << endl;
}

void Cmp_le::print () {
	cout << "Cmp_le"; 
	printParams(); 
	cout << endl;
}


void Copy::gen_asm(ostream &o) {
	if(bb->getCfg()->verbose)
		o << "# copy : " << params[1] << " -> " << params[0] << endl;
	
	string source = getVarName(params[1]);

	if(source[0] != '%' && getVarName(params[0])[0] != '%') {
		// On ne peut pas faire un mov de la pile vers la pile
		// -> On utilise un registre temporairement
		o << "\tmov" << asmSuffix[t] << "\t" << source << ", %" << workReg1[t] << endl;
		source = string("%") + workReg1[t];
	}

	o << "\tmov" << asmSuffix[t]
	  << "\t" << source << ", "
	  << getVarName(params[0]) << endl;
}

void Copy::print() {
	cout << "Copy"; 
	printParams(); 
	cout << endl;
}

void Ret::gen_asm(ostream &o) {
	if(bb->getCfg()->verbose)
		o << "# return from " << params[0] << endl;
	

	o << "\tjmp ." << params[0] << "_e" << endl;
}

void Ret::print() {
	cout << "Return"; 
	printParams(); 
	cout << endl;
}

void Cmp_ge::gen_asm (ostream & o)
{
if(bb->getCfg()->verbose)
	o << "# comp_ge : " << params[0] << " = " << params[1] << " >= " << params[2] << endl;


	string destStr = getVarName (params.at (0));
	string op1Str = getVarName (params.at (1));
	string op2Str = getVarName (params.at (2));

	o << "\tmov" << asmSuffix[t] << "\t" << op2Str << ", %" << workReg1[t] << endl
	  << "\tcmp" << asmSuffix[t] << "\t" << op1Str << ", %" << workReg1[t] << endl
	  << "\tsetle\t%al" << endl;
	if (t > CHAR)
		o << "\tmovzb" << asmSuffix[t] << "\t%al, %" << workReg1[t] << endl;
	if (destStr != string ("%") + workReg1[t])
		o << "\tmov" << asmSuffix[t] << "\t%" << workReg1[t] << ", " << destStr << endl;
}

void Cmp_ge::print ()
{
	cout << "Cmp_ge"; 
	printParams(); 
	cout << endl;
}

void Cmp_gt::gen_asm(ostream & o)
{
if(bb->getCfg()->verbose)
	o << "# comp_gt : " << params[0] << " = " << params[1] << " > " << params[2] << endl;


	string destStr = getVarName (params.at (0));
	string op1Str = getVarName (params.at (1));
	string op2Str = getVarName (params.at (2));

	o << "\tmov" << asmSuffix[t] << "\t" << op2Str << ", %" << workReg1[t] << endl
	  << "\tcmp" << asmSuffix[t] << "\t" << op1Str << ", %" << workReg1[t] << endl
	  << "\tsetl\t%al" << endl;
	if (t > CHAR)
		o << "\tmovzb" << asmSuffix[t] << "\t%al, %" << workReg1[t] << endl;
	if (destStr != string ("%") + workReg1[t])
		o << "\tmov" << asmSuffix[t] << "\t%" << workReg1[t] << ", " << destStr << endl;
}
void Cmp_gt::print()
{
	cout << "Cmp_gt"; 
	printParams(); 
	cout << endl;
}

void Orbit::gen_asm(ostream & o)
{
if(bb->getCfg()->verbose)
	o << "# ORb : " << params[0] << " = " << params[1] << " | " << params[2] << endl;


	string destStr = getVarName (params.at (0));
	string op1Str = getVarName (params.at (1));
	string op2Str = getVarName (params.at (2));

	o	<< "\tmov" << asmSuffix[t] << "\t" << op2Str << ", %" << workReg1[t] << endl
		<< "\tor" << asmSuffix[t] << "\t" << op1Str << ", %" << workReg1[t] << endl;
	if (destStr != string ("%") + workReg1[t])
		o << "\tmov" << asmSuffix[t] << "\t%" << workReg1[t] << ", " << destStr << endl;
}
void Orbit::print()
{
	cout << "Orbit"; 
	printParams(); 
	cout << endl;
}

void Andbit::gen_asm(ostream & o)
{
if(bb->getCfg()->verbose)
	o << "# ANDb : " << params[0] << " = " << params[1] << " & " << params[2] << endl;


	string destStr = getVarName (params.at (0));
	string op1Str = getVarName (params.at (1));
	string op2Str = getVarName (params.at (2));

	o << "\tmov" << asmSuffix[t] << "\t" << op2Str << ", %" << workReg1[t] << endl
	  << "\tand" << asmSuffix[t] << "\t" << op1Str << ", %" << workReg1[t] << endl;
	if (destStr != string ("%") + workReg1[t])
		o << "\tmov" << asmSuffix[t] << "\t%" << workReg1[t] << ", " << destStr << endl;
}

void Andbit::print()
{
	cout << "Andbit"; 
	printParams(); 
	cout << endl;
}

void Xorbit::gen_asm(ostream & o)
{
	if(bb->getCfg()->verbose)
		o << "# XORb : " << params[0] << " = " << params[1] << " ^ " << params[2] << endl;
	

	string destStr = getVarName (params.at (0));
	string op1Str = getVarName (params.at (1));
	string op2Str = getVarName (params.at (2));

	o << "\tmov" << asmSuffix[t] << "\t" << op2Str << ", %" << workReg1[t] << endl
	  << "\txor" << asmSuffix[t] << "\t" << op1Str << ", %" << workReg1[t] << endl;
	if (destStr != string ("%") + workReg1[t])
		o << "\tmov" << asmSuffix[t] << "\t%" << workReg1[t] << ", " << destStr << endl;
}

void Xorbit::print()
{
	cout << "XorBit"; 
	printParams(); 
	cout << endl;
}

void Negbit::gen_asm(ostream & o)
{
	if(bb->getCfg()->verbose)
		o << "# NegBit : " << params[0] << " = ~" << params[1] << endl;
	
	o << "\tmov" << asmSuffix[t] << "\t" << getVarName(params[1]) << ", %" << workReg1[t] << endl
	  << "\tnot" << asmSuffix[t] << "\t%" << workReg1[t] << endl;
	if (getVarName(params[0]) != string ("%") + workReg1[t])
		o << "\tmov" << asmSuffix[t] << "\t%" << workReg1[t] << ", " << getVarName(params[0]) << endl;
}

void Negbit::print()
{
	cout << "NegBit"; 
	printParams(); 
	cout << endl;
}

void Rinc::gen_asm(ostream & o)
{
	if(bb->getCfg()->verbose)
		o << "# Rinc : " << params[0] << " = " << params[1] << "++" << endl;
	
	o << "\tmov" << asmSuffix[t] << "\t" << getVarName(params[1]) << ", %" << workReg1[t] << endl
	  << "\tinc" << asmSuffix[t] << "\t" << getVarName(params[1]) << endl;
	if (getVarName(params[0]) != string ("%") + workReg1[t])
		o << "\tmov" << asmSuffix[t] << "\t%" << workReg1[t] << ", " << getVarName(params[0]) << endl;
}

void Rinc::print()
{
	cout << "Rinc"; 
	printParams(); 
	cout << endl;
}

void Rdec::gen_asm(ostream & o)
{
	if(bb->getCfg()->verbose)
		o << "# Rdec : " << params[0] << " = " << params[1] << "--" << endl;
	
	o << "\tmov" << asmSuffix[t] << "\t" << getVarName(params[1]) << ", %" << workReg1[t] << endl
	  << "\tdec" << asmSuffix[t] << "\t" << getVarName(params[1]) << endl;
	if (getVarName(params[0]) != string ("%") + workReg1[t])
		o << "\tmov" << asmSuffix[t] << "\t%" << workReg1[t] << ", " << getVarName(params[0]) << endl;
}

void Rdec::print()
{
	cout << "Rdec"; 
	printParams(); 
	cout << endl;
}

void Linc::gen_asm(ostream & o)
{
	if(bb->getCfg()->verbose)
		o << "# Linc : ++" << params[0] << endl;
	
	o << "\tinc" << asmSuffix[t] << "\t" << getVarName(params[0]) << endl;
}

void Linc::print()
{
	cout << "Linc"; 
	printParams(); 
	cout << endl;
}

void Ldec::gen_asm(ostream & o)
{
	if(bb->getCfg()->verbose)
		o << "# Ldec : --" << params[0] << endl;
	
	o << "\tdec" << asmSuffix[t] << "\t" << getVarName(params[0]) << endl;
}

void Ldec::print()
{
	cout << "Ldec"; 
	printParams(); 
	cout << endl;
}

void Leaq::gen_asm(ostream & o)
{
	if(bb->getCfg()->verbose)
		o << "# leaq : " << params[0] << " = address of " << params[1] << endl;
    
    if(getVarName(params[1])[0] != '%' && getVarName(params[0])[0] != '%') {
        // On ne peut pas faire un mov de la pile vers la pile
        // -> On utilise un registre temporairement
        o << "\tleaq\t" << getVarName(params[1]) << ", %" << workReg1[INT64_T] << endl
          << "\tmovq\t%" << workReg1[INT64_T] << ", " << getVarName(params[0]) << endl;
    } else
	    o << "\tleaq\t" << getVarName(params[1]) << ", " << getVarName(params[0]) << endl;
}

void Leaq::print()
{
	cout << "Leaq"; 
	printParams(); 
	cout << endl;
}

void Rmem::gen_asm(ostream & o)
{
if(bb->getCfg()->verbose)
    o << "# Rmem : " << params[0] << " = content of adress " << params[1] << endl;

    if(getVarName(params[1])[0] != '%') {
        // On ne peut pas faire un mov de la pile vers la pile
        // -> On utilise un registre temporairement
        o << "\tmovq\t" << getVarName(params[1]) << ", %" << workReg1[INT64_T] << endl
          << "\tmov" << asmSuffix[t] << "\t" << "(%" << workReg1[INT64_T] << "), %" << workReg2[t]<< "" << endl
          << "\tmov" << asmSuffix[t] << "\t" << "%" << workReg2[t] << ", " << getVarName(params[0])<< "" << endl;
    } else
        o << "\tmov" << asmSuffix[t] << "\t" << "(" << getVarName(params[1]) << "), " << getVarName(params[0]) << "" << endl;
}

void Rmem::print()
{
    cout << "Rmem"; 
	printParams(); 
	cout << endl;
}

void Push::gen_asm (ostream & o)
{
if(bb->getCfg()->verbose)
	o << "# Push : " << params[0] << " is pushed on the stack " << endl;

	o << "\tpushq" << "\t" << getVarName(params[0]) << endl;
}

void Push::print ()
{
	cout << "Rmem";
	printParams ();
	cout << endl;
}