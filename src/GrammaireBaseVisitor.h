
// Generated from Grammaire.g4 by ANTLR 4.7.1

#pragma once


#include "antlr4-runtime.h"
#include "GrammaireVisitor.h"


/**
 * This class provides an empty implementation of GrammaireVisitor, which can be
 * extended to create a visitor which only needs to handle a subset of the available methods.
 */
class  GrammaireBaseVisitor : public GrammaireVisitor {
public:

  virtual antlrcpp::Any visitProgc(GrammaireParser::ProgcContext *ctx) override {
    return visitChildren(ctx);
  }

  virtual antlrcpp::Any visitProgDecl(GrammaireParser::ProgDeclContext *ctx) override {
    return visitChildren(ctx);
  }

  virtual antlrcpp::Any visitProgFunc(GrammaireParser::ProgFuncContext *ctx) override {
    return visitChildren(ctx);
  }

  virtual antlrcpp::Any visitProgEOF(GrammaireParser::ProgEOFContext *ctx) override {
    return visitChildren(ctx);
  }

  virtual antlrcpp::Any visitParamV(GrammaireParser::ParamVContext *ctx) override {
    return visitChildren(ctx);
  }

  virtual antlrcpp::Any visitParamTab(GrammaireParser::ParamTabContext *ctx) override {
    return visitChildren(ctx);
  }

  virtual antlrcpp::Any visitParamSimple(GrammaireParser::ParamSimpleContext *ctx) override {
    return visitChildren(ctx);
  }

  virtual antlrcpp::Any visitParamVide(GrammaireParser::ParamVideContext *ctx) override {
    return visitChildren(ctx);
  }

  virtual antlrcpp::Any visitFonction(GrammaireParser::FonctionContext *ctx) override {
    return visitChildren(ctx);
  }

  virtual antlrcpp::Any visitTypeRetType(GrammaireParser::TypeRetTypeContext *ctx) override {
    return visitChildren(ctx);
  }

  virtual antlrcpp::Any visitTypeRetVoid(GrammaireParser::TypeRetVoidContext *ctx) override {
    return visitChildren(ctx);
  }

  virtual antlrcpp::Any visitParamAppelV(GrammaireParser::ParamAppelVContext *ctx) override {
    return visitChildren(ctx);
  }

  virtual antlrcpp::Any visitParamAppel(GrammaireParser::ParamAppelContext *ctx) override {
    return visitChildren(ctx);
  }

  virtual antlrcpp::Any visitAppelFonction(GrammaireParser::AppelFonctionContext *ctx) override {
    return visitChildren(ctx);
  }

  virtual antlrcpp::Any visitLvalSimple(GrammaireParser::LvalSimpleContext *ctx) override {
    return visitChildren(ctx);
  }

  virtual antlrcpp::Any visitLvalTab(GrammaireParser::LvalTabContext *ctx) override {
    return visitChildren(ctx);
  }

  virtual antlrcpp::Any visitElementTabV(GrammaireParser::ElementTabVContext *ctx) override {
    return visitChildren(ctx);
  }

  virtual antlrcpp::Any visitElementsTab(GrammaireParser::ElementsTabContext *ctx) override {
    return visitChildren(ctx);
  }

  virtual antlrcpp::Any visitDeclDef(GrammaireParser::DeclDefContext *ctx) override {
    return visitChildren(ctx);
  }

  virtual antlrcpp::Any visitDeclLval(GrammaireParser::DeclLvalContext *ctx) override {
    return visitChildren(ctx);
  }

  virtual antlrcpp::Any visitDeclTabDef(GrammaireParser::DeclTabDefContext *ctx) override {
    return visitChildren(ctx);
  }

  virtual antlrcpp::Any visitDef(GrammaireParser::DefContext *ctx) override {
    return visitChildren(ctx);
  }

  virtual antlrcpp::Any visitInstrReturn(GrammaireParser::InstrReturnContext *ctx) override {
    return visitChildren(ctx);
  }

  virtual antlrcpp::Any visitInstrExp(GrammaireParser::InstrExpContext *ctx) override {
    return visitChildren(ctx);
  }

  virtual antlrcpp::Any visitInstrDecl(GrammaireParser::InstrDeclContext *ctx) override {
    return visitChildren(ctx);
  }

  virtual antlrcpp::Any visitInstrControle(GrammaireParser::InstrControleContext *ctx) override {
    return visitChildren(ctx);
  }

  virtual antlrcpp::Any visitExprOrBit(GrammaireParser::ExprOrBitContext *ctx) override {
    return visitChildren(ctx);
  }

  virtual antlrcpp::Any visitExprLt(GrammaireParser::ExprLtContext *ctx) override {
    return visitChildren(ctx);
  }

  virtual antlrcpp::Any visitExprLdec(GrammaireParser::ExprLdecContext *ctx) override {
    return visitChildren(ctx);
  }

  virtual antlrcpp::Any visitExprAnd(GrammaireParser::ExprAndContext *ctx) override {
    return visitChildren(ctx);
  }

  virtual antlrcpp::Any visitExprVar(GrammaireParser::ExprVarContext *ctx) override {
    return visitChildren(ctx);
  }

  virtual antlrcpp::Any visitExprNegBin(GrammaireParser::ExprNegBinContext *ctx) override {
    return visitChildren(ctx);
  }

  virtual antlrcpp::Any visitExprFunc(GrammaireParser::ExprFuncContext *ctx) override {
    return visitChildren(ctx);
  }

  virtual antlrcpp::Any visitExprMultDiv(GrammaireParser::ExprMultDivContext *ctx) override {
    return visitChildren(ctx);
  }

  virtual antlrcpp::Any visitExprDecR(GrammaireParser::ExprDecRContext *ctx) override {
    return visitChildren(ctx);
  }

  virtual antlrcpp::Any visitExprLeq(GrammaireParser::ExprLeqContext *ctx) override {
    return visitChildren(ctx);
  }

  virtual antlrcpp::Any visitExprNeg(GrammaireParser::ExprNegContext *ctx) override {
    return visitChildren(ctx);
  }

  virtual antlrcpp::Any visitExprLinc(GrammaireParser::ExprLincContext *ctx) override {
    return visitChildren(ctx);
  }

  virtual antlrcpp::Any visitExprEq(GrammaireParser::ExprEqContext *ctx) override {
    return visitChildren(ctx);
  }

  virtual antlrcpp::Any visitExprNeq(GrammaireParser::ExprNeqContext *ctx) override {
    return visitChildren(ctx);
  }

  virtual antlrcpp::Any visitExprChar(GrammaireParser::ExprCharContext *ctx) override {
    return visitChildren(ctx);
  }

  virtual antlrcpp::Any visitExprConst(GrammaireParser::ExprConstContext *ctx) override {
    return visitChildren(ctx);
  }

  virtual antlrcpp::Any visitExprDecL(GrammaireParser::ExprDecLContext *ctx) override {
    return visitChildren(ctx);
  }

  virtual antlrcpp::Any visitExprXorBit(GrammaireParser::ExprXorBitContext *ctx) override {
    return visitChildren(ctx);
  }

  virtual antlrcpp::Any visitExprGt(GrammaireParser::ExprGtContext *ctx) override {
    return visitChildren(ctx);
  }

  virtual antlrcpp::Any visitExprInc(GrammaireParser::ExprIncContext *ctx) override {
    return visitChildren(ctx);
  }

  virtual antlrcpp::Any visitExprSizeE(GrammaireParser::ExprSizeEContext *ctx) override {
    return visitChildren(ctx);
  }

  virtual antlrcpp::Any visitExprPar(GrammaireParser::ExprParContext *ctx) override {
    return visitChildren(ctx);
  }

  virtual antlrcpp::Any visitExprGeq(GrammaireParser::ExprGeqContext *ctx) override {
    return visitChildren(ctx);
  }

  virtual antlrcpp::Any visitExprCast(GrammaireParser::ExprCastContext *ctx) override {
    return visitChildren(ctx);
  }

  virtual antlrcpp::Any visitExprOr(GrammaireParser::ExprOrContext *ctx) override {
    return visitChildren(ctx);
  }

  virtual antlrcpp::Any visitExprAndBit(GrammaireParser::ExprAndBitContext *ctx) override {
    return visitChildren(ctx);
  }

  virtual antlrcpp::Any visitExprDec(GrammaireParser::ExprDecContext *ctx) override {
    return visitChildren(ctx);
  }

  virtual antlrcpp::Any visitExprTer(GrammaireParser::ExprTerContext *ctx) override {
    return visitChildren(ctx);
  }

  virtual antlrcpp::Any visitExprDef(GrammaireParser::ExprDefContext *ctx) override {
    return visitChildren(ctx);
  }

  virtual antlrcpp::Any visitExprAddMoins(GrammaireParser::ExprAddMoinsContext *ctx) override {
    return visitChildren(ctx);
  }

  virtual antlrcpp::Any visitExprMod(GrammaireParser::ExprModContext *ctx) override {
    return visitChildren(ctx);
  }

  virtual antlrcpp::Any visitExprSizeT(GrammaireParser::ExprSizeTContext *ctx) override {
    return visitChildren(ctx);
  }

  virtual antlrcpp::Any visitExprNon(GrammaireParser::ExprNonContext *ctx) override {
    return visitChildren(ctx);
  }

  virtual antlrcpp::Any visitControleWhile(GrammaireParser::ControleWhileContext *ctx) override {
    return visitChildren(ctx);
  }

  virtual antlrcpp::Any visitControleIfElse(GrammaireParser::ControleIfElseContext *ctx) override {
    return visitChildren(ctx);
  }

  virtual antlrcpp::Any visitControleIf(GrammaireParser::ControleIfContext *ctx) override {
    return visitChildren(ctx);
  }

  virtual antlrcpp::Any visitBloc(GrammaireParser::BlocContext *ctx) override {
    return visitChildren(ctx);
  }

  virtual antlrcpp::Any visitContenuBloc(GrammaireParser::ContenuBlocContext *ctx) override {
    return visitChildren(ctx);
  }


};

