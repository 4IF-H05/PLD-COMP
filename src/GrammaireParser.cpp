
// Generated from Grammaire.g4 by ANTLR 4.7.1


#include "GrammaireVisitor.h"

#include "GrammaireParser.h"


using namespace antlrcpp;
using namespace antlr4;

GrammaireParser::GrammaireParser(TokenStream *input) : Parser(input) {
  _interpreter = new atn::ParserATNSimulator(this, _atn, _decisionToDFA, _sharedContextCache);
}

GrammaireParser::~GrammaireParser() {
  delete _interpreter;
}

std::string GrammaireParser::getGrammarFileName() const {
  return "Grammaire.g4";
}

const std::vector<std::string>& GrammaireParser::getRuleNames() const {
  return _ruleNames;
}

dfa::Vocabulary& GrammaireParser::getVocabulary() const {
  return _vocabulary;
}


//----------------- ProgcContext ------------------------------------------------------------------

GrammaireParser::ProgcContext::ProgcContext(ParserRuleContext *parent, size_t invokingState)
  : ParserRuleContext(parent, invokingState) {
}

GrammaireParser::ProgContext* GrammaireParser::ProgcContext::prog() {
  return getRuleContext<GrammaireParser::ProgContext>(0);
}


size_t GrammaireParser::ProgcContext::getRuleIndex() const {
  return GrammaireParser::RuleProgc;
}

antlrcpp::Any GrammaireParser::ProgcContext::accept(tree::ParseTreeVisitor *visitor) {
  if (auto parserVisitor = dynamic_cast<GrammaireVisitor*>(visitor))
    return parserVisitor->visitProgc(this);
  else
    return visitor->visitChildren(this);
}

GrammaireParser::ProgcContext* GrammaireParser::progc() {
  ProgcContext *_localctx = _tracker.createInstance<ProgcContext>(_ctx, getState());
  enterRule(_localctx, 0, GrammaireParser::RuleProgc);

  auto onExit = finally([=] {
    exitRule();
  });
  try {
    enterOuterAlt(_localctx, 1);
    setState(38);
    prog();
   
  }
  catch (RecognitionException &e) {
    _errHandler->reportError(this, e);
    _localctx->exception = std::current_exception();
    _errHandler->recover(this, _localctx->exception);
  }

  return _localctx;
}

//----------------- ProgContext ------------------------------------------------------------------

GrammaireParser::ProgContext::ProgContext(ParserRuleContext *parent, size_t invokingState)
  : ParserRuleContext(parent, invokingState) {
}


size_t GrammaireParser::ProgContext::getRuleIndex() const {
  return GrammaireParser::RuleProg;
}

void GrammaireParser::ProgContext::copyFrom(ProgContext *ctx) {
  ParserRuleContext::copyFrom(ctx);
}

//----------------- ProgEOFContext ------------------------------------------------------------------

tree::TerminalNode* GrammaireParser::ProgEOFContext::EOF() {
  return getToken(GrammaireParser::EOF, 0);
}

GrammaireParser::ProgEOFContext::ProgEOFContext(ProgContext *ctx) { copyFrom(ctx); }

antlrcpp::Any GrammaireParser::ProgEOFContext::accept(tree::ParseTreeVisitor *visitor) {
  if (auto parserVisitor = dynamic_cast<GrammaireVisitor*>(visitor))
    return parserVisitor->visitProgEOF(this);
  else
    return visitor->visitChildren(this);
}
//----------------- ProgDeclContext ------------------------------------------------------------------

GrammaireParser::DeclContext* GrammaireParser::ProgDeclContext::decl() {
  return getRuleContext<GrammaireParser::DeclContext>(0);
}

GrammaireParser::ProgContext* GrammaireParser::ProgDeclContext::prog() {
  return getRuleContext<GrammaireParser::ProgContext>(0);
}

GrammaireParser::ProgDeclContext::ProgDeclContext(ProgContext *ctx) { copyFrom(ctx); }

antlrcpp::Any GrammaireParser::ProgDeclContext::accept(tree::ParseTreeVisitor *visitor) {
  if (auto parserVisitor = dynamic_cast<GrammaireVisitor*>(visitor))
    return parserVisitor->visitProgDecl(this);
  else
    return visitor->visitChildren(this);
}
//----------------- ProgFuncContext ------------------------------------------------------------------

GrammaireParser::FonctionContext* GrammaireParser::ProgFuncContext::fonction() {
  return getRuleContext<GrammaireParser::FonctionContext>(0);
}

GrammaireParser::ProgContext* GrammaireParser::ProgFuncContext::prog() {
  return getRuleContext<GrammaireParser::ProgContext>(0);
}

GrammaireParser::ProgFuncContext::ProgFuncContext(ProgContext *ctx) { copyFrom(ctx); }

antlrcpp::Any GrammaireParser::ProgFuncContext::accept(tree::ParseTreeVisitor *visitor) {
  if (auto parserVisitor = dynamic_cast<GrammaireVisitor*>(visitor))
    return parserVisitor->visitProgFunc(this);
  else
    return visitor->visitChildren(this);
}
GrammaireParser::ProgContext* GrammaireParser::prog() {
  ProgContext *_localctx = _tracker.createInstance<ProgContext>(_ctx, getState());
  enterRule(_localctx, 2, GrammaireParser::RuleProg);

  auto onExit = finally([=] {
    exitRule();
  });
  try {
    setState(47);
    _errHandler->sync(this);
    switch (getInterpreter<atn::ParserATNSimulator>()->adaptivePredict(_input, 0, _ctx)) {
    case 1: {
      _localctx = dynamic_cast<ProgContext *>(_tracker.createInstance<GrammaireParser::ProgDeclContext>(_localctx));
      enterOuterAlt(_localctx, 1);
      setState(40);
      decl();
      setState(41);
      prog();
      break;
    }

    case 2: {
      _localctx = dynamic_cast<ProgContext *>(_tracker.createInstance<GrammaireParser::ProgFuncContext>(_localctx));
      enterOuterAlt(_localctx, 2);
      setState(43);
      fonction();
      setState(44);
      prog();
      break;
    }

    case 3: {
      _localctx = dynamic_cast<ProgContext *>(_tracker.createInstance<GrammaireParser::ProgEOFContext>(_localctx));
      enterOuterAlt(_localctx, 3);
      setState(46);
      match(GrammaireParser::EOF);
      break;
    }

    }
   
  }
  catch (RecognitionException &e) {
    _errHandler->reportError(this, e);
    _localctx->exception = std::current_exception();
    _errHandler->recover(this, _localctx->exception);
  }

  return _localctx;
}

//----------------- ParamVContext ------------------------------------------------------------------

GrammaireParser::ParamVContext::ParamVContext(ParserRuleContext *parent, size_t invokingState)
  : ParserRuleContext(parent, invokingState) {
}

GrammaireParser::ParamContext* GrammaireParser::ParamVContext::param() {
  return getRuleContext<GrammaireParser::ParamContext>(0);
}


size_t GrammaireParser::ParamVContext::getRuleIndex() const {
  return GrammaireParser::RuleParamV;
}

antlrcpp::Any GrammaireParser::ParamVContext::accept(tree::ParseTreeVisitor *visitor) {
  if (auto parserVisitor = dynamic_cast<GrammaireVisitor*>(visitor))
    return parserVisitor->visitParamV(this);
  else
    return visitor->visitChildren(this);
}

GrammaireParser::ParamVContext* GrammaireParser::paramV() {
  ParamVContext *_localctx = _tracker.createInstance<ParamVContext>(_ctx, getState());
  enterRule(_localctx, 4, GrammaireParser::RuleParamV);

  auto onExit = finally([=] {
    exitRule();
  });
  try {
    setState(52);
    _errHandler->sync(this);
    switch (_input->LA(1)) {
      case GrammaireParser::T__0: {
        enterOuterAlt(_localctx, 1);
        setState(49);
        match(GrammaireParser::T__0);
        setState(50);
        param();
        break;
      }

      case GrammaireParser::T__4: {
        enterOuterAlt(_localctx, 2);

        break;
      }

    default:
      throw NoViableAltException(this);
    }
   
  }
  catch (RecognitionException &e) {
    _errHandler->reportError(this, e);
    _localctx->exception = std::current_exception();
    _errHandler->recover(this, _localctx->exception);
  }

  return _localctx;
}

//----------------- ParamContext ------------------------------------------------------------------

GrammaireParser::ParamContext::ParamContext(ParserRuleContext *parent, size_t invokingState)
  : ParserRuleContext(parent, invokingState) {
}


size_t GrammaireParser::ParamContext::getRuleIndex() const {
  return GrammaireParser::RuleParam;
}

void GrammaireParser::ParamContext::copyFrom(ParamContext *ctx) {
  ParserRuleContext::copyFrom(ctx);
}

//----------------- ParamSimpleContext ------------------------------------------------------------------

tree::TerminalNode* GrammaireParser::ParamSimpleContext::TYPE() {
  return getToken(GrammaireParser::TYPE, 0);
}

tree::TerminalNode* GrammaireParser::ParamSimpleContext::VAR() {
  return getToken(GrammaireParser::VAR, 0);
}

GrammaireParser::ParamVContext* GrammaireParser::ParamSimpleContext::paramV() {
  return getRuleContext<GrammaireParser::ParamVContext>(0);
}

GrammaireParser::ParamSimpleContext::ParamSimpleContext(ParamContext *ctx) { copyFrom(ctx); }

antlrcpp::Any GrammaireParser::ParamSimpleContext::accept(tree::ParseTreeVisitor *visitor) {
  if (auto parserVisitor = dynamic_cast<GrammaireVisitor*>(visitor))
    return parserVisitor->visitParamSimple(this);
  else
    return visitor->visitChildren(this);
}
//----------------- ParamVideContext ------------------------------------------------------------------

GrammaireParser::ParamVideContext::ParamVideContext(ParamContext *ctx) { copyFrom(ctx); }

antlrcpp::Any GrammaireParser::ParamVideContext::accept(tree::ParseTreeVisitor *visitor) {
  if (auto parserVisitor = dynamic_cast<GrammaireVisitor*>(visitor))
    return parserVisitor->visitParamVide(this);
  else
    return visitor->visitChildren(this);
}
//----------------- ParamTabContext ------------------------------------------------------------------

tree::TerminalNode* GrammaireParser::ParamTabContext::TYPE() {
  return getToken(GrammaireParser::TYPE, 0);
}

tree::TerminalNode* GrammaireParser::ParamTabContext::VAR() {
  return getToken(GrammaireParser::VAR, 0);
}

GrammaireParser::ParamVContext* GrammaireParser::ParamTabContext::paramV() {
  return getRuleContext<GrammaireParser::ParamVContext>(0);
}

GrammaireParser::ParamTabContext::ParamTabContext(ParamContext *ctx) { copyFrom(ctx); }

antlrcpp::Any GrammaireParser::ParamTabContext::accept(tree::ParseTreeVisitor *visitor) {
  if (auto parserVisitor = dynamic_cast<GrammaireVisitor*>(visitor))
    return parserVisitor->visitParamTab(this);
  else
    return visitor->visitChildren(this);
}
GrammaireParser::ParamContext* GrammaireParser::param() {
  ParamContext *_localctx = _tracker.createInstance<ParamContext>(_ctx, getState());
  enterRule(_localctx, 6, GrammaireParser::RuleParam);

  auto onExit = finally([=] {
    exitRule();
  });
  try {
    setState(63);
    _errHandler->sync(this);
    switch (getInterpreter<atn::ParserATNSimulator>()->adaptivePredict(_input, 2, _ctx)) {
    case 1: {
      _localctx = dynamic_cast<ParamContext *>(_tracker.createInstance<GrammaireParser::ParamTabContext>(_localctx));
      enterOuterAlt(_localctx, 1);
      setState(54);
      match(GrammaireParser::TYPE);
      setState(55);
      match(GrammaireParser::VAR);
      setState(56);
      match(GrammaireParser::T__1);
      setState(57);
      match(GrammaireParser::T__2);
      setState(58);
      paramV();
      break;
    }

    case 2: {
      _localctx = dynamic_cast<ParamContext *>(_tracker.createInstance<GrammaireParser::ParamSimpleContext>(_localctx));
      enterOuterAlt(_localctx, 2);
      setState(59);
      match(GrammaireParser::TYPE);
      setState(60);
      match(GrammaireParser::VAR);
      setState(61);
      paramV();
      break;
    }

    case 3: {
      _localctx = dynamic_cast<ParamContext *>(_tracker.createInstance<GrammaireParser::ParamVideContext>(_localctx));
      enterOuterAlt(_localctx, 3);

      break;
    }

    }
   
  }
  catch (RecognitionException &e) {
    _errHandler->reportError(this, e);
    _localctx->exception = std::current_exception();
    _errHandler->recover(this, _localctx->exception);
  }

  return _localctx;
}

//----------------- FonctionContext ------------------------------------------------------------------

GrammaireParser::FonctionContext::FonctionContext(ParserRuleContext *parent, size_t invokingState)
  : ParserRuleContext(parent, invokingState) {
}

GrammaireParser::TypeRetContext* GrammaireParser::FonctionContext::typeRet() {
  return getRuleContext<GrammaireParser::TypeRetContext>(0);
}

tree::TerminalNode* GrammaireParser::FonctionContext::VAR() {
  return getToken(GrammaireParser::VAR, 0);
}

GrammaireParser::ParamContext* GrammaireParser::FonctionContext::param() {
  return getRuleContext<GrammaireParser::ParamContext>(0);
}

GrammaireParser::BlocContext* GrammaireParser::FonctionContext::bloc() {
  return getRuleContext<GrammaireParser::BlocContext>(0);
}


size_t GrammaireParser::FonctionContext::getRuleIndex() const {
  return GrammaireParser::RuleFonction;
}

antlrcpp::Any GrammaireParser::FonctionContext::accept(tree::ParseTreeVisitor *visitor) {
  if (auto parserVisitor = dynamic_cast<GrammaireVisitor*>(visitor))
    return parserVisitor->visitFonction(this);
  else
    return visitor->visitChildren(this);
}

GrammaireParser::FonctionContext* GrammaireParser::fonction() {
  FonctionContext *_localctx = _tracker.createInstance<FonctionContext>(_ctx, getState());
  enterRule(_localctx, 8, GrammaireParser::RuleFonction);

  auto onExit = finally([=] {
    exitRule();
  });
  try {
    enterOuterAlt(_localctx, 1);
    setState(65);
    typeRet();
    setState(66);
    match(GrammaireParser::VAR);
    setState(67);
    match(GrammaireParser::T__3);
    setState(68);
    param();
    setState(69);
    match(GrammaireParser::T__4);
    setState(70);
    bloc();
   
  }
  catch (RecognitionException &e) {
    _errHandler->reportError(this, e);
    _localctx->exception = std::current_exception();
    _errHandler->recover(this, _localctx->exception);
  }

  return _localctx;
}

//----------------- TypeRetContext ------------------------------------------------------------------

GrammaireParser::TypeRetContext::TypeRetContext(ParserRuleContext *parent, size_t invokingState)
  : ParserRuleContext(parent, invokingState) {
}


size_t GrammaireParser::TypeRetContext::getRuleIndex() const {
  return GrammaireParser::RuleTypeRet;
}

void GrammaireParser::TypeRetContext::copyFrom(TypeRetContext *ctx) {
  ParserRuleContext::copyFrom(ctx);
}

//----------------- TypeRetVoidContext ------------------------------------------------------------------

GrammaireParser::TypeRetVoidContext::TypeRetVoidContext(TypeRetContext *ctx) { copyFrom(ctx); }

antlrcpp::Any GrammaireParser::TypeRetVoidContext::accept(tree::ParseTreeVisitor *visitor) {
  if (auto parserVisitor = dynamic_cast<GrammaireVisitor*>(visitor))
    return parserVisitor->visitTypeRetVoid(this);
  else
    return visitor->visitChildren(this);
}
//----------------- TypeRetTypeContext ------------------------------------------------------------------

tree::TerminalNode* GrammaireParser::TypeRetTypeContext::TYPE() {
  return getToken(GrammaireParser::TYPE, 0);
}

GrammaireParser::TypeRetTypeContext::TypeRetTypeContext(TypeRetContext *ctx) { copyFrom(ctx); }

antlrcpp::Any GrammaireParser::TypeRetTypeContext::accept(tree::ParseTreeVisitor *visitor) {
  if (auto parserVisitor = dynamic_cast<GrammaireVisitor*>(visitor))
    return parserVisitor->visitTypeRetType(this);
  else
    return visitor->visitChildren(this);
}
GrammaireParser::TypeRetContext* GrammaireParser::typeRet() {
  TypeRetContext *_localctx = _tracker.createInstance<TypeRetContext>(_ctx, getState());
  enterRule(_localctx, 10, GrammaireParser::RuleTypeRet);

  auto onExit = finally([=] {
    exitRule();
  });
  try {
    setState(74);
    _errHandler->sync(this);
    switch (_input->LA(1)) {
      case GrammaireParser::TYPE: {
        _localctx = dynamic_cast<TypeRetContext *>(_tracker.createInstance<GrammaireParser::TypeRetTypeContext>(_localctx));
        enterOuterAlt(_localctx, 1);
        setState(72);
        match(GrammaireParser::TYPE);
        break;
      }

      case GrammaireParser::T__5: {
        _localctx = dynamic_cast<TypeRetContext *>(_tracker.createInstance<GrammaireParser::TypeRetVoidContext>(_localctx));
        enterOuterAlt(_localctx, 2);
        setState(73);
        match(GrammaireParser::T__5);
        break;
      }

    default:
      throw NoViableAltException(this);
    }
   
  }
  catch (RecognitionException &e) {
    _errHandler->reportError(this, e);
    _localctx->exception = std::current_exception();
    _errHandler->recover(this, _localctx->exception);
  }

  return _localctx;
}

//----------------- ParamAppelVContext ------------------------------------------------------------------

GrammaireParser::ParamAppelVContext::ParamAppelVContext(ParserRuleContext *parent, size_t invokingState)
  : ParserRuleContext(parent, invokingState) {
}

GrammaireParser::ParamAppelContext* GrammaireParser::ParamAppelVContext::paramAppel() {
  return getRuleContext<GrammaireParser::ParamAppelContext>(0);
}


size_t GrammaireParser::ParamAppelVContext::getRuleIndex() const {
  return GrammaireParser::RuleParamAppelV;
}

antlrcpp::Any GrammaireParser::ParamAppelVContext::accept(tree::ParseTreeVisitor *visitor) {
  if (auto parserVisitor = dynamic_cast<GrammaireVisitor*>(visitor))
    return parserVisitor->visitParamAppelV(this);
  else
    return visitor->visitChildren(this);
}

GrammaireParser::ParamAppelVContext* GrammaireParser::paramAppelV() {
  ParamAppelVContext *_localctx = _tracker.createInstance<ParamAppelVContext>(_ctx, getState());
  enterRule(_localctx, 12, GrammaireParser::RuleParamAppelV);

  auto onExit = finally([=] {
    exitRule();
  });
  try {
    setState(79);
    _errHandler->sync(this);
    switch (_input->LA(1)) {
      case GrammaireParser::T__0: {
        enterOuterAlt(_localctx, 1);
        setState(76);
        match(GrammaireParser::T__0);
        setState(77);
        paramAppel();
        break;
      }

      case GrammaireParser::T__4: {
        enterOuterAlt(_localctx, 2);

        break;
      }

    default:
      throw NoViableAltException(this);
    }
   
  }
  catch (RecognitionException &e) {
    _errHandler->reportError(this, e);
    _localctx->exception = std::current_exception();
    _errHandler->recover(this, _localctx->exception);
  }

  return _localctx;
}

//----------------- ParamAppelContext ------------------------------------------------------------------

GrammaireParser::ParamAppelContext::ParamAppelContext(ParserRuleContext *parent, size_t invokingState)
  : ParserRuleContext(parent, invokingState) {
}

GrammaireParser::ExprContext* GrammaireParser::ParamAppelContext::expr() {
  return getRuleContext<GrammaireParser::ExprContext>(0);
}

GrammaireParser::ParamAppelVContext* GrammaireParser::ParamAppelContext::paramAppelV() {
  return getRuleContext<GrammaireParser::ParamAppelVContext>(0);
}


size_t GrammaireParser::ParamAppelContext::getRuleIndex() const {
  return GrammaireParser::RuleParamAppel;
}

antlrcpp::Any GrammaireParser::ParamAppelContext::accept(tree::ParseTreeVisitor *visitor) {
  if (auto parserVisitor = dynamic_cast<GrammaireVisitor*>(visitor))
    return parserVisitor->visitParamAppel(this);
  else
    return visitor->visitChildren(this);
}

GrammaireParser::ParamAppelContext* GrammaireParser::paramAppel() {
  ParamAppelContext *_localctx = _tracker.createInstance<ParamAppelContext>(_ctx, getState());
  enterRule(_localctx, 14, GrammaireParser::RuleParamAppel);

  auto onExit = finally([=] {
    exitRule();
  });
  try {
    setState(85);
    _errHandler->sync(this);
    switch (_input->LA(1)) {
      case GrammaireParser::T__3:
      case GrammaireParser::T__11:
      case GrammaireParser::T__12:
      case GrammaireParser::T__13:
      case GrammaireParser::T__14:
      case GrammaireParser::T__15:
      case GrammaireParser::T__16:
      case GrammaireParser::VAR:
      case GrammaireParser::CHAR:
      case GrammaireParser::INT: {
        enterOuterAlt(_localctx, 1);
        setState(81);
        expr(0);
        setState(82);
        paramAppelV();
        break;
      }

      case GrammaireParser::T__4: {
        enterOuterAlt(_localctx, 2);

        break;
      }

    default:
      throw NoViableAltException(this);
    }
   
  }
  catch (RecognitionException &e) {
    _errHandler->reportError(this, e);
    _localctx->exception = std::current_exception();
    _errHandler->recover(this, _localctx->exception);
  }

  return _localctx;
}

//----------------- AppelFonctionContext ------------------------------------------------------------------

GrammaireParser::AppelFonctionContext::AppelFonctionContext(ParserRuleContext *parent, size_t invokingState)
  : ParserRuleContext(parent, invokingState) {
}

tree::TerminalNode* GrammaireParser::AppelFonctionContext::VAR() {
  return getToken(GrammaireParser::VAR, 0);
}

GrammaireParser::ParamAppelContext* GrammaireParser::AppelFonctionContext::paramAppel() {
  return getRuleContext<GrammaireParser::ParamAppelContext>(0);
}


size_t GrammaireParser::AppelFonctionContext::getRuleIndex() const {
  return GrammaireParser::RuleAppelFonction;
}

antlrcpp::Any GrammaireParser::AppelFonctionContext::accept(tree::ParseTreeVisitor *visitor) {
  if (auto parserVisitor = dynamic_cast<GrammaireVisitor*>(visitor))
    return parserVisitor->visitAppelFonction(this);
  else
    return visitor->visitChildren(this);
}

GrammaireParser::AppelFonctionContext* GrammaireParser::appelFonction() {
  AppelFonctionContext *_localctx = _tracker.createInstance<AppelFonctionContext>(_ctx, getState());
  enterRule(_localctx, 16, GrammaireParser::RuleAppelFonction);

  auto onExit = finally([=] {
    exitRule();
  });
  try {
    enterOuterAlt(_localctx, 1);
    setState(87);
    match(GrammaireParser::VAR);
    setState(88);
    match(GrammaireParser::T__3);
    setState(89);
    paramAppel();
    setState(90);
    match(GrammaireParser::T__4);
   
  }
  catch (RecognitionException &e) {
    _errHandler->reportError(this, e);
    _localctx->exception = std::current_exception();
    _errHandler->recover(this, _localctx->exception);
  }

  return _localctx;
}

//----------------- LvalContext ------------------------------------------------------------------

GrammaireParser::LvalContext::LvalContext(ParserRuleContext *parent, size_t invokingState)
  : ParserRuleContext(parent, invokingState) {
}


size_t GrammaireParser::LvalContext::getRuleIndex() const {
  return GrammaireParser::RuleLval;
}

void GrammaireParser::LvalContext::copyFrom(LvalContext *ctx) {
  ParserRuleContext::copyFrom(ctx);
}

//----------------- LvalTabContext ------------------------------------------------------------------

tree::TerminalNode* GrammaireParser::LvalTabContext::VAR() {
  return getToken(GrammaireParser::VAR, 0);
}

GrammaireParser::ExprContext* GrammaireParser::LvalTabContext::expr() {
  return getRuleContext<GrammaireParser::ExprContext>(0);
}

GrammaireParser::LvalTabContext::LvalTabContext(LvalContext *ctx) { copyFrom(ctx); }

antlrcpp::Any GrammaireParser::LvalTabContext::accept(tree::ParseTreeVisitor *visitor) {
  if (auto parserVisitor = dynamic_cast<GrammaireVisitor*>(visitor))
    return parserVisitor->visitLvalTab(this);
  else
    return visitor->visitChildren(this);
}
//----------------- LvalSimpleContext ------------------------------------------------------------------

tree::TerminalNode* GrammaireParser::LvalSimpleContext::VAR() {
  return getToken(GrammaireParser::VAR, 0);
}

GrammaireParser::LvalSimpleContext::LvalSimpleContext(LvalContext *ctx) { copyFrom(ctx); }

antlrcpp::Any GrammaireParser::LvalSimpleContext::accept(tree::ParseTreeVisitor *visitor) {
  if (auto parserVisitor = dynamic_cast<GrammaireVisitor*>(visitor))
    return parserVisitor->visitLvalSimple(this);
  else
    return visitor->visitChildren(this);
}
GrammaireParser::LvalContext* GrammaireParser::lval() {
  LvalContext *_localctx = _tracker.createInstance<LvalContext>(_ctx, getState());
  enterRule(_localctx, 18, GrammaireParser::RuleLval);

  auto onExit = finally([=] {
    exitRule();
  });
  try {
    setState(98);
    _errHandler->sync(this);
    switch (getInterpreter<atn::ParserATNSimulator>()->adaptivePredict(_input, 6, _ctx)) {
    case 1: {
      _localctx = dynamic_cast<LvalContext *>(_tracker.createInstance<GrammaireParser::LvalSimpleContext>(_localctx));
      enterOuterAlt(_localctx, 1);
      setState(92);
      match(GrammaireParser::VAR);
      break;
    }

    case 2: {
      _localctx = dynamic_cast<LvalContext *>(_tracker.createInstance<GrammaireParser::LvalTabContext>(_localctx));
      enterOuterAlt(_localctx, 2);
      setState(93);
      match(GrammaireParser::VAR);
      setState(94);
      match(GrammaireParser::T__1);
      setState(95);
      expr(0);
      setState(96);
      match(GrammaireParser::T__2);
      break;
    }

    }
   
  }
  catch (RecognitionException &e) {
    _errHandler->reportError(this, e);
    _localctx->exception = std::current_exception();
    _errHandler->recover(this, _localctx->exception);
  }

  return _localctx;
}

//----------------- ElementTabVContext ------------------------------------------------------------------

GrammaireParser::ElementTabVContext::ElementTabVContext(ParserRuleContext *parent, size_t invokingState)
  : ParserRuleContext(parent, invokingState) {
}

GrammaireParser::ExprContext* GrammaireParser::ElementTabVContext::expr() {
  return getRuleContext<GrammaireParser::ExprContext>(0);
}

GrammaireParser::ElementTabVContext* GrammaireParser::ElementTabVContext::elementTabV() {
  return getRuleContext<GrammaireParser::ElementTabVContext>(0);
}


size_t GrammaireParser::ElementTabVContext::getRuleIndex() const {
  return GrammaireParser::RuleElementTabV;
}

antlrcpp::Any GrammaireParser::ElementTabVContext::accept(tree::ParseTreeVisitor *visitor) {
  if (auto parserVisitor = dynamic_cast<GrammaireVisitor*>(visitor))
    return parserVisitor->visitElementTabV(this);
  else
    return visitor->visitChildren(this);
}

GrammaireParser::ElementTabVContext* GrammaireParser::elementTabV() {
  ElementTabVContext *_localctx = _tracker.createInstance<ElementTabVContext>(_ctx, getState());
  enterRule(_localctx, 20, GrammaireParser::RuleElementTabV);

  auto onExit = finally([=] {
    exitRule();
  });
  try {
    setState(105);
    _errHandler->sync(this);
    switch (_input->LA(1)) {
      case GrammaireParser::T__0: {
        enterOuterAlt(_localctx, 1);
        setState(100);
        match(GrammaireParser::T__0);
        setState(101);
        expr(0);
        setState(102);
        elementTabV();
        break;
      }

      case GrammaireParser::T__9: {
        enterOuterAlt(_localctx, 2);

        break;
      }

    default:
      throw NoViableAltException(this);
    }
   
  }
  catch (RecognitionException &e) {
    _errHandler->reportError(this, e);
    _localctx->exception = std::current_exception();
    _errHandler->recover(this, _localctx->exception);
  }

  return _localctx;
}

//----------------- ElementsTabContext ------------------------------------------------------------------

GrammaireParser::ElementsTabContext::ElementsTabContext(ParserRuleContext *parent, size_t invokingState)
  : ParserRuleContext(parent, invokingState) {
}

GrammaireParser::ExprContext* GrammaireParser::ElementsTabContext::expr() {
  return getRuleContext<GrammaireParser::ExprContext>(0);
}

GrammaireParser::ElementTabVContext* GrammaireParser::ElementsTabContext::elementTabV() {
  return getRuleContext<GrammaireParser::ElementTabVContext>(0);
}


size_t GrammaireParser::ElementsTabContext::getRuleIndex() const {
  return GrammaireParser::RuleElementsTab;
}

antlrcpp::Any GrammaireParser::ElementsTabContext::accept(tree::ParseTreeVisitor *visitor) {
  if (auto parserVisitor = dynamic_cast<GrammaireVisitor*>(visitor))
    return parserVisitor->visitElementsTab(this);
  else
    return visitor->visitChildren(this);
}

GrammaireParser::ElementsTabContext* GrammaireParser::elementsTab() {
  ElementsTabContext *_localctx = _tracker.createInstance<ElementsTabContext>(_ctx, getState());
  enterRule(_localctx, 22, GrammaireParser::RuleElementsTab);

  auto onExit = finally([=] {
    exitRule();
  });
  try {
    setState(111);
    _errHandler->sync(this);
    switch (_input->LA(1)) {
      case GrammaireParser::T__3:
      case GrammaireParser::T__11:
      case GrammaireParser::T__12:
      case GrammaireParser::T__13:
      case GrammaireParser::T__14:
      case GrammaireParser::T__15:
      case GrammaireParser::T__16:
      case GrammaireParser::VAR:
      case GrammaireParser::CHAR:
      case GrammaireParser::INT: {
        enterOuterAlt(_localctx, 1);
        setState(107);
        expr(0);
        setState(108);
        elementTabV();
        break;
      }

      case GrammaireParser::T__9: {
        enterOuterAlt(_localctx, 2);

        break;
      }

    default:
      throw NoViableAltException(this);
    }
   
  }
  catch (RecognitionException &e) {
    _errHandler->reportError(this, e);
    _localctx->exception = std::current_exception();
    _errHandler->recover(this, _localctx->exception);
  }

  return _localctx;
}

//----------------- DeclContext ------------------------------------------------------------------

GrammaireParser::DeclContext::DeclContext(ParserRuleContext *parent, size_t invokingState)
  : ParserRuleContext(parent, invokingState) {
}


size_t GrammaireParser::DeclContext::getRuleIndex() const {
  return GrammaireParser::RuleDecl;
}

void GrammaireParser::DeclContext::copyFrom(DeclContext *ctx) {
  ParserRuleContext::copyFrom(ctx);
}

//----------------- DeclTabDefContext ------------------------------------------------------------------

tree::TerminalNode* GrammaireParser::DeclTabDefContext::TYPE() {
  return getToken(GrammaireParser::TYPE, 0);
}

tree::TerminalNode* GrammaireParser::DeclTabDefContext::VAR() {
  return getToken(GrammaireParser::VAR, 0);
}

GrammaireParser::ElementsTabContext* GrammaireParser::DeclTabDefContext::elementsTab() {
  return getRuleContext<GrammaireParser::ElementsTabContext>(0);
}

GrammaireParser::DeclTabDefContext::DeclTabDefContext(DeclContext *ctx) { copyFrom(ctx); }

antlrcpp::Any GrammaireParser::DeclTabDefContext::accept(tree::ParseTreeVisitor *visitor) {
  if (auto parserVisitor = dynamic_cast<GrammaireVisitor*>(visitor))
    return parserVisitor->visitDeclTabDef(this);
  else
    return visitor->visitChildren(this);
}
//----------------- DeclLvalContext ------------------------------------------------------------------

tree::TerminalNode* GrammaireParser::DeclLvalContext::TYPE() {
  return getToken(GrammaireParser::TYPE, 0);
}

GrammaireParser::LvalContext* GrammaireParser::DeclLvalContext::lval() {
  return getRuleContext<GrammaireParser::LvalContext>(0);
}

GrammaireParser::DeclLvalContext::DeclLvalContext(DeclContext *ctx) { copyFrom(ctx); }

antlrcpp::Any GrammaireParser::DeclLvalContext::accept(tree::ParseTreeVisitor *visitor) {
  if (auto parserVisitor = dynamic_cast<GrammaireVisitor*>(visitor))
    return parserVisitor->visitDeclLval(this);
  else
    return visitor->visitChildren(this);
}
//----------------- DeclDefContext ------------------------------------------------------------------

tree::TerminalNode* GrammaireParser::DeclDefContext::TYPE() {
  return getToken(GrammaireParser::TYPE, 0);
}

GrammaireParser::DefContext* GrammaireParser::DeclDefContext::def() {
  return getRuleContext<GrammaireParser::DefContext>(0);
}

GrammaireParser::DeclDefContext::DeclDefContext(DeclContext *ctx) { copyFrom(ctx); }

antlrcpp::Any GrammaireParser::DeclDefContext::accept(tree::ParseTreeVisitor *visitor) {
  if (auto parserVisitor = dynamic_cast<GrammaireVisitor*>(visitor))
    return parserVisitor->visitDeclDef(this);
  else
    return visitor->visitChildren(this);
}
GrammaireParser::DeclContext* GrammaireParser::decl() {
  DeclContext *_localctx = _tracker.createInstance<DeclContext>(_ctx, getState());
  enterRule(_localctx, 24, GrammaireParser::RuleDecl);

  auto onExit = finally([=] {
    exitRule();
  });
  try {
    setState(131);
    _errHandler->sync(this);
    switch (getInterpreter<atn::ParserATNSimulator>()->adaptivePredict(_input, 9, _ctx)) {
    case 1: {
      _localctx = dynamic_cast<DeclContext *>(_tracker.createInstance<GrammaireParser::DeclDefContext>(_localctx));
      enterOuterAlt(_localctx, 1);
      setState(113);
      match(GrammaireParser::TYPE);
      setState(114);
      def();
      setState(115);
      match(GrammaireParser::T__6);
      break;
    }

    case 2: {
      _localctx = dynamic_cast<DeclContext *>(_tracker.createInstance<GrammaireParser::DeclLvalContext>(_localctx));
      enterOuterAlt(_localctx, 2);
      setState(117);
      match(GrammaireParser::TYPE);
      setState(118);
      lval();
      setState(119);
      match(GrammaireParser::T__6);
      break;
    }

    case 3: {
      _localctx = dynamic_cast<DeclContext *>(_tracker.createInstance<GrammaireParser::DeclTabDefContext>(_localctx));
      enterOuterAlt(_localctx, 3);
      setState(121);
      match(GrammaireParser::TYPE);
      setState(122);
      match(GrammaireParser::VAR);
      setState(123);
      match(GrammaireParser::T__1);
      setState(124);
      match(GrammaireParser::T__2);
      setState(125);
      match(GrammaireParser::T__7);
      setState(126);
      match(GrammaireParser::T__8);
      setState(127);
      elementsTab();
      setState(128);
      match(GrammaireParser::T__9);
      setState(129);
      match(GrammaireParser::T__6);
      break;
    }

    }
   
  }
  catch (RecognitionException &e) {
    _errHandler->reportError(this, e);
    _localctx->exception = std::current_exception();
    _errHandler->recover(this, _localctx->exception);
  }

  return _localctx;
}

//----------------- DefContext ------------------------------------------------------------------

GrammaireParser::DefContext::DefContext(ParserRuleContext *parent, size_t invokingState)
  : ParserRuleContext(parent, invokingState) {
}

GrammaireParser::LvalContext* GrammaireParser::DefContext::lval() {
  return getRuleContext<GrammaireParser::LvalContext>(0);
}

GrammaireParser::ExprContext* GrammaireParser::DefContext::expr() {
  return getRuleContext<GrammaireParser::ExprContext>(0);
}


size_t GrammaireParser::DefContext::getRuleIndex() const {
  return GrammaireParser::RuleDef;
}

antlrcpp::Any GrammaireParser::DefContext::accept(tree::ParseTreeVisitor *visitor) {
  if (auto parserVisitor = dynamic_cast<GrammaireVisitor*>(visitor))
    return parserVisitor->visitDef(this);
  else
    return visitor->visitChildren(this);
}

GrammaireParser::DefContext* GrammaireParser::def() {
  DefContext *_localctx = _tracker.createInstance<DefContext>(_ctx, getState());
  enterRule(_localctx, 26, GrammaireParser::RuleDef);

  auto onExit = finally([=] {
    exitRule();
  });
  try {
    enterOuterAlt(_localctx, 1);
    setState(133);
    lval();
    setState(134);
    match(GrammaireParser::T__7);
    setState(135);
    expr(0);
   
  }
  catch (RecognitionException &e) {
    _errHandler->reportError(this, e);
    _localctx->exception = std::current_exception();
    _errHandler->recover(this, _localctx->exception);
  }

  return _localctx;
}

//----------------- InstrContext ------------------------------------------------------------------

GrammaireParser::InstrContext::InstrContext(ParserRuleContext *parent, size_t invokingState)
  : ParserRuleContext(parent, invokingState) {
}


size_t GrammaireParser::InstrContext::getRuleIndex() const {
  return GrammaireParser::RuleInstr;
}

void GrammaireParser::InstrContext::copyFrom(InstrContext *ctx) {
  ParserRuleContext::copyFrom(ctx);
}

//----------------- InstrDeclContext ------------------------------------------------------------------

GrammaireParser::DeclContext* GrammaireParser::InstrDeclContext::decl() {
  return getRuleContext<GrammaireParser::DeclContext>(0);
}

GrammaireParser::InstrDeclContext::InstrDeclContext(InstrContext *ctx) { copyFrom(ctx); }

antlrcpp::Any GrammaireParser::InstrDeclContext::accept(tree::ParseTreeVisitor *visitor) {
  if (auto parserVisitor = dynamic_cast<GrammaireVisitor*>(visitor))
    return parserVisitor->visitInstrDecl(this);
  else
    return visitor->visitChildren(this);
}
//----------------- InstrControleContext ------------------------------------------------------------------

GrammaireParser::ControleContext* GrammaireParser::InstrControleContext::controle() {
  return getRuleContext<GrammaireParser::ControleContext>(0);
}

GrammaireParser::InstrControleContext::InstrControleContext(InstrContext *ctx) { copyFrom(ctx); }

antlrcpp::Any GrammaireParser::InstrControleContext::accept(tree::ParseTreeVisitor *visitor) {
  if (auto parserVisitor = dynamic_cast<GrammaireVisitor*>(visitor))
    return parserVisitor->visitInstrControle(this);
  else
    return visitor->visitChildren(this);
}
//----------------- InstrExpContext ------------------------------------------------------------------

GrammaireParser::ExprContext* GrammaireParser::InstrExpContext::expr() {
  return getRuleContext<GrammaireParser::ExprContext>(0);
}

GrammaireParser::InstrExpContext::InstrExpContext(InstrContext *ctx) { copyFrom(ctx); }

antlrcpp::Any GrammaireParser::InstrExpContext::accept(tree::ParseTreeVisitor *visitor) {
  if (auto parserVisitor = dynamic_cast<GrammaireVisitor*>(visitor))
    return parserVisitor->visitInstrExp(this);
  else
    return visitor->visitChildren(this);
}
//----------------- InstrReturnContext ------------------------------------------------------------------

GrammaireParser::ExprContext* GrammaireParser::InstrReturnContext::expr() {
  return getRuleContext<GrammaireParser::ExprContext>(0);
}

GrammaireParser::InstrReturnContext::InstrReturnContext(InstrContext *ctx) { copyFrom(ctx); }

antlrcpp::Any GrammaireParser::InstrReturnContext::accept(tree::ParseTreeVisitor *visitor) {
  if (auto parserVisitor = dynamic_cast<GrammaireVisitor*>(visitor))
    return parserVisitor->visitInstrReturn(this);
  else
    return visitor->visitChildren(this);
}
GrammaireParser::InstrContext* GrammaireParser::instr() {
  InstrContext *_localctx = _tracker.createInstance<InstrContext>(_ctx, getState());
  enterRule(_localctx, 28, GrammaireParser::RuleInstr);

  auto onExit = finally([=] {
    exitRule();
  });
  try {
    setState(146);
    _errHandler->sync(this);
    switch (_input->LA(1)) {
      case GrammaireParser::T__10: {
        _localctx = dynamic_cast<InstrContext *>(_tracker.createInstance<GrammaireParser::InstrReturnContext>(_localctx));
        enterOuterAlt(_localctx, 1);
        setState(137);
        match(GrammaireParser::T__10);
        setState(138);
        expr(0);
        setState(139);
        match(GrammaireParser::T__6);
        break;
      }

      case GrammaireParser::T__3:
      case GrammaireParser::T__11:
      case GrammaireParser::T__12:
      case GrammaireParser::T__13:
      case GrammaireParser::T__14:
      case GrammaireParser::T__15:
      case GrammaireParser::T__16:
      case GrammaireParser::VAR:
      case GrammaireParser::CHAR:
      case GrammaireParser::INT: {
        _localctx = dynamic_cast<InstrContext *>(_tracker.createInstance<GrammaireParser::InstrExpContext>(_localctx));
        enterOuterAlt(_localctx, 2);
        setState(141);
        expr(0);
        setState(142);
        match(GrammaireParser::T__6);
        break;
      }

      case GrammaireParser::TYPE: {
        _localctx = dynamic_cast<InstrContext *>(_tracker.createInstance<GrammaireParser::InstrDeclContext>(_localctx));
        enterOuterAlt(_localctx, 3);
        setState(144);
        decl();
        break;
      }

      case GrammaireParser::T__36:
      case GrammaireParser::T__37: {
        _localctx = dynamic_cast<InstrContext *>(_tracker.createInstance<GrammaireParser::InstrControleContext>(_localctx));
        enterOuterAlt(_localctx, 4);
        setState(145);
        controle();
        break;
      }

    default:
      throw NoViableAltException(this);
    }
   
  }
  catch (RecognitionException &e) {
    _errHandler->reportError(this, e);
    _localctx->exception = std::current_exception();
    _errHandler->recover(this, _localctx->exception);
  }

  return _localctx;
}

//----------------- ExprContext ------------------------------------------------------------------

GrammaireParser::ExprContext::ExprContext(ParserRuleContext *parent, size_t invokingState)
  : ParserRuleContext(parent, invokingState) {
}


size_t GrammaireParser::ExprContext::getRuleIndex() const {
  return GrammaireParser::RuleExpr;
}

void GrammaireParser::ExprContext::copyFrom(ExprContext *ctx) {
  ParserRuleContext::copyFrom(ctx);
}

//----------------- ExprOrBitContext ------------------------------------------------------------------

std::vector<GrammaireParser::ExprContext *> GrammaireParser::ExprOrBitContext::expr() {
  return getRuleContexts<GrammaireParser::ExprContext>();
}

GrammaireParser::ExprContext* GrammaireParser::ExprOrBitContext::expr(size_t i) {
  return getRuleContext<GrammaireParser::ExprContext>(i);
}

GrammaireParser::ExprOrBitContext::ExprOrBitContext(ExprContext *ctx) { copyFrom(ctx); }

antlrcpp::Any GrammaireParser::ExprOrBitContext::accept(tree::ParseTreeVisitor *visitor) {
  if (auto parserVisitor = dynamic_cast<GrammaireVisitor*>(visitor))
    return parserVisitor->visitExprOrBit(this);
  else
    return visitor->visitChildren(this);
}
//----------------- ExprLtContext ------------------------------------------------------------------

std::vector<GrammaireParser::ExprContext *> GrammaireParser::ExprLtContext::expr() {
  return getRuleContexts<GrammaireParser::ExprContext>();
}

GrammaireParser::ExprContext* GrammaireParser::ExprLtContext::expr(size_t i) {
  return getRuleContext<GrammaireParser::ExprContext>(i);
}

GrammaireParser::ExprLtContext::ExprLtContext(ExprContext *ctx) { copyFrom(ctx); }

antlrcpp::Any GrammaireParser::ExprLtContext::accept(tree::ParseTreeVisitor *visitor) {
  if (auto parserVisitor = dynamic_cast<GrammaireVisitor*>(visitor))
    return parserVisitor->visitExprLt(this);
  else
    return visitor->visitChildren(this);
}
//----------------- ExprLdecContext ------------------------------------------------------------------

GrammaireParser::LvalContext* GrammaireParser::ExprLdecContext::lval() {
  return getRuleContext<GrammaireParser::LvalContext>(0);
}

GrammaireParser::ExprLdecContext::ExprLdecContext(ExprContext *ctx) { copyFrom(ctx); }

antlrcpp::Any GrammaireParser::ExprLdecContext::accept(tree::ParseTreeVisitor *visitor) {
  if (auto parserVisitor = dynamic_cast<GrammaireVisitor*>(visitor))
    return parserVisitor->visitExprLdec(this);
  else
    return visitor->visitChildren(this);
}
//----------------- ExprAndContext ------------------------------------------------------------------

std::vector<GrammaireParser::ExprContext *> GrammaireParser::ExprAndContext::expr() {
  return getRuleContexts<GrammaireParser::ExprContext>();
}

GrammaireParser::ExprContext* GrammaireParser::ExprAndContext::expr(size_t i) {
  return getRuleContext<GrammaireParser::ExprContext>(i);
}

GrammaireParser::ExprAndContext::ExprAndContext(ExprContext *ctx) { copyFrom(ctx); }

antlrcpp::Any GrammaireParser::ExprAndContext::accept(tree::ParseTreeVisitor *visitor) {
  if (auto parserVisitor = dynamic_cast<GrammaireVisitor*>(visitor))
    return parserVisitor->visitExprAnd(this);
  else
    return visitor->visitChildren(this);
}
//----------------- ExprVarContext ------------------------------------------------------------------

GrammaireParser::LvalContext* GrammaireParser::ExprVarContext::lval() {
  return getRuleContext<GrammaireParser::LvalContext>(0);
}

GrammaireParser::ExprVarContext::ExprVarContext(ExprContext *ctx) { copyFrom(ctx); }

antlrcpp::Any GrammaireParser::ExprVarContext::accept(tree::ParseTreeVisitor *visitor) {
  if (auto parserVisitor = dynamic_cast<GrammaireVisitor*>(visitor))
    return parserVisitor->visitExprVar(this);
  else
    return visitor->visitChildren(this);
}
//----------------- ExprNegBinContext ------------------------------------------------------------------

GrammaireParser::ExprContext* GrammaireParser::ExprNegBinContext::expr() {
  return getRuleContext<GrammaireParser::ExprContext>(0);
}

GrammaireParser::ExprNegBinContext::ExprNegBinContext(ExprContext *ctx) { copyFrom(ctx); }

antlrcpp::Any GrammaireParser::ExprNegBinContext::accept(tree::ParseTreeVisitor *visitor) {
  if (auto parserVisitor = dynamic_cast<GrammaireVisitor*>(visitor))
    return parserVisitor->visitExprNegBin(this);
  else
    return visitor->visitChildren(this);
}
//----------------- ExprDecRContext ------------------------------------------------------------------

std::vector<GrammaireParser::ExprContext *> GrammaireParser::ExprDecRContext::expr() {
  return getRuleContexts<GrammaireParser::ExprContext>();
}

GrammaireParser::ExprContext* GrammaireParser::ExprDecRContext::expr(size_t i) {
  return getRuleContext<GrammaireParser::ExprContext>(i);
}

GrammaireParser::ExprDecRContext::ExprDecRContext(ExprContext *ctx) { copyFrom(ctx); }

antlrcpp::Any GrammaireParser::ExprDecRContext::accept(tree::ParseTreeVisitor *visitor) {
  if (auto parserVisitor = dynamic_cast<GrammaireVisitor*>(visitor))
    return parserVisitor->visitExprDecR(this);
  else
    return visitor->visitChildren(this);
}
//----------------- ExprMultDivContext ------------------------------------------------------------------

std::vector<GrammaireParser::ExprContext *> GrammaireParser::ExprMultDivContext::expr() {
  return getRuleContexts<GrammaireParser::ExprContext>();
}

GrammaireParser::ExprContext* GrammaireParser::ExprMultDivContext::expr(size_t i) {
  return getRuleContext<GrammaireParser::ExprContext>(i);
}

GrammaireParser::ExprMultDivContext::ExprMultDivContext(ExprContext *ctx) { copyFrom(ctx); }

antlrcpp::Any GrammaireParser::ExprMultDivContext::accept(tree::ParseTreeVisitor *visitor) {
  if (auto parserVisitor = dynamic_cast<GrammaireVisitor*>(visitor))
    return parserVisitor->visitExprMultDiv(this);
  else
    return visitor->visitChildren(this);
}
//----------------- ExprFuncContext ------------------------------------------------------------------

GrammaireParser::AppelFonctionContext* GrammaireParser::ExprFuncContext::appelFonction() {
  return getRuleContext<GrammaireParser::AppelFonctionContext>(0);
}

GrammaireParser::ExprFuncContext::ExprFuncContext(ExprContext *ctx) { copyFrom(ctx); }

antlrcpp::Any GrammaireParser::ExprFuncContext::accept(tree::ParseTreeVisitor *visitor) {
  if (auto parserVisitor = dynamic_cast<GrammaireVisitor*>(visitor))
    return parserVisitor->visitExprFunc(this);
  else
    return visitor->visitChildren(this);
}
//----------------- ExprLeqContext ------------------------------------------------------------------

std::vector<GrammaireParser::ExprContext *> GrammaireParser::ExprLeqContext::expr() {
  return getRuleContexts<GrammaireParser::ExprContext>();
}

GrammaireParser::ExprContext* GrammaireParser::ExprLeqContext::expr(size_t i) {
  return getRuleContext<GrammaireParser::ExprContext>(i);
}

GrammaireParser::ExprLeqContext::ExprLeqContext(ExprContext *ctx) { copyFrom(ctx); }

antlrcpp::Any GrammaireParser::ExprLeqContext::accept(tree::ParseTreeVisitor *visitor) {
  if (auto parserVisitor = dynamic_cast<GrammaireVisitor*>(visitor))
    return parserVisitor->visitExprLeq(this);
  else
    return visitor->visitChildren(this);
}
//----------------- ExprNegContext ------------------------------------------------------------------

GrammaireParser::ExprContext* GrammaireParser::ExprNegContext::expr() {
  return getRuleContext<GrammaireParser::ExprContext>(0);
}

GrammaireParser::ExprNegContext::ExprNegContext(ExprContext *ctx) { copyFrom(ctx); }

antlrcpp::Any GrammaireParser::ExprNegContext::accept(tree::ParseTreeVisitor *visitor) {
  if (auto parserVisitor = dynamic_cast<GrammaireVisitor*>(visitor))
    return parserVisitor->visitExprNeg(this);
  else
    return visitor->visitChildren(this);
}
//----------------- ExprLincContext ------------------------------------------------------------------

GrammaireParser::LvalContext* GrammaireParser::ExprLincContext::lval() {
  return getRuleContext<GrammaireParser::LvalContext>(0);
}

GrammaireParser::ExprLincContext::ExprLincContext(ExprContext *ctx) { copyFrom(ctx); }

antlrcpp::Any GrammaireParser::ExprLincContext::accept(tree::ParseTreeVisitor *visitor) {
  if (auto parserVisitor = dynamic_cast<GrammaireVisitor*>(visitor))
    return parserVisitor->visitExprLinc(this);
  else
    return visitor->visitChildren(this);
}
//----------------- ExprEqContext ------------------------------------------------------------------

std::vector<GrammaireParser::ExprContext *> GrammaireParser::ExprEqContext::expr() {
  return getRuleContexts<GrammaireParser::ExprContext>();
}

GrammaireParser::ExprContext* GrammaireParser::ExprEqContext::expr(size_t i) {
  return getRuleContext<GrammaireParser::ExprContext>(i);
}

GrammaireParser::ExprEqContext::ExprEqContext(ExprContext *ctx) { copyFrom(ctx); }

antlrcpp::Any GrammaireParser::ExprEqContext::accept(tree::ParseTreeVisitor *visitor) {
  if (auto parserVisitor = dynamic_cast<GrammaireVisitor*>(visitor))
    return parserVisitor->visitExprEq(this);
  else
    return visitor->visitChildren(this);
}
//----------------- ExprNeqContext ------------------------------------------------------------------

std::vector<GrammaireParser::ExprContext *> GrammaireParser::ExprNeqContext::expr() {
  return getRuleContexts<GrammaireParser::ExprContext>();
}

GrammaireParser::ExprContext* GrammaireParser::ExprNeqContext::expr(size_t i) {
  return getRuleContext<GrammaireParser::ExprContext>(i);
}

GrammaireParser::ExprNeqContext::ExprNeqContext(ExprContext *ctx) { copyFrom(ctx); }

antlrcpp::Any GrammaireParser::ExprNeqContext::accept(tree::ParseTreeVisitor *visitor) {
  if (auto parserVisitor = dynamic_cast<GrammaireVisitor*>(visitor))
    return parserVisitor->visitExprNeq(this);
  else
    return visitor->visitChildren(this);
}
//----------------- ExprCharContext ------------------------------------------------------------------

tree::TerminalNode* GrammaireParser::ExprCharContext::CHAR() {
  return getToken(GrammaireParser::CHAR, 0);
}

GrammaireParser::ExprCharContext::ExprCharContext(ExprContext *ctx) { copyFrom(ctx); }

antlrcpp::Any GrammaireParser::ExprCharContext::accept(tree::ParseTreeVisitor *visitor) {
  if (auto parserVisitor = dynamic_cast<GrammaireVisitor*>(visitor))
    return parserVisitor->visitExprChar(this);
  else
    return visitor->visitChildren(this);
}
//----------------- ExprConstContext ------------------------------------------------------------------

tree::TerminalNode* GrammaireParser::ExprConstContext::INT() {
  return getToken(GrammaireParser::INT, 0);
}

GrammaireParser::ExprConstContext::ExprConstContext(ExprContext *ctx) { copyFrom(ctx); }

antlrcpp::Any GrammaireParser::ExprConstContext::accept(tree::ParseTreeVisitor *visitor) {
  if (auto parserVisitor = dynamic_cast<GrammaireVisitor*>(visitor))
    return parserVisitor->visitExprConst(this);
  else
    return visitor->visitChildren(this);
}
//----------------- ExprDecLContext ------------------------------------------------------------------

std::vector<GrammaireParser::ExprContext *> GrammaireParser::ExprDecLContext::expr() {
  return getRuleContexts<GrammaireParser::ExprContext>();
}

GrammaireParser::ExprContext* GrammaireParser::ExprDecLContext::expr(size_t i) {
  return getRuleContext<GrammaireParser::ExprContext>(i);
}

GrammaireParser::ExprDecLContext::ExprDecLContext(ExprContext *ctx) { copyFrom(ctx); }

antlrcpp::Any GrammaireParser::ExprDecLContext::accept(tree::ParseTreeVisitor *visitor) {
  if (auto parserVisitor = dynamic_cast<GrammaireVisitor*>(visitor))
    return parserVisitor->visitExprDecL(this);
  else
    return visitor->visitChildren(this);
}
//----------------- ExprXorBitContext ------------------------------------------------------------------

std::vector<GrammaireParser::ExprContext *> GrammaireParser::ExprXorBitContext::expr() {
  return getRuleContexts<GrammaireParser::ExprContext>();
}

GrammaireParser::ExprContext* GrammaireParser::ExprXorBitContext::expr(size_t i) {
  return getRuleContext<GrammaireParser::ExprContext>(i);
}

GrammaireParser::ExprXorBitContext::ExprXorBitContext(ExprContext *ctx) { copyFrom(ctx); }

antlrcpp::Any GrammaireParser::ExprXorBitContext::accept(tree::ParseTreeVisitor *visitor) {
  if (auto parserVisitor = dynamic_cast<GrammaireVisitor*>(visitor))
    return parserVisitor->visitExprXorBit(this);
  else
    return visitor->visitChildren(this);
}
//----------------- ExprGtContext ------------------------------------------------------------------

std::vector<GrammaireParser::ExprContext *> GrammaireParser::ExprGtContext::expr() {
  return getRuleContexts<GrammaireParser::ExprContext>();
}

GrammaireParser::ExprContext* GrammaireParser::ExprGtContext::expr(size_t i) {
  return getRuleContext<GrammaireParser::ExprContext>(i);
}

GrammaireParser::ExprGtContext::ExprGtContext(ExprContext *ctx) { copyFrom(ctx); }

antlrcpp::Any GrammaireParser::ExprGtContext::accept(tree::ParseTreeVisitor *visitor) {
  if (auto parserVisitor = dynamic_cast<GrammaireVisitor*>(visitor))
    return parserVisitor->visitExprGt(this);
  else
    return visitor->visitChildren(this);
}
//----------------- ExprIncContext ------------------------------------------------------------------

GrammaireParser::LvalContext* GrammaireParser::ExprIncContext::lval() {
  return getRuleContext<GrammaireParser::LvalContext>(0);
}

GrammaireParser::ExprIncContext::ExprIncContext(ExprContext *ctx) { copyFrom(ctx); }

antlrcpp::Any GrammaireParser::ExprIncContext::accept(tree::ParseTreeVisitor *visitor) {
  if (auto parserVisitor = dynamic_cast<GrammaireVisitor*>(visitor))
    return parserVisitor->visitExprInc(this);
  else
    return visitor->visitChildren(this);
}
//----------------- ExprSizeEContext ------------------------------------------------------------------

GrammaireParser::ExprContext* GrammaireParser::ExprSizeEContext::expr() {
  return getRuleContext<GrammaireParser::ExprContext>(0);
}

GrammaireParser::ExprSizeEContext::ExprSizeEContext(ExprContext *ctx) { copyFrom(ctx); }

antlrcpp::Any GrammaireParser::ExprSizeEContext::accept(tree::ParseTreeVisitor *visitor) {
  if (auto parserVisitor = dynamic_cast<GrammaireVisitor*>(visitor))
    return parserVisitor->visitExprSizeE(this);
  else
    return visitor->visitChildren(this);
}
//----------------- ExprParContext ------------------------------------------------------------------

GrammaireParser::ExprContext* GrammaireParser::ExprParContext::expr() {
  return getRuleContext<GrammaireParser::ExprContext>(0);
}

GrammaireParser::ExprParContext::ExprParContext(ExprContext *ctx) { copyFrom(ctx); }

antlrcpp::Any GrammaireParser::ExprParContext::accept(tree::ParseTreeVisitor *visitor) {
  if (auto parserVisitor = dynamic_cast<GrammaireVisitor*>(visitor))
    return parserVisitor->visitExprPar(this);
  else
    return visitor->visitChildren(this);
}
//----------------- ExprGeqContext ------------------------------------------------------------------

std::vector<GrammaireParser::ExprContext *> GrammaireParser::ExprGeqContext::expr() {
  return getRuleContexts<GrammaireParser::ExprContext>();
}

GrammaireParser::ExprContext* GrammaireParser::ExprGeqContext::expr(size_t i) {
  return getRuleContext<GrammaireParser::ExprContext>(i);
}

GrammaireParser::ExprGeqContext::ExprGeqContext(ExprContext *ctx) { copyFrom(ctx); }

antlrcpp::Any GrammaireParser::ExprGeqContext::accept(tree::ParseTreeVisitor *visitor) {
  if (auto parserVisitor = dynamic_cast<GrammaireVisitor*>(visitor))
    return parserVisitor->visitExprGeq(this);
  else
    return visitor->visitChildren(this);
}
//----------------- ExprCastContext ------------------------------------------------------------------

tree::TerminalNode* GrammaireParser::ExprCastContext::TYPE() {
  return getToken(GrammaireParser::TYPE, 0);
}

GrammaireParser::ExprContext* GrammaireParser::ExprCastContext::expr() {
  return getRuleContext<GrammaireParser::ExprContext>(0);
}

GrammaireParser::ExprCastContext::ExprCastContext(ExprContext *ctx) { copyFrom(ctx); }

antlrcpp::Any GrammaireParser::ExprCastContext::accept(tree::ParseTreeVisitor *visitor) {
  if (auto parserVisitor = dynamic_cast<GrammaireVisitor*>(visitor))
    return parserVisitor->visitExprCast(this);
  else
    return visitor->visitChildren(this);
}
//----------------- ExprOrContext ------------------------------------------------------------------

std::vector<GrammaireParser::ExprContext *> GrammaireParser::ExprOrContext::expr() {
  return getRuleContexts<GrammaireParser::ExprContext>();
}

GrammaireParser::ExprContext* GrammaireParser::ExprOrContext::expr(size_t i) {
  return getRuleContext<GrammaireParser::ExprContext>(i);
}

GrammaireParser::ExprOrContext::ExprOrContext(ExprContext *ctx) { copyFrom(ctx); }

antlrcpp::Any GrammaireParser::ExprOrContext::accept(tree::ParseTreeVisitor *visitor) {
  if (auto parserVisitor = dynamic_cast<GrammaireVisitor*>(visitor))
    return parserVisitor->visitExprOr(this);
  else
    return visitor->visitChildren(this);
}
//----------------- ExprAndBitContext ------------------------------------------------------------------

std::vector<GrammaireParser::ExprContext *> GrammaireParser::ExprAndBitContext::expr() {
  return getRuleContexts<GrammaireParser::ExprContext>();
}

GrammaireParser::ExprContext* GrammaireParser::ExprAndBitContext::expr(size_t i) {
  return getRuleContext<GrammaireParser::ExprContext>(i);
}

GrammaireParser::ExprAndBitContext::ExprAndBitContext(ExprContext *ctx) { copyFrom(ctx); }

antlrcpp::Any GrammaireParser::ExprAndBitContext::accept(tree::ParseTreeVisitor *visitor) {
  if (auto parserVisitor = dynamic_cast<GrammaireVisitor*>(visitor))
    return parserVisitor->visitExprAndBit(this);
  else
    return visitor->visitChildren(this);
}
//----------------- ExprDecContext ------------------------------------------------------------------

GrammaireParser::LvalContext* GrammaireParser::ExprDecContext::lval() {
  return getRuleContext<GrammaireParser::LvalContext>(0);
}

GrammaireParser::ExprDecContext::ExprDecContext(ExprContext *ctx) { copyFrom(ctx); }

antlrcpp::Any GrammaireParser::ExprDecContext::accept(tree::ParseTreeVisitor *visitor) {
  if (auto parserVisitor = dynamic_cast<GrammaireVisitor*>(visitor))
    return parserVisitor->visitExprDec(this);
  else
    return visitor->visitChildren(this);
}
//----------------- ExprTerContext ------------------------------------------------------------------

std::vector<GrammaireParser::ExprContext *> GrammaireParser::ExprTerContext::expr() {
  return getRuleContexts<GrammaireParser::ExprContext>();
}

GrammaireParser::ExprContext* GrammaireParser::ExprTerContext::expr(size_t i) {
  return getRuleContext<GrammaireParser::ExprContext>(i);
}

GrammaireParser::ExprTerContext::ExprTerContext(ExprContext *ctx) { copyFrom(ctx); }

antlrcpp::Any GrammaireParser::ExprTerContext::accept(tree::ParseTreeVisitor *visitor) {
  if (auto parserVisitor = dynamic_cast<GrammaireVisitor*>(visitor))
    return parserVisitor->visitExprTer(this);
  else
    return visitor->visitChildren(this);
}
//----------------- ExprDefContext ------------------------------------------------------------------

GrammaireParser::DefContext* GrammaireParser::ExprDefContext::def() {
  return getRuleContext<GrammaireParser::DefContext>(0);
}

GrammaireParser::ExprDefContext::ExprDefContext(ExprContext *ctx) { copyFrom(ctx); }

antlrcpp::Any GrammaireParser::ExprDefContext::accept(tree::ParseTreeVisitor *visitor) {
  if (auto parserVisitor = dynamic_cast<GrammaireVisitor*>(visitor))
    return parserVisitor->visitExprDef(this);
  else
    return visitor->visitChildren(this);
}
//----------------- ExprAddMoinsContext ------------------------------------------------------------------

std::vector<GrammaireParser::ExprContext *> GrammaireParser::ExprAddMoinsContext::expr() {
  return getRuleContexts<GrammaireParser::ExprContext>();
}

GrammaireParser::ExprContext* GrammaireParser::ExprAddMoinsContext::expr(size_t i) {
  return getRuleContext<GrammaireParser::ExprContext>(i);
}

GrammaireParser::ExprAddMoinsContext::ExprAddMoinsContext(ExprContext *ctx) { copyFrom(ctx); }

antlrcpp::Any GrammaireParser::ExprAddMoinsContext::accept(tree::ParseTreeVisitor *visitor) {
  if (auto parserVisitor = dynamic_cast<GrammaireVisitor*>(visitor))
    return parserVisitor->visitExprAddMoins(this);
  else
    return visitor->visitChildren(this);
}
//----------------- ExprModContext ------------------------------------------------------------------

std::vector<GrammaireParser::ExprContext *> GrammaireParser::ExprModContext::expr() {
  return getRuleContexts<GrammaireParser::ExprContext>();
}

GrammaireParser::ExprContext* GrammaireParser::ExprModContext::expr(size_t i) {
  return getRuleContext<GrammaireParser::ExprContext>(i);
}

GrammaireParser::ExprModContext::ExprModContext(ExprContext *ctx) { copyFrom(ctx); }

antlrcpp::Any GrammaireParser::ExprModContext::accept(tree::ParseTreeVisitor *visitor) {
  if (auto parserVisitor = dynamic_cast<GrammaireVisitor*>(visitor))
    return parserVisitor->visitExprMod(this);
  else
    return visitor->visitChildren(this);
}
//----------------- ExprSizeTContext ------------------------------------------------------------------

tree::TerminalNode* GrammaireParser::ExprSizeTContext::TYPE() {
  return getToken(GrammaireParser::TYPE, 0);
}

GrammaireParser::ExprSizeTContext::ExprSizeTContext(ExprContext *ctx) { copyFrom(ctx); }

antlrcpp::Any GrammaireParser::ExprSizeTContext::accept(tree::ParseTreeVisitor *visitor) {
  if (auto parserVisitor = dynamic_cast<GrammaireVisitor*>(visitor))
    return parserVisitor->visitExprSizeT(this);
  else
    return visitor->visitChildren(this);
}
//----------------- ExprNonContext ------------------------------------------------------------------

GrammaireParser::ExprContext* GrammaireParser::ExprNonContext::expr() {
  return getRuleContext<GrammaireParser::ExprContext>(0);
}

GrammaireParser::ExprNonContext::ExprNonContext(ExprContext *ctx) { copyFrom(ctx); }

antlrcpp::Any GrammaireParser::ExprNonContext::accept(tree::ParseTreeVisitor *visitor) {
  if (auto parserVisitor = dynamic_cast<GrammaireVisitor*>(visitor))
    return parserVisitor->visitExprNon(this);
  else
    return visitor->visitChildren(this);
}

GrammaireParser::ExprContext* GrammaireParser::expr() {
   return expr(0);
}

GrammaireParser::ExprContext* GrammaireParser::expr(int precedence) {
  ParserRuleContext *parentContext = _ctx;
  size_t parentState = getState();
  GrammaireParser::ExprContext *_localctx = _tracker.createInstance<ExprContext>(_ctx, parentState);
  GrammaireParser::ExprContext *previousContext = _localctx;
  size_t startState = 30;
  enterRecursionRule(_localctx, 30, GrammaireParser::RuleExpr, precedence);

    size_t _la = 0;

  auto onExit = finally([=] {
    unrollRecursionContexts(parentContext);
  });
  try {
    size_t alt;
    enterOuterAlt(_localctx, 1);
    setState(184);
    _errHandler->sync(this);
    switch (getInterpreter<atn::ParserATNSimulator>()->adaptivePredict(_input, 11, _ctx)) {
    case 1: {
      _localctx = _tracker.createInstance<ExprFuncContext>(_localctx);
      _ctx = _localctx;
      previousContext = _localctx;

      setState(149);
      appelFonction();
      break;
    }

    case 2: {
      _localctx = _tracker.createInstance<ExprDefContext>(_localctx);
      _ctx = _localctx;
      previousContext = _localctx;
      setState(150);
      def();
      break;
    }

    case 3: {
      _localctx = _tracker.createInstance<ExprParContext>(_localctx);
      _ctx = _localctx;
      previousContext = _localctx;
      setState(151);
      match(GrammaireParser::T__3);
      setState(152);
      expr(0);
      setState(153);
      match(GrammaireParser::T__4);
      break;
    }

    case 4: {
      _localctx = _tracker.createInstance<ExprSizeEContext>(_localctx);
      _ctx = _localctx;
      previousContext = _localctx;
      setState(155);
      match(GrammaireParser::T__11);
      setState(156);
      expr(30);
      break;
    }

    case 5: {
      _localctx = _tracker.createInstance<ExprSizeTContext>(_localctx);
      _ctx = _localctx;
      previousContext = _localctx;
      setState(157);
      match(GrammaireParser::T__11);
      setState(158);
      match(GrammaireParser::T__3);
      setState(159);
      match(GrammaireParser::TYPE);
      setState(160);
      match(GrammaireParser::T__4);
      break;
    }

    case 6: {
      _localctx = _tracker.createInstance<ExprNegContext>(_localctx);
      _ctx = _localctx;
      previousContext = _localctx;
      setState(161);
      match(GrammaireParser::T__12);
      setState(162);
      expr(28);
      break;
    }

    case 7: {
      _localctx = _tracker.createInstance<ExprNegBinContext>(_localctx);
      _ctx = _localctx;
      previousContext = _localctx;
      setState(163);
      match(GrammaireParser::T__13);
      setState(164);
      expr(27);
      break;
    }

    case 8: {
      _localctx = _tracker.createInstance<ExprNonContext>(_localctx);
      _ctx = _localctx;
      previousContext = _localctx;
      setState(165);
      match(GrammaireParser::T__14);
      setState(166);
      expr(26);
      break;
    }

    case 9: {
      _localctx = _tracker.createInstance<ExprLincContext>(_localctx);
      _ctx = _localctx;
      previousContext = _localctx;
      setState(167);
      match(GrammaireParser::T__15);
      setState(168);
      lval();
      break;
    }

    case 10: {
      _localctx = _tracker.createInstance<ExprLdecContext>(_localctx);
      _ctx = _localctx;
      previousContext = _localctx;
      setState(169);
      match(GrammaireParser::T__16);
      setState(170);
      lval();
      break;
    }

    case 11: {
      _localctx = _tracker.createInstance<ExprDecContext>(_localctx);
      _ctx = _localctx;
      previousContext = _localctx;
      setState(171);
      lval();
      setState(172);
      match(GrammaireParser::T__16);
      break;
    }

    case 12: {
      _localctx = _tracker.createInstance<ExprIncContext>(_localctx);
      _ctx = _localctx;
      previousContext = _localctx;
      setState(174);
      lval();
      setState(175);
      match(GrammaireParser::T__15);
      break;
    }

    case 13: {
      _localctx = _tracker.createInstance<ExprCastContext>(_localctx);
      _ctx = _localctx;
      previousContext = _localctx;
      setState(177);
      match(GrammaireParser::T__3);
      setState(178);
      match(GrammaireParser::TYPE);
      setState(179);
      match(GrammaireParser::T__4);
      setState(180);
      expr(21);
      break;
    }

    case 14: {
      _localctx = _tracker.createInstance<ExprConstContext>(_localctx);
      _ctx = _localctx;
      previousContext = _localctx;
      setState(181);
      match(GrammaireParser::INT);
      break;
    }

    case 15: {
      _localctx = _tracker.createInstance<ExprVarContext>(_localctx);
      _ctx = _localctx;
      previousContext = _localctx;
      setState(182);
      lval();
      break;
    }

    case 16: {
      _localctx = _tracker.createInstance<ExprCharContext>(_localctx);
      _ctx = _localctx;
      previousContext = _localctx;
      setState(183);
      match(GrammaireParser::CHAR);
      break;
    }

    }
    _ctx->stop = _input->LT(-1);
    setState(242);
    _errHandler->sync(this);
    alt = getInterpreter<atn::ParserATNSimulator>()->adaptivePredict(_input, 13, _ctx);
    while (alt != 2 && alt != atn::ATN::INVALID_ALT_NUMBER) {
      if (alt == 1) {
        if (!_parseListeners.empty())
          triggerExitRuleEvent();
        previousContext = _localctx;
        setState(240);
        _errHandler->sync(this);
        switch (getInterpreter<atn::ParserATNSimulator>()->adaptivePredict(_input, 12, _ctx)) {
        case 1: {
          auto newContext = _tracker.createInstance<ExprMultDivContext>(_tracker.createInstance<ExprContext>(parentContext, parentState));
          _localctx = newContext;
          pushNewRecursionContext(newContext, startState, RuleExpr);
          setState(186);

          if (!(precpred(_ctx, 20))) throw FailedPredicateException(this, "precpred(_ctx, 20)");
          setState(187);
          dynamic_cast<ExprMultDivContext *>(_localctx)->op = _input->LT(1);
          _la = _input->LA(1);
          if (!(_la == GrammaireParser::T__17

          || _la == GrammaireParser::T__18)) {
            dynamic_cast<ExprMultDivContext *>(_localctx)->op = _errHandler->recoverInline(this);
          }
          else {
            _errHandler->reportMatch(this);
            consume();
          }
          setState(188);
          expr(21);
          break;
        }

        case 2: {
          auto newContext = _tracker.createInstance<ExprAddMoinsContext>(_tracker.createInstance<ExprContext>(parentContext, parentState));
          _localctx = newContext;
          pushNewRecursionContext(newContext, startState, RuleExpr);
          setState(189);

          if (!(precpred(_ctx, 19))) throw FailedPredicateException(this, "precpred(_ctx, 19)");
          setState(190);
          dynamic_cast<ExprAddMoinsContext *>(_localctx)->op = _input->LT(1);
          _la = _input->LA(1);
          if (!(_la == GrammaireParser::T__12

          || _la == GrammaireParser::T__19)) {
            dynamic_cast<ExprAddMoinsContext *>(_localctx)->op = _errHandler->recoverInline(this);
          }
          else {
            _errHandler->reportMatch(this);
            consume();
          }
          setState(191);
          expr(20);
          break;
        }

        case 3: {
          auto newContext = _tracker.createInstance<ExprDecLContext>(_tracker.createInstance<ExprContext>(parentContext, parentState));
          _localctx = newContext;
          pushNewRecursionContext(newContext, startState, RuleExpr);
          setState(192);

          if (!(precpred(_ctx, 18))) throw FailedPredicateException(this, "precpred(_ctx, 18)");
          setState(193);
          match(GrammaireParser::T__20);
          setState(194);
          expr(19);
          break;
        }

        case 4: {
          auto newContext = _tracker.createInstance<ExprDecRContext>(_tracker.createInstance<ExprContext>(parentContext, parentState));
          _localctx = newContext;
          pushNewRecursionContext(newContext, startState, RuleExpr);
          setState(195);

          if (!(precpred(_ctx, 17))) throw FailedPredicateException(this, "precpred(_ctx, 17)");
          setState(196);
          match(GrammaireParser::T__21);
          setState(197);
          expr(18);
          break;
        }

        case 5: {
          auto newContext = _tracker.createInstance<ExprModContext>(_tracker.createInstance<ExprContext>(parentContext, parentState));
          _localctx = newContext;
          pushNewRecursionContext(newContext, startState, RuleExpr);
          setState(198);

          if (!(precpred(_ctx, 16))) throw FailedPredicateException(this, "precpred(_ctx, 16)");
          setState(199);
          match(GrammaireParser::T__22);
          setState(200);
          expr(17);
          break;
        }

        case 6: {
          auto newContext = _tracker.createInstance<ExprEqContext>(_tracker.createInstance<ExprContext>(parentContext, parentState));
          _localctx = newContext;
          pushNewRecursionContext(newContext, startState, RuleExpr);
          setState(201);

          if (!(precpred(_ctx, 15))) throw FailedPredicateException(this, "precpred(_ctx, 15)");
          setState(202);
          match(GrammaireParser::T__23);
          setState(203);
          expr(16);
          break;
        }

        case 7: {
          auto newContext = _tracker.createInstance<ExprNeqContext>(_tracker.createInstance<ExprContext>(parentContext, parentState));
          _localctx = newContext;
          pushNewRecursionContext(newContext, startState, RuleExpr);
          setState(204);

          if (!(precpred(_ctx, 14))) throw FailedPredicateException(this, "precpred(_ctx, 14)");
          setState(205);
          match(GrammaireParser::T__24);
          setState(206);
          expr(15);
          break;
        }

        case 8: {
          auto newContext = _tracker.createInstance<ExprLtContext>(_tracker.createInstance<ExprContext>(parentContext, parentState));
          _localctx = newContext;
          pushNewRecursionContext(newContext, startState, RuleExpr);
          setState(207);

          if (!(precpred(_ctx, 13))) throw FailedPredicateException(this, "precpred(_ctx, 13)");
          setState(208);
          match(GrammaireParser::T__25);
          setState(209);
          expr(14);
          break;
        }

        case 9: {
          auto newContext = _tracker.createInstance<ExprGtContext>(_tracker.createInstance<ExprContext>(parentContext, parentState));
          _localctx = newContext;
          pushNewRecursionContext(newContext, startState, RuleExpr);
          setState(210);

          if (!(precpred(_ctx, 12))) throw FailedPredicateException(this, "precpred(_ctx, 12)");
          setState(211);
          match(GrammaireParser::T__26);
          setState(212);
          expr(13);
          break;
        }

        case 10: {
          auto newContext = _tracker.createInstance<ExprLeqContext>(_tracker.createInstance<ExprContext>(parentContext, parentState));
          _localctx = newContext;
          pushNewRecursionContext(newContext, startState, RuleExpr);
          setState(213);

          if (!(precpred(_ctx, 11))) throw FailedPredicateException(this, "precpred(_ctx, 11)");
          setState(214);
          match(GrammaireParser::T__27);
          setState(215);
          expr(12);
          break;
        }

        case 11: {
          auto newContext = _tracker.createInstance<ExprGeqContext>(_tracker.createInstance<ExprContext>(parentContext, parentState));
          _localctx = newContext;
          pushNewRecursionContext(newContext, startState, RuleExpr);
          setState(216);

          if (!(precpred(_ctx, 10))) throw FailedPredicateException(this, "precpred(_ctx, 10)");
          setState(217);
          match(GrammaireParser::T__28);
          setState(218);
          expr(11);
          break;
        }

        case 12: {
          auto newContext = _tracker.createInstance<ExprAndContext>(_tracker.createInstance<ExprContext>(parentContext, parentState));
          _localctx = newContext;
          pushNewRecursionContext(newContext, startState, RuleExpr);
          setState(219);

          if (!(precpred(_ctx, 9))) throw FailedPredicateException(this, "precpred(_ctx, 9)");
          setState(220);
          match(GrammaireParser::T__29);
          setState(221);
          expr(10);
          break;
        }

        case 13: {
          auto newContext = _tracker.createInstance<ExprAndBitContext>(_tracker.createInstance<ExprContext>(parentContext, parentState));
          _localctx = newContext;
          pushNewRecursionContext(newContext, startState, RuleExpr);
          setState(222);

          if (!(precpred(_ctx, 8))) throw FailedPredicateException(this, "precpred(_ctx, 8)");
          setState(223);
          match(GrammaireParser::T__30);
          setState(224);
          expr(9);
          break;
        }

        case 14: {
          auto newContext = _tracker.createInstance<ExprOrBitContext>(_tracker.createInstance<ExprContext>(parentContext, parentState));
          _localctx = newContext;
          pushNewRecursionContext(newContext, startState, RuleExpr);
          setState(225);

          if (!(precpred(_ctx, 7))) throw FailedPredicateException(this, "precpred(_ctx, 7)");
          setState(226);
          match(GrammaireParser::T__31);
          setState(227);
          expr(8);
          break;
        }

        case 15: {
          auto newContext = _tracker.createInstance<ExprXorBitContext>(_tracker.createInstance<ExprContext>(parentContext, parentState));
          _localctx = newContext;
          pushNewRecursionContext(newContext, startState, RuleExpr);
          setState(228);

          if (!(precpred(_ctx, 6))) throw FailedPredicateException(this, "precpred(_ctx, 6)");
          setState(229);
          match(GrammaireParser::T__32);
          setState(230);
          expr(7);
          break;
        }

        case 16: {
          auto newContext = _tracker.createInstance<ExprOrContext>(_tracker.createInstance<ExprContext>(parentContext, parentState));
          _localctx = newContext;
          pushNewRecursionContext(newContext, startState, RuleExpr);
          setState(231);

          if (!(precpred(_ctx, 5))) throw FailedPredicateException(this, "precpred(_ctx, 5)");
          setState(232);
          match(GrammaireParser::T__33);
          setState(233);
          expr(6);
          break;
        }

        case 17: {
          auto newContext = _tracker.createInstance<ExprTerContext>(_tracker.createInstance<ExprContext>(parentContext, parentState));
          _localctx = newContext;
          pushNewRecursionContext(newContext, startState, RuleExpr);
          setState(234);

          if (!(precpred(_ctx, 4))) throw FailedPredicateException(this, "precpred(_ctx, 4)");
          setState(235);
          match(GrammaireParser::T__34);
          setState(236);
          expr(0);
          setState(237);
          match(GrammaireParser::T__35);
          setState(238);
          expr(5);
          break;
        }

        } 
      }
      setState(244);
      _errHandler->sync(this);
      alt = getInterpreter<atn::ParserATNSimulator>()->adaptivePredict(_input, 13, _ctx);
    }
  }
  catch (RecognitionException &e) {
    _errHandler->reportError(this, e);
    _localctx->exception = std::current_exception();
    _errHandler->recover(this, _localctx->exception);
  }
  return _localctx;
}

//----------------- ControleContext ------------------------------------------------------------------

GrammaireParser::ControleContext::ControleContext(ParserRuleContext *parent, size_t invokingState)
  : ParserRuleContext(parent, invokingState) {
}


size_t GrammaireParser::ControleContext::getRuleIndex() const {
  return GrammaireParser::RuleControle;
}

void GrammaireParser::ControleContext::copyFrom(ControleContext *ctx) {
  ParserRuleContext::copyFrom(ctx);
}

//----------------- ControleWhileContext ------------------------------------------------------------------

GrammaireParser::ExprContext* GrammaireParser::ControleWhileContext::expr() {
  return getRuleContext<GrammaireParser::ExprContext>(0);
}

GrammaireParser::BlocContext* GrammaireParser::ControleWhileContext::bloc() {
  return getRuleContext<GrammaireParser::BlocContext>(0);
}

GrammaireParser::InstrContext* GrammaireParser::ControleWhileContext::instr() {
  return getRuleContext<GrammaireParser::InstrContext>(0);
}

GrammaireParser::ControleWhileContext::ControleWhileContext(ControleContext *ctx) { copyFrom(ctx); }

antlrcpp::Any GrammaireParser::ControleWhileContext::accept(tree::ParseTreeVisitor *visitor) {
  if (auto parserVisitor = dynamic_cast<GrammaireVisitor*>(visitor))
    return parserVisitor->visitControleWhile(this);
  else
    return visitor->visitChildren(this);
}
//----------------- ControleIfElseContext ------------------------------------------------------------------

GrammaireParser::ExprContext* GrammaireParser::ControleIfElseContext::expr() {
  return getRuleContext<GrammaireParser::ExprContext>(0);
}

std::vector<GrammaireParser::BlocContext *> GrammaireParser::ControleIfElseContext::bloc() {
  return getRuleContexts<GrammaireParser::BlocContext>();
}

GrammaireParser::BlocContext* GrammaireParser::ControleIfElseContext::bloc(size_t i) {
  return getRuleContext<GrammaireParser::BlocContext>(i);
}

std::vector<GrammaireParser::InstrContext *> GrammaireParser::ControleIfElseContext::instr() {
  return getRuleContexts<GrammaireParser::InstrContext>();
}

GrammaireParser::InstrContext* GrammaireParser::ControleIfElseContext::instr(size_t i) {
  return getRuleContext<GrammaireParser::InstrContext>(i);
}

GrammaireParser::ControleIfElseContext::ControleIfElseContext(ControleContext *ctx) { copyFrom(ctx); }

antlrcpp::Any GrammaireParser::ControleIfElseContext::accept(tree::ParseTreeVisitor *visitor) {
  if (auto parserVisitor = dynamic_cast<GrammaireVisitor*>(visitor))
    return parserVisitor->visitControleIfElse(this);
  else
    return visitor->visitChildren(this);
}
//----------------- ControleIfContext ------------------------------------------------------------------

GrammaireParser::ExprContext* GrammaireParser::ControleIfContext::expr() {
  return getRuleContext<GrammaireParser::ExprContext>(0);
}

GrammaireParser::BlocContext* GrammaireParser::ControleIfContext::bloc() {
  return getRuleContext<GrammaireParser::BlocContext>(0);
}

GrammaireParser::InstrContext* GrammaireParser::ControleIfContext::instr() {
  return getRuleContext<GrammaireParser::InstrContext>(0);
}

GrammaireParser::ControleIfContext::ControleIfContext(ControleContext *ctx) { copyFrom(ctx); }

antlrcpp::Any GrammaireParser::ControleIfContext::accept(tree::ParseTreeVisitor *visitor) {
  if (auto parserVisitor = dynamic_cast<GrammaireVisitor*>(visitor))
    return parserVisitor->visitControleIf(this);
  else
    return visitor->visitChildren(this);
}
GrammaireParser::ControleContext* GrammaireParser::controle() {
  ControleContext *_localctx = _tracker.createInstance<ControleContext>(_ctx, getState());
  enterRule(_localctx, 32, GrammaireParser::RuleControle);

  auto onExit = finally([=] {
    exitRule();
  });
  try {
    setState(301);
    _errHandler->sync(this);
    switch (getInterpreter<atn::ParserATNSimulator>()->adaptivePredict(_input, 14, _ctx)) {
    case 1: {
      _localctx = dynamic_cast<ControleContext *>(_tracker.createInstance<GrammaireParser::ControleWhileContext>(_localctx));
      enterOuterAlt(_localctx, 1);
      setState(245);
      match(GrammaireParser::T__36);
      setState(246);
      match(GrammaireParser::T__3);
      setState(247);
      expr(0);
      setState(248);
      match(GrammaireParser::T__4);
      setState(249);
      bloc();
      break;
    }

    case 2: {
      _localctx = dynamic_cast<ControleContext *>(_tracker.createInstance<GrammaireParser::ControleWhileContext>(_localctx));
      enterOuterAlt(_localctx, 2);
      setState(251);
      match(GrammaireParser::T__36);
      setState(252);
      match(GrammaireParser::T__3);
      setState(253);
      expr(0);
      setState(254);
      match(GrammaireParser::T__4);
      setState(255);
      instr();
      break;
    }

    case 3: {
      _localctx = dynamic_cast<ControleContext *>(_tracker.createInstance<GrammaireParser::ControleIfElseContext>(_localctx));
      enterOuterAlt(_localctx, 3);
      setState(257);
      match(GrammaireParser::T__37);
      setState(258);
      match(GrammaireParser::T__3);
      setState(259);
      expr(0);
      setState(260);
      match(GrammaireParser::T__4);
      setState(261);
      bloc();
      setState(262);
      match(GrammaireParser::T__38);
      setState(263);
      bloc();
      break;
    }

    case 4: {
      _localctx = dynamic_cast<ControleContext *>(_tracker.createInstance<GrammaireParser::ControleIfElseContext>(_localctx));
      enterOuterAlt(_localctx, 4);
      setState(265);
      match(GrammaireParser::T__37);
      setState(266);
      match(GrammaireParser::T__3);
      setState(267);
      expr(0);
      setState(268);
      match(GrammaireParser::T__4);
      setState(269);
      instr();
      setState(270);
      match(GrammaireParser::T__38);
      setState(271);
      instr();
      break;
    }

    case 5: {
      _localctx = dynamic_cast<ControleContext *>(_tracker.createInstance<GrammaireParser::ControleIfElseContext>(_localctx));
      enterOuterAlt(_localctx, 5);
      setState(273);
      match(GrammaireParser::T__37);
      setState(274);
      match(GrammaireParser::T__3);
      setState(275);
      expr(0);
      setState(276);
      match(GrammaireParser::T__4);
      setState(277);
      bloc();
      setState(278);
      match(GrammaireParser::T__38);
      setState(279);
      instr();
      break;
    }

    case 6: {
      _localctx = dynamic_cast<ControleContext *>(_tracker.createInstance<GrammaireParser::ControleIfElseContext>(_localctx));
      enterOuterAlt(_localctx, 6);
      setState(281);
      match(GrammaireParser::T__37);
      setState(282);
      match(GrammaireParser::T__3);
      setState(283);
      expr(0);
      setState(284);
      match(GrammaireParser::T__4);
      setState(285);
      instr();
      setState(286);
      match(GrammaireParser::T__38);
      setState(287);
      bloc();
      break;
    }

    case 7: {
      _localctx = dynamic_cast<ControleContext *>(_tracker.createInstance<GrammaireParser::ControleIfContext>(_localctx));
      enterOuterAlt(_localctx, 7);
      setState(289);
      match(GrammaireParser::T__37);
      setState(290);
      match(GrammaireParser::T__3);
      setState(291);
      expr(0);
      setState(292);
      match(GrammaireParser::T__4);
      setState(293);
      bloc();
      break;
    }

    case 8: {
      _localctx = dynamic_cast<ControleContext *>(_tracker.createInstance<GrammaireParser::ControleIfContext>(_localctx));
      enterOuterAlt(_localctx, 8);
      setState(295);
      match(GrammaireParser::T__37);
      setState(296);
      match(GrammaireParser::T__3);
      setState(297);
      expr(0);
      setState(298);
      match(GrammaireParser::T__4);
      setState(299);
      instr();
      break;
    }

    }
   
  }
  catch (RecognitionException &e) {
    _errHandler->reportError(this, e);
    _localctx->exception = std::current_exception();
    _errHandler->recover(this, _localctx->exception);
  }

  return _localctx;
}

//----------------- BlocContext ------------------------------------------------------------------

GrammaireParser::BlocContext::BlocContext(ParserRuleContext *parent, size_t invokingState)
  : ParserRuleContext(parent, invokingState) {
}

GrammaireParser::ContenuBlocContext* GrammaireParser::BlocContext::contenuBloc() {
  return getRuleContext<GrammaireParser::ContenuBlocContext>(0);
}


size_t GrammaireParser::BlocContext::getRuleIndex() const {
  return GrammaireParser::RuleBloc;
}

antlrcpp::Any GrammaireParser::BlocContext::accept(tree::ParseTreeVisitor *visitor) {
  if (auto parserVisitor = dynamic_cast<GrammaireVisitor*>(visitor))
    return parserVisitor->visitBloc(this);
  else
    return visitor->visitChildren(this);
}

GrammaireParser::BlocContext* GrammaireParser::bloc() {
  BlocContext *_localctx = _tracker.createInstance<BlocContext>(_ctx, getState());
  enterRule(_localctx, 34, GrammaireParser::RuleBloc);

  auto onExit = finally([=] {
    exitRule();
  });
  try {
    enterOuterAlt(_localctx, 1);
    setState(303);
    match(GrammaireParser::T__8);
    setState(304);
    contenuBloc();
    setState(305);
    match(GrammaireParser::T__9);
   
  }
  catch (RecognitionException &e) {
    _errHandler->reportError(this, e);
    _localctx->exception = std::current_exception();
    _errHandler->recover(this, _localctx->exception);
  }

  return _localctx;
}

//----------------- ContenuBlocContext ------------------------------------------------------------------

GrammaireParser::ContenuBlocContext::ContenuBlocContext(ParserRuleContext *parent, size_t invokingState)
  : ParserRuleContext(parent, invokingState) {
}

GrammaireParser::InstrContext* GrammaireParser::ContenuBlocContext::instr() {
  return getRuleContext<GrammaireParser::InstrContext>(0);
}

GrammaireParser::ContenuBlocContext* GrammaireParser::ContenuBlocContext::contenuBloc() {
  return getRuleContext<GrammaireParser::ContenuBlocContext>(0);
}

GrammaireParser::BlocContext* GrammaireParser::ContenuBlocContext::bloc() {
  return getRuleContext<GrammaireParser::BlocContext>(0);
}


size_t GrammaireParser::ContenuBlocContext::getRuleIndex() const {
  return GrammaireParser::RuleContenuBloc;
}

antlrcpp::Any GrammaireParser::ContenuBlocContext::accept(tree::ParseTreeVisitor *visitor) {
  if (auto parserVisitor = dynamic_cast<GrammaireVisitor*>(visitor))
    return parserVisitor->visitContenuBloc(this);
  else
    return visitor->visitChildren(this);
}

GrammaireParser::ContenuBlocContext* GrammaireParser::contenuBloc() {
  ContenuBlocContext *_localctx = _tracker.createInstance<ContenuBlocContext>(_ctx, getState());
  enterRule(_localctx, 36, GrammaireParser::RuleContenuBloc);

  auto onExit = finally([=] {
    exitRule();
  });
  try {
    setState(314);
    _errHandler->sync(this);
    switch (_input->LA(1)) {
      case GrammaireParser::T__3:
      case GrammaireParser::T__10:
      case GrammaireParser::T__11:
      case GrammaireParser::T__12:
      case GrammaireParser::T__13:
      case GrammaireParser::T__14:
      case GrammaireParser::T__15:
      case GrammaireParser::T__16:
      case GrammaireParser::T__36:
      case GrammaireParser::T__37:
      case GrammaireParser::TYPE:
      case GrammaireParser::VAR:
      case GrammaireParser::CHAR:
      case GrammaireParser::INT: {
        enterOuterAlt(_localctx, 1);
        setState(307);
        instr();
        setState(308);
        contenuBloc();
        break;
      }

      case GrammaireParser::T__8: {
        enterOuterAlt(_localctx, 2);
        setState(310);
        bloc();
        setState(311);
        contenuBloc();
        break;
      }

      case GrammaireParser::T__9: {
        enterOuterAlt(_localctx, 3);

        break;
      }

    default:
      throw NoViableAltException(this);
    }
   
  }
  catch (RecognitionException &e) {
    _errHandler->reportError(this, e);
    _localctx->exception = std::current_exception();
    _errHandler->recover(this, _localctx->exception);
  }

  return _localctx;
}

bool GrammaireParser::sempred(RuleContext *context, size_t ruleIndex, size_t predicateIndex) {
  switch (ruleIndex) {
    case 15: return exprSempred(dynamic_cast<ExprContext *>(context), predicateIndex);

  default:
    break;
  }
  return true;
}

bool GrammaireParser::exprSempred(ExprContext *_localctx, size_t predicateIndex) {
  switch (predicateIndex) {
    case 0: return precpred(_ctx, 20);
    case 1: return precpred(_ctx, 19);
    case 2: return precpred(_ctx, 18);
    case 3: return precpred(_ctx, 17);
    case 4: return precpred(_ctx, 16);
    case 5: return precpred(_ctx, 15);
    case 6: return precpred(_ctx, 14);
    case 7: return precpred(_ctx, 13);
    case 8: return precpred(_ctx, 12);
    case 9: return precpred(_ctx, 11);
    case 10: return precpred(_ctx, 10);
    case 11: return precpred(_ctx, 9);
    case 12: return precpred(_ctx, 8);
    case 13: return precpred(_ctx, 7);
    case 14: return precpred(_ctx, 6);
    case 15: return precpred(_ctx, 5);
    case 16: return precpred(_ctx, 4);

  default:
    break;
  }
  return true;
}

// Static vars and initialization.
std::vector<dfa::DFA> GrammaireParser::_decisionToDFA;
atn::PredictionContextCache GrammaireParser::_sharedContextCache;

// We own the ATN which in turn owns the ATN states.
atn::ATN GrammaireParser::_atn;
std::vector<uint16_t> GrammaireParser::_serializedATN;

std::vector<std::string> GrammaireParser::_ruleNames = {
  "progc", "prog", "paramV", "param", "fonction", "typeRet", "paramAppelV", 
  "paramAppel", "appelFonction", "lval", "elementTabV", "elementsTab", "decl", 
  "def", "instr", "expr", "controle", "bloc", "contenuBloc"
};

std::vector<std::string> GrammaireParser::_literalNames = {
  "", "','", "'['", "']'", "'('", "')'", "'void'", "';'", "'='", "'{'", 
  "'}'", "'return'", "'sizeof'", "'-'", "'~'", "'!'", "'++'", "'--'", "'*'", 
  "'/'", "'+'", "'<<'", "'>>'", "'%'", "'=='", "'!='", "'<'", "'>'", "'<='", 
  "'>='", "'&&'", "'&'", "'|'", "'$'", "'||'", "'?'", "':'", "'while'", 
  "'if'", "'else'"
};

std::vector<std::string> GrammaireParser::_symbolicNames = {
  "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", 
  "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", 
  "", "", "", "", "WS", "COMMENT_MULT", "COMMENT_LINE", "DP", "EOL", "TYPE", 
  "VAR", "CHAR", "INT"
};

dfa::Vocabulary GrammaireParser::_vocabulary(_literalNames, _symbolicNames);

std::vector<std::string> GrammaireParser::_tokenNames;

GrammaireParser::Initializer::Initializer() {
	for (size_t i = 0; i < _symbolicNames.size(); ++i) {
		std::string name = _vocabulary.getLiteralName(i);
		if (name.empty()) {
			name = _vocabulary.getSymbolicName(i);
		}

		if (name.empty()) {
			_tokenNames.push_back("<INVALID>");
		} else {
      _tokenNames.push_back(name);
    }
	}

  _serializedATN = {
    0x3, 0x608b, 0xa72a, 0x8133, 0xb9ed, 0x417c, 0x3be7, 0x7786, 0x5964, 
    0x3, 0x32, 0x13f, 0x4, 0x2, 0x9, 0x2, 0x4, 0x3, 0x9, 0x3, 0x4, 0x4, 
    0x9, 0x4, 0x4, 0x5, 0x9, 0x5, 0x4, 0x6, 0x9, 0x6, 0x4, 0x7, 0x9, 0x7, 
    0x4, 0x8, 0x9, 0x8, 0x4, 0x9, 0x9, 0x9, 0x4, 0xa, 0x9, 0xa, 0x4, 0xb, 
    0x9, 0xb, 0x4, 0xc, 0x9, 0xc, 0x4, 0xd, 0x9, 0xd, 0x4, 0xe, 0x9, 0xe, 
    0x4, 0xf, 0x9, 0xf, 0x4, 0x10, 0x9, 0x10, 0x4, 0x11, 0x9, 0x11, 0x4, 
    0x12, 0x9, 0x12, 0x4, 0x13, 0x9, 0x13, 0x4, 0x14, 0x9, 0x14, 0x3, 0x2, 
    0x3, 0x2, 0x3, 0x3, 0x3, 0x3, 0x3, 0x3, 0x3, 0x3, 0x3, 0x3, 0x3, 0x3, 
    0x3, 0x3, 0x5, 0x3, 0x32, 0xa, 0x3, 0x3, 0x4, 0x3, 0x4, 0x3, 0x4, 0x5, 
    0x4, 0x37, 0xa, 0x4, 0x3, 0x5, 0x3, 0x5, 0x3, 0x5, 0x3, 0x5, 0x3, 0x5, 
    0x3, 0x5, 0x3, 0x5, 0x3, 0x5, 0x3, 0x5, 0x5, 0x5, 0x42, 0xa, 0x5, 0x3, 
    0x6, 0x3, 0x6, 0x3, 0x6, 0x3, 0x6, 0x3, 0x6, 0x3, 0x6, 0x3, 0x6, 0x3, 
    0x7, 0x3, 0x7, 0x5, 0x7, 0x4d, 0xa, 0x7, 0x3, 0x8, 0x3, 0x8, 0x3, 0x8, 
    0x5, 0x8, 0x52, 0xa, 0x8, 0x3, 0x9, 0x3, 0x9, 0x3, 0x9, 0x3, 0x9, 0x5, 
    0x9, 0x58, 0xa, 0x9, 0x3, 0xa, 0x3, 0xa, 0x3, 0xa, 0x3, 0xa, 0x3, 0xa, 
    0x3, 0xb, 0x3, 0xb, 0x3, 0xb, 0x3, 0xb, 0x3, 0xb, 0x3, 0xb, 0x5, 0xb, 
    0x65, 0xa, 0xb, 0x3, 0xc, 0x3, 0xc, 0x3, 0xc, 0x3, 0xc, 0x3, 0xc, 0x5, 
    0xc, 0x6c, 0xa, 0xc, 0x3, 0xd, 0x3, 0xd, 0x3, 0xd, 0x3, 0xd, 0x5, 0xd, 
    0x72, 0xa, 0xd, 0x3, 0xe, 0x3, 0xe, 0x3, 0xe, 0x3, 0xe, 0x3, 0xe, 0x3, 
    0xe, 0x3, 0xe, 0x3, 0xe, 0x3, 0xe, 0x3, 0xe, 0x3, 0xe, 0x3, 0xe, 0x3, 
    0xe, 0x3, 0xe, 0x3, 0xe, 0x3, 0xe, 0x3, 0xe, 0x3, 0xe, 0x5, 0xe, 0x86, 
    0xa, 0xe, 0x3, 0xf, 0x3, 0xf, 0x3, 0xf, 0x3, 0xf, 0x3, 0x10, 0x3, 0x10, 
    0x3, 0x10, 0x3, 0x10, 0x3, 0x10, 0x3, 0x10, 0x3, 0x10, 0x3, 0x10, 0x3, 
    0x10, 0x5, 0x10, 0x95, 0xa, 0x10, 0x3, 0x11, 0x3, 0x11, 0x3, 0x11, 0x3, 
    0x11, 0x3, 0x11, 0x3, 0x11, 0x3, 0x11, 0x3, 0x11, 0x3, 0x11, 0x3, 0x11, 
    0x3, 0x11, 0x3, 0x11, 0x3, 0x11, 0x3, 0x11, 0x3, 0x11, 0x3, 0x11, 0x3, 
    0x11, 0x3, 0x11, 0x3, 0x11, 0x3, 0x11, 0x3, 0x11, 0x3, 0x11, 0x3, 0x11, 
    0x3, 0x11, 0x3, 0x11, 0x3, 0x11, 0x3, 0x11, 0x3, 0x11, 0x3, 0x11, 0x3, 
    0x11, 0x3, 0x11, 0x3, 0x11, 0x3, 0x11, 0x3, 0x11, 0x3, 0x11, 0x3, 0x11, 
    0x5, 0x11, 0xbb, 0xa, 0x11, 0x3, 0x11, 0x3, 0x11, 0x3, 0x11, 0x3, 0x11, 
    0x3, 0x11, 0x3, 0x11, 0x3, 0x11, 0x3, 0x11, 0x3, 0x11, 0x3, 0x11, 0x3, 
    0x11, 0x3, 0x11, 0x3, 0x11, 0x3, 0x11, 0x3, 0x11, 0x3, 0x11, 0x3, 0x11, 
    0x3, 0x11, 0x3, 0x11, 0x3, 0x11, 0x3, 0x11, 0x3, 0x11, 0x3, 0x11, 0x3, 
    0x11, 0x3, 0x11, 0x3, 0x11, 0x3, 0x11, 0x3, 0x11, 0x3, 0x11, 0x3, 0x11, 
    0x3, 0x11, 0x3, 0x11, 0x3, 0x11, 0x3, 0x11, 0x3, 0x11, 0x3, 0x11, 0x3, 
    0x11, 0x3, 0x11, 0x3, 0x11, 0x3, 0x11, 0x3, 0x11, 0x3, 0x11, 0x3, 0x11, 
    0x3, 0x11, 0x3, 0x11, 0x3, 0x11, 0x3, 0x11, 0x3, 0x11, 0x3, 0x11, 0x3, 
    0x11, 0x3, 0x11, 0x3, 0x11, 0x3, 0x11, 0x3, 0x11, 0x7, 0x11, 0xf3, 0xa, 
    0x11, 0xc, 0x11, 0xe, 0x11, 0xf6, 0xb, 0x11, 0x3, 0x12, 0x3, 0x12, 0x3, 
    0x12, 0x3, 0x12, 0x3, 0x12, 0x3, 0x12, 0x3, 0x12, 0x3, 0x12, 0x3, 0x12, 
    0x3, 0x12, 0x3, 0x12, 0x3, 0x12, 0x3, 0x12, 0x3, 0x12, 0x3, 0x12, 0x3, 
    0x12, 0x3, 0x12, 0x3, 0x12, 0x3, 0x12, 0x3, 0x12, 0x3, 0x12, 0x3, 0x12, 
    0x3, 0x12, 0x3, 0x12, 0x3, 0x12, 0x3, 0x12, 0x3, 0x12, 0x3, 0x12, 0x3, 
    0x12, 0x3, 0x12, 0x3, 0x12, 0x3, 0x12, 0x3, 0x12, 0x3, 0x12, 0x3, 0x12, 
    0x3, 0x12, 0x3, 0x12, 0x3, 0x12, 0x3, 0x12, 0x3, 0x12, 0x3, 0x12, 0x3, 
    0x12, 0x3, 0x12, 0x3, 0x12, 0x3, 0x12, 0x3, 0x12, 0x3, 0x12, 0x3, 0x12, 
    0x3, 0x12, 0x3, 0x12, 0x3, 0x12, 0x3, 0x12, 0x3, 0x12, 0x3, 0x12, 0x3, 
    0x12, 0x3, 0x12, 0x5, 0x12, 0x130, 0xa, 0x12, 0x3, 0x13, 0x3, 0x13, 
    0x3, 0x13, 0x3, 0x13, 0x3, 0x14, 0x3, 0x14, 0x3, 0x14, 0x3, 0x14, 0x3, 
    0x14, 0x3, 0x14, 0x3, 0x14, 0x5, 0x14, 0x13d, 0xa, 0x14, 0x3, 0x14, 
    0x2, 0x3, 0x20, 0x15, 0x2, 0x4, 0x6, 0x8, 0xa, 0xc, 0xe, 0x10, 0x12, 
    0x14, 0x16, 0x18, 0x1a, 0x1c, 0x1e, 0x20, 0x22, 0x24, 0x26, 0x2, 0x4, 
    0x3, 0x2, 0x14, 0x15, 0x4, 0x2, 0xf, 0xf, 0x16, 0x16, 0x2, 0x164, 0x2, 
    0x28, 0x3, 0x2, 0x2, 0x2, 0x4, 0x31, 0x3, 0x2, 0x2, 0x2, 0x6, 0x36, 
    0x3, 0x2, 0x2, 0x2, 0x8, 0x41, 0x3, 0x2, 0x2, 0x2, 0xa, 0x43, 0x3, 0x2, 
    0x2, 0x2, 0xc, 0x4c, 0x3, 0x2, 0x2, 0x2, 0xe, 0x51, 0x3, 0x2, 0x2, 0x2, 
    0x10, 0x57, 0x3, 0x2, 0x2, 0x2, 0x12, 0x59, 0x3, 0x2, 0x2, 0x2, 0x14, 
    0x64, 0x3, 0x2, 0x2, 0x2, 0x16, 0x6b, 0x3, 0x2, 0x2, 0x2, 0x18, 0x71, 
    0x3, 0x2, 0x2, 0x2, 0x1a, 0x85, 0x3, 0x2, 0x2, 0x2, 0x1c, 0x87, 0x3, 
    0x2, 0x2, 0x2, 0x1e, 0x94, 0x3, 0x2, 0x2, 0x2, 0x20, 0xba, 0x3, 0x2, 
    0x2, 0x2, 0x22, 0x12f, 0x3, 0x2, 0x2, 0x2, 0x24, 0x131, 0x3, 0x2, 0x2, 
    0x2, 0x26, 0x13c, 0x3, 0x2, 0x2, 0x2, 0x28, 0x29, 0x5, 0x4, 0x3, 0x2, 
    0x29, 0x3, 0x3, 0x2, 0x2, 0x2, 0x2a, 0x2b, 0x5, 0x1a, 0xe, 0x2, 0x2b, 
    0x2c, 0x5, 0x4, 0x3, 0x2, 0x2c, 0x32, 0x3, 0x2, 0x2, 0x2, 0x2d, 0x2e, 
    0x5, 0xa, 0x6, 0x2, 0x2e, 0x2f, 0x5, 0x4, 0x3, 0x2, 0x2f, 0x32, 0x3, 
    0x2, 0x2, 0x2, 0x30, 0x32, 0x7, 0x2, 0x2, 0x3, 0x31, 0x2a, 0x3, 0x2, 
    0x2, 0x2, 0x31, 0x2d, 0x3, 0x2, 0x2, 0x2, 0x31, 0x30, 0x3, 0x2, 0x2, 
    0x2, 0x32, 0x5, 0x3, 0x2, 0x2, 0x2, 0x33, 0x34, 0x7, 0x3, 0x2, 0x2, 
    0x34, 0x37, 0x5, 0x8, 0x5, 0x2, 0x35, 0x37, 0x3, 0x2, 0x2, 0x2, 0x36, 
    0x33, 0x3, 0x2, 0x2, 0x2, 0x36, 0x35, 0x3, 0x2, 0x2, 0x2, 0x37, 0x7, 
    0x3, 0x2, 0x2, 0x2, 0x38, 0x39, 0x7, 0x2f, 0x2, 0x2, 0x39, 0x3a, 0x7, 
    0x30, 0x2, 0x2, 0x3a, 0x3b, 0x7, 0x4, 0x2, 0x2, 0x3b, 0x3c, 0x7, 0x5, 
    0x2, 0x2, 0x3c, 0x42, 0x5, 0x6, 0x4, 0x2, 0x3d, 0x3e, 0x7, 0x2f, 0x2, 
    0x2, 0x3e, 0x3f, 0x7, 0x30, 0x2, 0x2, 0x3f, 0x42, 0x5, 0x6, 0x4, 0x2, 
    0x40, 0x42, 0x3, 0x2, 0x2, 0x2, 0x41, 0x38, 0x3, 0x2, 0x2, 0x2, 0x41, 
    0x3d, 0x3, 0x2, 0x2, 0x2, 0x41, 0x40, 0x3, 0x2, 0x2, 0x2, 0x42, 0x9, 
    0x3, 0x2, 0x2, 0x2, 0x43, 0x44, 0x5, 0xc, 0x7, 0x2, 0x44, 0x45, 0x7, 
    0x30, 0x2, 0x2, 0x45, 0x46, 0x7, 0x6, 0x2, 0x2, 0x46, 0x47, 0x5, 0x8, 
    0x5, 0x2, 0x47, 0x48, 0x7, 0x7, 0x2, 0x2, 0x48, 0x49, 0x5, 0x24, 0x13, 
    0x2, 0x49, 0xb, 0x3, 0x2, 0x2, 0x2, 0x4a, 0x4d, 0x7, 0x2f, 0x2, 0x2, 
    0x4b, 0x4d, 0x7, 0x8, 0x2, 0x2, 0x4c, 0x4a, 0x3, 0x2, 0x2, 0x2, 0x4c, 
    0x4b, 0x3, 0x2, 0x2, 0x2, 0x4d, 0xd, 0x3, 0x2, 0x2, 0x2, 0x4e, 0x4f, 
    0x7, 0x3, 0x2, 0x2, 0x4f, 0x52, 0x5, 0x10, 0x9, 0x2, 0x50, 0x52, 0x3, 
    0x2, 0x2, 0x2, 0x51, 0x4e, 0x3, 0x2, 0x2, 0x2, 0x51, 0x50, 0x3, 0x2, 
    0x2, 0x2, 0x52, 0xf, 0x3, 0x2, 0x2, 0x2, 0x53, 0x54, 0x5, 0x20, 0x11, 
    0x2, 0x54, 0x55, 0x5, 0xe, 0x8, 0x2, 0x55, 0x58, 0x3, 0x2, 0x2, 0x2, 
    0x56, 0x58, 0x3, 0x2, 0x2, 0x2, 0x57, 0x53, 0x3, 0x2, 0x2, 0x2, 0x57, 
    0x56, 0x3, 0x2, 0x2, 0x2, 0x58, 0x11, 0x3, 0x2, 0x2, 0x2, 0x59, 0x5a, 
    0x7, 0x30, 0x2, 0x2, 0x5a, 0x5b, 0x7, 0x6, 0x2, 0x2, 0x5b, 0x5c, 0x5, 
    0x10, 0x9, 0x2, 0x5c, 0x5d, 0x7, 0x7, 0x2, 0x2, 0x5d, 0x13, 0x3, 0x2, 
    0x2, 0x2, 0x5e, 0x65, 0x7, 0x30, 0x2, 0x2, 0x5f, 0x60, 0x7, 0x30, 0x2, 
    0x2, 0x60, 0x61, 0x7, 0x4, 0x2, 0x2, 0x61, 0x62, 0x5, 0x20, 0x11, 0x2, 
    0x62, 0x63, 0x7, 0x5, 0x2, 0x2, 0x63, 0x65, 0x3, 0x2, 0x2, 0x2, 0x64, 
    0x5e, 0x3, 0x2, 0x2, 0x2, 0x64, 0x5f, 0x3, 0x2, 0x2, 0x2, 0x65, 0x15, 
    0x3, 0x2, 0x2, 0x2, 0x66, 0x67, 0x7, 0x3, 0x2, 0x2, 0x67, 0x68, 0x5, 
    0x20, 0x11, 0x2, 0x68, 0x69, 0x5, 0x16, 0xc, 0x2, 0x69, 0x6c, 0x3, 0x2, 
    0x2, 0x2, 0x6a, 0x6c, 0x3, 0x2, 0x2, 0x2, 0x6b, 0x66, 0x3, 0x2, 0x2, 
    0x2, 0x6b, 0x6a, 0x3, 0x2, 0x2, 0x2, 0x6c, 0x17, 0x3, 0x2, 0x2, 0x2, 
    0x6d, 0x6e, 0x5, 0x20, 0x11, 0x2, 0x6e, 0x6f, 0x5, 0x16, 0xc, 0x2, 0x6f, 
    0x72, 0x3, 0x2, 0x2, 0x2, 0x70, 0x72, 0x3, 0x2, 0x2, 0x2, 0x71, 0x6d, 
    0x3, 0x2, 0x2, 0x2, 0x71, 0x70, 0x3, 0x2, 0x2, 0x2, 0x72, 0x19, 0x3, 
    0x2, 0x2, 0x2, 0x73, 0x74, 0x7, 0x2f, 0x2, 0x2, 0x74, 0x75, 0x5, 0x1c, 
    0xf, 0x2, 0x75, 0x76, 0x7, 0x9, 0x2, 0x2, 0x76, 0x86, 0x3, 0x2, 0x2, 
    0x2, 0x77, 0x78, 0x7, 0x2f, 0x2, 0x2, 0x78, 0x79, 0x5, 0x14, 0xb, 0x2, 
    0x79, 0x7a, 0x7, 0x9, 0x2, 0x2, 0x7a, 0x86, 0x3, 0x2, 0x2, 0x2, 0x7b, 
    0x7c, 0x7, 0x2f, 0x2, 0x2, 0x7c, 0x7d, 0x7, 0x30, 0x2, 0x2, 0x7d, 0x7e, 
    0x7, 0x4, 0x2, 0x2, 0x7e, 0x7f, 0x7, 0x5, 0x2, 0x2, 0x7f, 0x80, 0x7, 
    0xa, 0x2, 0x2, 0x80, 0x81, 0x7, 0xb, 0x2, 0x2, 0x81, 0x82, 0x5, 0x18, 
    0xd, 0x2, 0x82, 0x83, 0x7, 0xc, 0x2, 0x2, 0x83, 0x84, 0x7, 0x9, 0x2, 
    0x2, 0x84, 0x86, 0x3, 0x2, 0x2, 0x2, 0x85, 0x73, 0x3, 0x2, 0x2, 0x2, 
    0x85, 0x77, 0x3, 0x2, 0x2, 0x2, 0x85, 0x7b, 0x3, 0x2, 0x2, 0x2, 0x86, 
    0x1b, 0x3, 0x2, 0x2, 0x2, 0x87, 0x88, 0x5, 0x14, 0xb, 0x2, 0x88, 0x89, 
    0x7, 0xa, 0x2, 0x2, 0x89, 0x8a, 0x5, 0x20, 0x11, 0x2, 0x8a, 0x1d, 0x3, 
    0x2, 0x2, 0x2, 0x8b, 0x8c, 0x7, 0xd, 0x2, 0x2, 0x8c, 0x8d, 0x5, 0x20, 
    0x11, 0x2, 0x8d, 0x8e, 0x7, 0x9, 0x2, 0x2, 0x8e, 0x95, 0x3, 0x2, 0x2, 
    0x2, 0x8f, 0x90, 0x5, 0x20, 0x11, 0x2, 0x90, 0x91, 0x7, 0x9, 0x2, 0x2, 
    0x91, 0x95, 0x3, 0x2, 0x2, 0x2, 0x92, 0x95, 0x5, 0x1a, 0xe, 0x2, 0x93, 
    0x95, 0x5, 0x22, 0x12, 0x2, 0x94, 0x8b, 0x3, 0x2, 0x2, 0x2, 0x94, 0x8f, 
    0x3, 0x2, 0x2, 0x2, 0x94, 0x92, 0x3, 0x2, 0x2, 0x2, 0x94, 0x93, 0x3, 
    0x2, 0x2, 0x2, 0x95, 0x1f, 0x3, 0x2, 0x2, 0x2, 0x96, 0x97, 0x8, 0x11, 
    0x1, 0x2, 0x97, 0xbb, 0x5, 0x12, 0xa, 0x2, 0x98, 0xbb, 0x5, 0x1c, 0xf, 
    0x2, 0x99, 0x9a, 0x7, 0x6, 0x2, 0x2, 0x9a, 0x9b, 0x5, 0x20, 0x11, 0x2, 
    0x9b, 0x9c, 0x7, 0x7, 0x2, 0x2, 0x9c, 0xbb, 0x3, 0x2, 0x2, 0x2, 0x9d, 
    0x9e, 0x7, 0xe, 0x2, 0x2, 0x9e, 0xbb, 0x5, 0x20, 0x11, 0x20, 0x9f, 0xa0, 
    0x7, 0xe, 0x2, 0x2, 0xa0, 0xa1, 0x7, 0x6, 0x2, 0x2, 0xa1, 0xa2, 0x7, 
    0x2f, 0x2, 0x2, 0xa2, 0xbb, 0x7, 0x7, 0x2, 0x2, 0xa3, 0xa4, 0x7, 0xf, 
    0x2, 0x2, 0xa4, 0xbb, 0x5, 0x20, 0x11, 0x1e, 0xa5, 0xa6, 0x7, 0x10, 
    0x2, 0x2, 0xa6, 0xbb, 0x5, 0x20, 0x11, 0x1d, 0xa7, 0xa8, 0x7, 0x11, 
    0x2, 0x2, 0xa8, 0xbb, 0x5, 0x20, 0x11, 0x1c, 0xa9, 0xaa, 0x7, 0x12, 
    0x2, 0x2, 0xaa, 0xbb, 0x5, 0x14, 0xb, 0x2, 0xab, 0xac, 0x7, 0x13, 0x2, 
    0x2, 0xac, 0xbb, 0x5, 0x14, 0xb, 0x2, 0xad, 0xae, 0x5, 0x14, 0xb, 0x2, 
    0xae, 0xaf, 0x7, 0x13, 0x2, 0x2, 0xaf, 0xbb, 0x3, 0x2, 0x2, 0x2, 0xb0, 
    0xb1, 0x5, 0x14, 0xb, 0x2, 0xb1, 0xb2, 0x7, 0x12, 0x2, 0x2, 0xb2, 0xbb, 
    0x3, 0x2, 0x2, 0x2, 0xb3, 0xb4, 0x7, 0x6, 0x2, 0x2, 0xb4, 0xb5, 0x7, 
    0x2f, 0x2, 0x2, 0xb5, 0xb6, 0x7, 0x7, 0x2, 0x2, 0xb6, 0xbb, 0x5, 0x20, 
    0x11, 0x17, 0xb7, 0xbb, 0x7, 0x32, 0x2, 0x2, 0xb8, 0xbb, 0x5, 0x14, 
    0xb, 0x2, 0xb9, 0xbb, 0x7, 0x31, 0x2, 0x2, 0xba, 0x96, 0x3, 0x2, 0x2, 
    0x2, 0xba, 0x98, 0x3, 0x2, 0x2, 0x2, 0xba, 0x99, 0x3, 0x2, 0x2, 0x2, 
    0xba, 0x9d, 0x3, 0x2, 0x2, 0x2, 0xba, 0x9f, 0x3, 0x2, 0x2, 0x2, 0xba, 
    0xa3, 0x3, 0x2, 0x2, 0x2, 0xba, 0xa5, 0x3, 0x2, 0x2, 0x2, 0xba, 0xa7, 
    0x3, 0x2, 0x2, 0x2, 0xba, 0xa9, 0x3, 0x2, 0x2, 0x2, 0xba, 0xab, 0x3, 
    0x2, 0x2, 0x2, 0xba, 0xad, 0x3, 0x2, 0x2, 0x2, 0xba, 0xb0, 0x3, 0x2, 
    0x2, 0x2, 0xba, 0xb3, 0x3, 0x2, 0x2, 0x2, 0xba, 0xb7, 0x3, 0x2, 0x2, 
    0x2, 0xba, 0xb8, 0x3, 0x2, 0x2, 0x2, 0xba, 0xb9, 0x3, 0x2, 0x2, 0x2, 
    0xbb, 0xf4, 0x3, 0x2, 0x2, 0x2, 0xbc, 0xbd, 0xc, 0x16, 0x2, 0x2, 0xbd, 
    0xbe, 0x9, 0x2, 0x2, 0x2, 0xbe, 0xf3, 0x5, 0x20, 0x11, 0x17, 0xbf, 0xc0, 
    0xc, 0x15, 0x2, 0x2, 0xc0, 0xc1, 0x9, 0x3, 0x2, 0x2, 0xc1, 0xf3, 0x5, 
    0x20, 0x11, 0x16, 0xc2, 0xc3, 0xc, 0x14, 0x2, 0x2, 0xc3, 0xc4, 0x7, 
    0x17, 0x2, 0x2, 0xc4, 0xf3, 0x5, 0x20, 0x11, 0x15, 0xc5, 0xc6, 0xc, 
    0x13, 0x2, 0x2, 0xc6, 0xc7, 0x7, 0x18, 0x2, 0x2, 0xc7, 0xf3, 0x5, 0x20, 
    0x11, 0x14, 0xc8, 0xc9, 0xc, 0x12, 0x2, 0x2, 0xc9, 0xca, 0x7, 0x19, 
    0x2, 0x2, 0xca, 0xf3, 0x5, 0x20, 0x11, 0x13, 0xcb, 0xcc, 0xc, 0x11, 
    0x2, 0x2, 0xcc, 0xcd, 0x7, 0x1a, 0x2, 0x2, 0xcd, 0xf3, 0x5, 0x20, 0x11, 
    0x12, 0xce, 0xcf, 0xc, 0x10, 0x2, 0x2, 0xcf, 0xd0, 0x7, 0x1b, 0x2, 0x2, 
    0xd0, 0xf3, 0x5, 0x20, 0x11, 0x11, 0xd1, 0xd2, 0xc, 0xf, 0x2, 0x2, 0xd2, 
    0xd3, 0x7, 0x1c, 0x2, 0x2, 0xd3, 0xf3, 0x5, 0x20, 0x11, 0x10, 0xd4, 
    0xd5, 0xc, 0xe, 0x2, 0x2, 0xd5, 0xd6, 0x7, 0x1d, 0x2, 0x2, 0xd6, 0xf3, 
    0x5, 0x20, 0x11, 0xf, 0xd7, 0xd8, 0xc, 0xd, 0x2, 0x2, 0xd8, 0xd9, 0x7, 
    0x1e, 0x2, 0x2, 0xd9, 0xf3, 0x5, 0x20, 0x11, 0xe, 0xda, 0xdb, 0xc, 0xc, 
    0x2, 0x2, 0xdb, 0xdc, 0x7, 0x1f, 0x2, 0x2, 0xdc, 0xf3, 0x5, 0x20, 0x11, 
    0xd, 0xdd, 0xde, 0xc, 0xb, 0x2, 0x2, 0xde, 0xdf, 0x7, 0x20, 0x2, 0x2, 
    0xdf, 0xf3, 0x5, 0x20, 0x11, 0xc, 0xe0, 0xe1, 0xc, 0xa, 0x2, 0x2, 0xe1, 
    0xe2, 0x7, 0x21, 0x2, 0x2, 0xe2, 0xf3, 0x5, 0x20, 0x11, 0xb, 0xe3, 0xe4, 
    0xc, 0x9, 0x2, 0x2, 0xe4, 0xe5, 0x7, 0x22, 0x2, 0x2, 0xe5, 0xf3, 0x5, 
    0x20, 0x11, 0xa, 0xe6, 0xe7, 0xc, 0x8, 0x2, 0x2, 0xe7, 0xe8, 0x7, 0x23, 
    0x2, 0x2, 0xe8, 0xf3, 0x5, 0x20, 0x11, 0x9, 0xe9, 0xea, 0xc, 0x7, 0x2, 
    0x2, 0xea, 0xeb, 0x7, 0x24, 0x2, 0x2, 0xeb, 0xf3, 0x5, 0x20, 0x11, 0x8, 
    0xec, 0xed, 0xc, 0x6, 0x2, 0x2, 0xed, 0xee, 0x7, 0x25, 0x2, 0x2, 0xee, 
    0xef, 0x5, 0x20, 0x11, 0x2, 0xef, 0xf0, 0x7, 0x26, 0x2, 0x2, 0xf0, 0xf1, 
    0x5, 0x20, 0x11, 0x7, 0xf1, 0xf3, 0x3, 0x2, 0x2, 0x2, 0xf2, 0xbc, 0x3, 
    0x2, 0x2, 0x2, 0xf2, 0xbf, 0x3, 0x2, 0x2, 0x2, 0xf2, 0xc2, 0x3, 0x2, 
    0x2, 0x2, 0xf2, 0xc5, 0x3, 0x2, 0x2, 0x2, 0xf2, 0xc8, 0x3, 0x2, 0x2, 
    0x2, 0xf2, 0xcb, 0x3, 0x2, 0x2, 0x2, 0xf2, 0xce, 0x3, 0x2, 0x2, 0x2, 
    0xf2, 0xd1, 0x3, 0x2, 0x2, 0x2, 0xf2, 0xd4, 0x3, 0x2, 0x2, 0x2, 0xf2, 
    0xd7, 0x3, 0x2, 0x2, 0x2, 0xf2, 0xda, 0x3, 0x2, 0x2, 0x2, 0xf2, 0xdd, 
    0x3, 0x2, 0x2, 0x2, 0xf2, 0xe0, 0x3, 0x2, 0x2, 0x2, 0xf2, 0xe3, 0x3, 
    0x2, 0x2, 0x2, 0xf2, 0xe6, 0x3, 0x2, 0x2, 0x2, 0xf2, 0xe9, 0x3, 0x2, 
    0x2, 0x2, 0xf2, 0xec, 0x3, 0x2, 0x2, 0x2, 0xf3, 0xf6, 0x3, 0x2, 0x2, 
    0x2, 0xf4, 0xf2, 0x3, 0x2, 0x2, 0x2, 0xf4, 0xf5, 0x3, 0x2, 0x2, 0x2, 
    0xf5, 0x21, 0x3, 0x2, 0x2, 0x2, 0xf6, 0xf4, 0x3, 0x2, 0x2, 0x2, 0xf7, 
    0xf8, 0x7, 0x27, 0x2, 0x2, 0xf8, 0xf9, 0x7, 0x6, 0x2, 0x2, 0xf9, 0xfa, 
    0x5, 0x20, 0x11, 0x2, 0xfa, 0xfb, 0x7, 0x7, 0x2, 0x2, 0xfb, 0xfc, 0x5, 
    0x24, 0x13, 0x2, 0xfc, 0x130, 0x3, 0x2, 0x2, 0x2, 0xfd, 0xfe, 0x7, 0x27, 
    0x2, 0x2, 0xfe, 0xff, 0x7, 0x6, 0x2, 0x2, 0xff, 0x100, 0x5, 0x20, 0x11, 
    0x2, 0x100, 0x101, 0x7, 0x7, 0x2, 0x2, 0x101, 0x102, 0x5, 0x1e, 0x10, 
    0x2, 0x102, 0x130, 0x3, 0x2, 0x2, 0x2, 0x103, 0x104, 0x7, 0x28, 0x2, 
    0x2, 0x104, 0x105, 0x7, 0x6, 0x2, 0x2, 0x105, 0x106, 0x5, 0x20, 0x11, 
    0x2, 0x106, 0x107, 0x7, 0x7, 0x2, 0x2, 0x107, 0x108, 0x5, 0x24, 0x13, 
    0x2, 0x108, 0x109, 0x7, 0x29, 0x2, 0x2, 0x109, 0x10a, 0x5, 0x24, 0x13, 
    0x2, 0x10a, 0x130, 0x3, 0x2, 0x2, 0x2, 0x10b, 0x10c, 0x7, 0x28, 0x2, 
    0x2, 0x10c, 0x10d, 0x7, 0x6, 0x2, 0x2, 0x10d, 0x10e, 0x5, 0x20, 0x11, 
    0x2, 0x10e, 0x10f, 0x7, 0x7, 0x2, 0x2, 0x10f, 0x110, 0x5, 0x1e, 0x10, 
    0x2, 0x110, 0x111, 0x7, 0x29, 0x2, 0x2, 0x111, 0x112, 0x5, 0x1e, 0x10, 
    0x2, 0x112, 0x130, 0x3, 0x2, 0x2, 0x2, 0x113, 0x114, 0x7, 0x28, 0x2, 
    0x2, 0x114, 0x115, 0x7, 0x6, 0x2, 0x2, 0x115, 0x116, 0x5, 0x20, 0x11, 
    0x2, 0x116, 0x117, 0x7, 0x7, 0x2, 0x2, 0x117, 0x118, 0x5, 0x24, 0x13, 
    0x2, 0x118, 0x119, 0x7, 0x29, 0x2, 0x2, 0x119, 0x11a, 0x5, 0x1e, 0x10, 
    0x2, 0x11a, 0x130, 0x3, 0x2, 0x2, 0x2, 0x11b, 0x11c, 0x7, 0x28, 0x2, 
    0x2, 0x11c, 0x11d, 0x7, 0x6, 0x2, 0x2, 0x11d, 0x11e, 0x5, 0x20, 0x11, 
    0x2, 0x11e, 0x11f, 0x7, 0x7, 0x2, 0x2, 0x11f, 0x120, 0x5, 0x1e, 0x10, 
    0x2, 0x120, 0x121, 0x7, 0x29, 0x2, 0x2, 0x121, 0x122, 0x5, 0x24, 0x13, 
    0x2, 0x122, 0x130, 0x3, 0x2, 0x2, 0x2, 0x123, 0x124, 0x7, 0x28, 0x2, 
    0x2, 0x124, 0x125, 0x7, 0x6, 0x2, 0x2, 0x125, 0x126, 0x5, 0x20, 0x11, 
    0x2, 0x126, 0x127, 0x7, 0x7, 0x2, 0x2, 0x127, 0x128, 0x5, 0x24, 0x13, 
    0x2, 0x128, 0x130, 0x3, 0x2, 0x2, 0x2, 0x129, 0x12a, 0x7, 0x28, 0x2, 
    0x2, 0x12a, 0x12b, 0x7, 0x6, 0x2, 0x2, 0x12b, 0x12c, 0x5, 0x20, 0x11, 
    0x2, 0x12c, 0x12d, 0x7, 0x7, 0x2, 0x2, 0x12d, 0x12e, 0x5, 0x1e, 0x10, 
    0x2, 0x12e, 0x130, 0x3, 0x2, 0x2, 0x2, 0x12f, 0xf7, 0x3, 0x2, 0x2, 0x2, 
    0x12f, 0xfd, 0x3, 0x2, 0x2, 0x2, 0x12f, 0x103, 0x3, 0x2, 0x2, 0x2, 0x12f, 
    0x10b, 0x3, 0x2, 0x2, 0x2, 0x12f, 0x113, 0x3, 0x2, 0x2, 0x2, 0x12f, 
    0x11b, 0x3, 0x2, 0x2, 0x2, 0x12f, 0x123, 0x3, 0x2, 0x2, 0x2, 0x12f, 
    0x129, 0x3, 0x2, 0x2, 0x2, 0x130, 0x23, 0x3, 0x2, 0x2, 0x2, 0x131, 0x132, 
    0x7, 0xb, 0x2, 0x2, 0x132, 0x133, 0x5, 0x26, 0x14, 0x2, 0x133, 0x134, 
    0x7, 0xc, 0x2, 0x2, 0x134, 0x25, 0x3, 0x2, 0x2, 0x2, 0x135, 0x136, 0x5, 
    0x1e, 0x10, 0x2, 0x136, 0x137, 0x5, 0x26, 0x14, 0x2, 0x137, 0x13d, 0x3, 
    0x2, 0x2, 0x2, 0x138, 0x139, 0x5, 0x24, 0x13, 0x2, 0x139, 0x13a, 0x5, 
    0x26, 0x14, 0x2, 0x13a, 0x13d, 0x3, 0x2, 0x2, 0x2, 0x13b, 0x13d, 0x3, 
    0x2, 0x2, 0x2, 0x13c, 0x135, 0x3, 0x2, 0x2, 0x2, 0x13c, 0x138, 0x3, 
    0x2, 0x2, 0x2, 0x13c, 0x13b, 0x3, 0x2, 0x2, 0x2, 0x13d, 0x27, 0x3, 0x2, 
    0x2, 0x2, 0x12, 0x31, 0x36, 0x41, 0x4c, 0x51, 0x57, 0x64, 0x6b, 0x71, 
    0x85, 0x94, 0xba, 0xf2, 0xf4, 0x12f, 0x13c, 
  };

  atn::ATNDeserializer deserializer;
  _atn = deserializer.deserialize(_serializedATN);

  size_t count = _atn.getNumberOfDecisions();
  _decisionToDFA.reserve(count);
  for (size_t i = 0; i < count; i++) { 
    _decisionToDFA.emplace_back(_atn.getDecisionState(i), i);
  }
}

GrammaireParser::Initializer GrammaireParser::_init;
