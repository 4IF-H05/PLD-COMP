
// Generated from Grammaire.g4 by ANTLR 4.7.1

#pragma once


#include "antlr4-runtime.h"




class  GrammaireParser : public antlr4::Parser {
public:
  enum {
    T__0 = 1, T__1 = 2, T__2 = 3, T__3 = 4, T__4 = 5, T__5 = 6, T__6 = 7, 
    T__7 = 8, T__8 = 9, T__9 = 10, T__10 = 11, T__11 = 12, T__12 = 13, T__13 = 14, 
    T__14 = 15, T__15 = 16, T__16 = 17, T__17 = 18, T__18 = 19, T__19 = 20, 
    T__20 = 21, T__21 = 22, T__22 = 23, T__23 = 24, T__24 = 25, T__25 = 26, 
    T__26 = 27, T__27 = 28, T__28 = 29, T__29 = 30, T__30 = 31, T__31 = 32, 
    T__32 = 33, T__33 = 34, T__34 = 35, T__35 = 36, T__36 = 37, T__37 = 38, 
    T__38 = 39, WS = 40, COMMENT_MULT = 41, COMMENT_LINE = 42, DP = 43, 
    EOL = 44, TYPE = 45, VAR = 46, CHAR = 47, INT = 48
  };

  enum {
    RuleProgc = 0, RuleProg = 1, RuleParamV = 2, RuleParam = 3, RuleFonction = 4, 
    RuleTypeRet = 5, RuleParamAppelV = 6, RuleParamAppel = 7, RuleAppelFonction = 8, 
    RuleLval = 9, RuleElementTabV = 10, RuleElementsTab = 11, RuleDecl = 12, 
    RuleDef = 13, RuleInstr = 14, RuleExpr = 15, RuleControle = 16, RuleBloc = 17, 
    RuleContenuBloc = 18
  };

  GrammaireParser(antlr4::TokenStream *input);
  ~GrammaireParser();

  virtual std::string getGrammarFileName() const override;
  virtual const antlr4::atn::ATN& getATN() const override { return _atn; };
  virtual const std::vector<std::string>& getTokenNames() const override { return _tokenNames; }; // deprecated: use vocabulary instead.
  virtual const std::vector<std::string>& getRuleNames() const override;
  virtual antlr4::dfa::Vocabulary& getVocabulary() const override;


  class ProgcContext;
  class ProgContext;
  class ParamVContext;
  class ParamContext;
  class FonctionContext;
  class TypeRetContext;
  class ParamAppelVContext;
  class ParamAppelContext;
  class AppelFonctionContext;
  class LvalContext;
  class ElementTabVContext;
  class ElementsTabContext;
  class DeclContext;
  class DefContext;
  class InstrContext;
  class ExprContext;
  class ControleContext;
  class BlocContext;
  class ContenuBlocContext; 

  class  ProgcContext : public antlr4::ParserRuleContext {
  public:
    ProgcContext(antlr4::ParserRuleContext *parent, size_t invokingState);
    virtual size_t getRuleIndex() const override;
    ProgContext *prog();

    virtual antlrcpp::Any accept(antlr4::tree::ParseTreeVisitor *visitor) override;
   
  };

  ProgcContext* progc();

  class  ProgContext : public antlr4::ParserRuleContext {
  public:
    ProgContext(antlr4::ParserRuleContext *parent, size_t invokingState);
   
    ProgContext() : antlr4::ParserRuleContext() { }
    void copyFrom(ProgContext *context);
    using antlr4::ParserRuleContext::copyFrom;

    virtual size_t getRuleIndex() const override;

   
  };

  class  ProgEOFContext : public ProgContext {
  public:
    ProgEOFContext(ProgContext *ctx);

    antlr4::tree::TerminalNode *EOF();
    virtual antlrcpp::Any accept(antlr4::tree::ParseTreeVisitor *visitor) override;
  };

  class  ProgDeclContext : public ProgContext {
  public:
    ProgDeclContext(ProgContext *ctx);

    DeclContext *decl();
    ProgContext *prog();
    virtual antlrcpp::Any accept(antlr4::tree::ParseTreeVisitor *visitor) override;
  };

  class  ProgFuncContext : public ProgContext {
  public:
    ProgFuncContext(ProgContext *ctx);

    FonctionContext *fonction();
    ProgContext *prog();
    virtual antlrcpp::Any accept(antlr4::tree::ParseTreeVisitor *visitor) override;
  };

  ProgContext* prog();

  class  ParamVContext : public antlr4::ParserRuleContext {
  public:
    ParamVContext(antlr4::ParserRuleContext *parent, size_t invokingState);
    virtual size_t getRuleIndex() const override;
    ParamContext *param();

    virtual antlrcpp::Any accept(antlr4::tree::ParseTreeVisitor *visitor) override;
   
  };

  ParamVContext* paramV();

  class  ParamContext : public antlr4::ParserRuleContext {
  public:
    ParamContext(antlr4::ParserRuleContext *parent, size_t invokingState);
   
    ParamContext() : antlr4::ParserRuleContext() { }
    void copyFrom(ParamContext *context);
    using antlr4::ParserRuleContext::copyFrom;

    virtual size_t getRuleIndex() const override;

   
  };

  class  ParamSimpleContext : public ParamContext {
  public:
    ParamSimpleContext(ParamContext *ctx);

    antlr4::tree::TerminalNode *TYPE();
    antlr4::tree::TerminalNode *VAR();
    ParamVContext *paramV();
    virtual antlrcpp::Any accept(antlr4::tree::ParseTreeVisitor *visitor) override;
  };

  class  ParamVideContext : public ParamContext {
  public:
    ParamVideContext(ParamContext *ctx);

    virtual antlrcpp::Any accept(antlr4::tree::ParseTreeVisitor *visitor) override;
  };

  class  ParamTabContext : public ParamContext {
  public:
    ParamTabContext(ParamContext *ctx);

    antlr4::tree::TerminalNode *TYPE();
    antlr4::tree::TerminalNode *VAR();
    ParamVContext *paramV();
    virtual antlrcpp::Any accept(antlr4::tree::ParseTreeVisitor *visitor) override;
  };

  ParamContext* param();

  class  FonctionContext : public antlr4::ParserRuleContext {
  public:
    FonctionContext(antlr4::ParserRuleContext *parent, size_t invokingState);
    virtual size_t getRuleIndex() const override;
    TypeRetContext *typeRet();
    antlr4::tree::TerminalNode *VAR();
    ParamContext *param();
    BlocContext *bloc();

    virtual antlrcpp::Any accept(antlr4::tree::ParseTreeVisitor *visitor) override;
   
  };

  FonctionContext* fonction();

  class  TypeRetContext : public antlr4::ParserRuleContext {
  public:
    TypeRetContext(antlr4::ParserRuleContext *parent, size_t invokingState);
   
    TypeRetContext() : antlr4::ParserRuleContext() { }
    void copyFrom(TypeRetContext *context);
    using antlr4::ParserRuleContext::copyFrom;

    virtual size_t getRuleIndex() const override;

   
  };

  class  TypeRetVoidContext : public TypeRetContext {
  public:
    TypeRetVoidContext(TypeRetContext *ctx);

    virtual antlrcpp::Any accept(antlr4::tree::ParseTreeVisitor *visitor) override;
  };

  class  TypeRetTypeContext : public TypeRetContext {
  public:
    TypeRetTypeContext(TypeRetContext *ctx);

    antlr4::tree::TerminalNode *TYPE();
    virtual antlrcpp::Any accept(antlr4::tree::ParseTreeVisitor *visitor) override;
  };

  TypeRetContext* typeRet();

  class  ParamAppelVContext : public antlr4::ParserRuleContext {
  public:
    ParamAppelVContext(antlr4::ParserRuleContext *parent, size_t invokingState);
    virtual size_t getRuleIndex() const override;
    ParamAppelContext *paramAppel();

    virtual antlrcpp::Any accept(antlr4::tree::ParseTreeVisitor *visitor) override;
   
  };

  ParamAppelVContext* paramAppelV();

  class  ParamAppelContext : public antlr4::ParserRuleContext {
  public:
    ParamAppelContext(antlr4::ParserRuleContext *parent, size_t invokingState);
    virtual size_t getRuleIndex() const override;
    ExprContext *expr();
    ParamAppelVContext *paramAppelV();

    virtual antlrcpp::Any accept(antlr4::tree::ParseTreeVisitor *visitor) override;
   
  };

  ParamAppelContext* paramAppel();

  class  AppelFonctionContext : public antlr4::ParserRuleContext {
  public:
    AppelFonctionContext(antlr4::ParserRuleContext *parent, size_t invokingState);
    virtual size_t getRuleIndex() const override;
    antlr4::tree::TerminalNode *VAR();
    ParamAppelContext *paramAppel();

    virtual antlrcpp::Any accept(antlr4::tree::ParseTreeVisitor *visitor) override;
   
  };

  AppelFonctionContext* appelFonction();

  class  LvalContext : public antlr4::ParserRuleContext {
  public:
    LvalContext(antlr4::ParserRuleContext *parent, size_t invokingState);
   
    LvalContext() : antlr4::ParserRuleContext() { }
    void copyFrom(LvalContext *context);
    using antlr4::ParserRuleContext::copyFrom;

    virtual size_t getRuleIndex() const override;

   
  };

  class  LvalTabContext : public LvalContext {
  public:
    LvalTabContext(LvalContext *ctx);

    antlr4::tree::TerminalNode *VAR();
    ExprContext *expr();
    virtual antlrcpp::Any accept(antlr4::tree::ParseTreeVisitor *visitor) override;
  };

  class  LvalSimpleContext : public LvalContext {
  public:
    LvalSimpleContext(LvalContext *ctx);

    antlr4::tree::TerminalNode *VAR();
    virtual antlrcpp::Any accept(antlr4::tree::ParseTreeVisitor *visitor) override;
  };

  LvalContext* lval();

  class  ElementTabVContext : public antlr4::ParserRuleContext {
  public:
    ElementTabVContext(antlr4::ParserRuleContext *parent, size_t invokingState);
    virtual size_t getRuleIndex() const override;
    ExprContext *expr();
    ElementTabVContext *elementTabV();

    virtual antlrcpp::Any accept(antlr4::tree::ParseTreeVisitor *visitor) override;
   
  };

  ElementTabVContext* elementTabV();

  class  ElementsTabContext : public antlr4::ParserRuleContext {
  public:
    ElementsTabContext(antlr4::ParserRuleContext *parent, size_t invokingState);
    virtual size_t getRuleIndex() const override;
    ExprContext *expr();
    ElementTabVContext *elementTabV();

    virtual antlrcpp::Any accept(antlr4::tree::ParseTreeVisitor *visitor) override;
   
  };

  ElementsTabContext* elementsTab();

  class  DeclContext : public antlr4::ParserRuleContext {
  public:
    DeclContext(antlr4::ParserRuleContext *parent, size_t invokingState);
   
    DeclContext() : antlr4::ParserRuleContext() { }
    void copyFrom(DeclContext *context);
    using antlr4::ParserRuleContext::copyFrom;

    virtual size_t getRuleIndex() const override;

   
  };

  class  DeclTabDefContext : public DeclContext {
  public:
    DeclTabDefContext(DeclContext *ctx);

    antlr4::tree::TerminalNode *TYPE();
    antlr4::tree::TerminalNode *VAR();
    ElementsTabContext *elementsTab();
    virtual antlrcpp::Any accept(antlr4::tree::ParseTreeVisitor *visitor) override;
  };

  class  DeclLvalContext : public DeclContext {
  public:
    DeclLvalContext(DeclContext *ctx);

    antlr4::tree::TerminalNode *TYPE();
    LvalContext *lval();
    virtual antlrcpp::Any accept(antlr4::tree::ParseTreeVisitor *visitor) override;
  };

  class  DeclDefContext : public DeclContext {
  public:
    DeclDefContext(DeclContext *ctx);

    antlr4::tree::TerminalNode *TYPE();
    DefContext *def();
    virtual antlrcpp::Any accept(antlr4::tree::ParseTreeVisitor *visitor) override;
  };

  DeclContext* decl();

  class  DefContext : public antlr4::ParserRuleContext {
  public:
    DefContext(antlr4::ParserRuleContext *parent, size_t invokingState);
    virtual size_t getRuleIndex() const override;
    LvalContext *lval();
    ExprContext *expr();

    virtual antlrcpp::Any accept(antlr4::tree::ParseTreeVisitor *visitor) override;
   
  };

  DefContext* def();

  class  InstrContext : public antlr4::ParserRuleContext {
  public:
    InstrContext(antlr4::ParserRuleContext *parent, size_t invokingState);
   
    InstrContext() : antlr4::ParserRuleContext() { }
    void copyFrom(InstrContext *context);
    using antlr4::ParserRuleContext::copyFrom;

    virtual size_t getRuleIndex() const override;

   
  };

  class  InstrDeclContext : public InstrContext {
  public:
    InstrDeclContext(InstrContext *ctx);

    DeclContext *decl();
    virtual antlrcpp::Any accept(antlr4::tree::ParseTreeVisitor *visitor) override;
  };

  class  InstrControleContext : public InstrContext {
  public:
    InstrControleContext(InstrContext *ctx);

    ControleContext *controle();
    virtual antlrcpp::Any accept(antlr4::tree::ParseTreeVisitor *visitor) override;
  };

  class  InstrExpContext : public InstrContext {
  public:
    InstrExpContext(InstrContext *ctx);

    ExprContext *expr();
    virtual antlrcpp::Any accept(antlr4::tree::ParseTreeVisitor *visitor) override;
  };

  class  InstrReturnContext : public InstrContext {
  public:
    InstrReturnContext(InstrContext *ctx);

    ExprContext *expr();
    virtual antlrcpp::Any accept(antlr4::tree::ParseTreeVisitor *visitor) override;
  };

  InstrContext* instr();

  class  ExprContext : public antlr4::ParserRuleContext {
  public:
    ExprContext(antlr4::ParserRuleContext *parent, size_t invokingState);
   
    ExprContext() : antlr4::ParserRuleContext() { }
    void copyFrom(ExprContext *context);
    using antlr4::ParserRuleContext::copyFrom;

    virtual size_t getRuleIndex() const override;

   
  };

  class  ExprOrBitContext : public ExprContext {
  public:
    ExprOrBitContext(ExprContext *ctx);

    std::vector<ExprContext *> expr();
    ExprContext* expr(size_t i);
    virtual antlrcpp::Any accept(antlr4::tree::ParseTreeVisitor *visitor) override;
  };

  class  ExprLtContext : public ExprContext {
  public:
    ExprLtContext(ExprContext *ctx);

    std::vector<ExprContext *> expr();
    ExprContext* expr(size_t i);
    virtual antlrcpp::Any accept(antlr4::tree::ParseTreeVisitor *visitor) override;
  };

  class  ExprLdecContext : public ExprContext {
  public:
    ExprLdecContext(ExprContext *ctx);

    LvalContext *lval();
    virtual antlrcpp::Any accept(antlr4::tree::ParseTreeVisitor *visitor) override;
  };

  class  ExprAndContext : public ExprContext {
  public:
    ExprAndContext(ExprContext *ctx);

    std::vector<ExprContext *> expr();
    ExprContext* expr(size_t i);
    virtual antlrcpp::Any accept(antlr4::tree::ParseTreeVisitor *visitor) override;
  };

  class  ExprVarContext : public ExprContext {
  public:
    ExprVarContext(ExprContext *ctx);

    LvalContext *lval();
    virtual antlrcpp::Any accept(antlr4::tree::ParseTreeVisitor *visitor) override;
  };

  class  ExprNegBinContext : public ExprContext {
  public:
    ExprNegBinContext(ExprContext *ctx);

    ExprContext *expr();
    virtual antlrcpp::Any accept(antlr4::tree::ParseTreeVisitor *visitor) override;
  };

  class  ExprDecRContext : public ExprContext {
  public:
    ExprDecRContext(ExprContext *ctx);

    std::vector<ExprContext *> expr();
    ExprContext* expr(size_t i);
    virtual antlrcpp::Any accept(antlr4::tree::ParseTreeVisitor *visitor) override;
  };

  class  ExprMultDivContext : public ExprContext {
  public:
    ExprMultDivContext(ExprContext *ctx);

    antlr4::Token *op = nullptr;
    std::vector<ExprContext *> expr();
    ExprContext* expr(size_t i);
    virtual antlrcpp::Any accept(antlr4::tree::ParseTreeVisitor *visitor) override;
  };

  class  ExprFuncContext : public ExprContext {
  public:
    ExprFuncContext(ExprContext *ctx);

    AppelFonctionContext *appelFonction();
    virtual antlrcpp::Any accept(antlr4::tree::ParseTreeVisitor *visitor) override;
  };

  class  ExprLeqContext : public ExprContext {
  public:
    ExprLeqContext(ExprContext *ctx);

    std::vector<ExprContext *> expr();
    ExprContext* expr(size_t i);
    virtual antlrcpp::Any accept(antlr4::tree::ParseTreeVisitor *visitor) override;
  };

  class  ExprNegContext : public ExprContext {
  public:
    ExprNegContext(ExprContext *ctx);

    ExprContext *expr();
    virtual antlrcpp::Any accept(antlr4::tree::ParseTreeVisitor *visitor) override;
  };

  class  ExprLincContext : public ExprContext {
  public:
    ExprLincContext(ExprContext *ctx);

    LvalContext *lval();
    virtual antlrcpp::Any accept(antlr4::tree::ParseTreeVisitor *visitor) override;
  };

  class  ExprEqContext : public ExprContext {
  public:
    ExprEqContext(ExprContext *ctx);

    std::vector<ExprContext *> expr();
    ExprContext* expr(size_t i);
    virtual antlrcpp::Any accept(antlr4::tree::ParseTreeVisitor *visitor) override;
  };

  class  ExprNeqContext : public ExprContext {
  public:
    ExprNeqContext(ExprContext *ctx);

    std::vector<ExprContext *> expr();
    ExprContext* expr(size_t i);
    virtual antlrcpp::Any accept(antlr4::tree::ParseTreeVisitor *visitor) override;
  };

  class  ExprCharContext : public ExprContext {
  public:
    ExprCharContext(ExprContext *ctx);

    antlr4::tree::TerminalNode *CHAR();
    virtual antlrcpp::Any accept(antlr4::tree::ParseTreeVisitor *visitor) override;
  };

  class  ExprConstContext : public ExprContext {
  public:
    ExprConstContext(ExprContext *ctx);

    antlr4::tree::TerminalNode *INT();
    virtual antlrcpp::Any accept(antlr4::tree::ParseTreeVisitor *visitor) override;
  };

  class  ExprDecLContext : public ExprContext {
  public:
    ExprDecLContext(ExprContext *ctx);

    std::vector<ExprContext *> expr();
    ExprContext* expr(size_t i);
    virtual antlrcpp::Any accept(antlr4::tree::ParseTreeVisitor *visitor) override;
  };

  class  ExprXorBitContext : public ExprContext {
  public:
    ExprXorBitContext(ExprContext *ctx);

    std::vector<ExprContext *> expr();
    ExprContext* expr(size_t i);
    virtual antlrcpp::Any accept(antlr4::tree::ParseTreeVisitor *visitor) override;
  };

  class  ExprGtContext : public ExprContext {
  public:
    ExprGtContext(ExprContext *ctx);

    std::vector<ExprContext *> expr();
    ExprContext* expr(size_t i);
    virtual antlrcpp::Any accept(antlr4::tree::ParseTreeVisitor *visitor) override;
  };

  class  ExprIncContext : public ExprContext {
  public:
    ExprIncContext(ExprContext *ctx);

    LvalContext *lval();
    virtual antlrcpp::Any accept(antlr4::tree::ParseTreeVisitor *visitor) override;
  };

  class  ExprSizeEContext : public ExprContext {
  public:
    ExprSizeEContext(ExprContext *ctx);

    ExprContext *expr();
    virtual antlrcpp::Any accept(antlr4::tree::ParseTreeVisitor *visitor) override;
  };

  class  ExprParContext : public ExprContext {
  public:
    ExprParContext(ExprContext *ctx);

    ExprContext *expr();
    virtual antlrcpp::Any accept(antlr4::tree::ParseTreeVisitor *visitor) override;
  };

  class  ExprGeqContext : public ExprContext {
  public:
    ExprGeqContext(ExprContext *ctx);

    std::vector<ExprContext *> expr();
    ExprContext* expr(size_t i);
    virtual antlrcpp::Any accept(antlr4::tree::ParseTreeVisitor *visitor) override;
  };

  class  ExprCastContext : public ExprContext {
  public:
    ExprCastContext(ExprContext *ctx);

    antlr4::tree::TerminalNode *TYPE();
    ExprContext *expr();
    virtual antlrcpp::Any accept(antlr4::tree::ParseTreeVisitor *visitor) override;
  };

  class  ExprOrContext : public ExprContext {
  public:
    ExprOrContext(ExprContext *ctx);

    std::vector<ExprContext *> expr();
    ExprContext* expr(size_t i);
    virtual antlrcpp::Any accept(antlr4::tree::ParseTreeVisitor *visitor) override;
  };

  class  ExprAndBitContext : public ExprContext {
  public:
    ExprAndBitContext(ExprContext *ctx);

    std::vector<ExprContext *> expr();
    ExprContext* expr(size_t i);
    virtual antlrcpp::Any accept(antlr4::tree::ParseTreeVisitor *visitor) override;
  };

  class  ExprDecContext : public ExprContext {
  public:
    ExprDecContext(ExprContext *ctx);

    LvalContext *lval();
    virtual antlrcpp::Any accept(antlr4::tree::ParseTreeVisitor *visitor) override;
  };

  class  ExprTerContext : public ExprContext {
  public:
    ExprTerContext(ExprContext *ctx);

    std::vector<ExprContext *> expr();
    ExprContext* expr(size_t i);
    virtual antlrcpp::Any accept(antlr4::tree::ParseTreeVisitor *visitor) override;
  };

  class  ExprDefContext : public ExprContext {
  public:
    ExprDefContext(ExprContext *ctx);

    DefContext *def();
    virtual antlrcpp::Any accept(antlr4::tree::ParseTreeVisitor *visitor) override;
  };

  class  ExprAddMoinsContext : public ExprContext {
  public:
    ExprAddMoinsContext(ExprContext *ctx);

    antlr4::Token *op = nullptr;
    std::vector<ExprContext *> expr();
    ExprContext* expr(size_t i);
    virtual antlrcpp::Any accept(antlr4::tree::ParseTreeVisitor *visitor) override;
  };

  class  ExprModContext : public ExprContext {
  public:
    ExprModContext(ExprContext *ctx);

    std::vector<ExprContext *> expr();
    ExprContext* expr(size_t i);
    virtual antlrcpp::Any accept(antlr4::tree::ParseTreeVisitor *visitor) override;
  };

  class  ExprSizeTContext : public ExprContext {
  public:
    ExprSizeTContext(ExprContext *ctx);

    antlr4::tree::TerminalNode *TYPE();
    virtual antlrcpp::Any accept(antlr4::tree::ParseTreeVisitor *visitor) override;
  };

  class  ExprNonContext : public ExprContext {
  public:
    ExprNonContext(ExprContext *ctx);

    ExprContext *expr();
    virtual antlrcpp::Any accept(antlr4::tree::ParseTreeVisitor *visitor) override;
  };

  ExprContext* expr();
  ExprContext* expr(int precedence);
  class  ControleContext : public antlr4::ParserRuleContext {
  public:
    ControleContext(antlr4::ParserRuleContext *parent, size_t invokingState);
   
    ControleContext() : antlr4::ParserRuleContext() { }
    void copyFrom(ControleContext *context);
    using antlr4::ParserRuleContext::copyFrom;

    virtual size_t getRuleIndex() const override;

   
  };

  class  ControleWhileContext : public ControleContext {
  public:
    ControleWhileContext(ControleContext *ctx);

    ExprContext *expr();
    BlocContext *bloc();
    InstrContext *instr();
    virtual antlrcpp::Any accept(antlr4::tree::ParseTreeVisitor *visitor) override;
  };

  class  ControleIfElseContext : public ControleContext {
  public:
    ControleIfElseContext(ControleContext *ctx);

    ExprContext *expr();
    std::vector<BlocContext *> bloc();
    BlocContext* bloc(size_t i);
    std::vector<InstrContext *> instr();
    InstrContext* instr(size_t i);
    virtual antlrcpp::Any accept(antlr4::tree::ParseTreeVisitor *visitor) override;
  };

  class  ControleIfContext : public ControleContext {
  public:
    ControleIfContext(ControleContext *ctx);

    ExprContext *expr();
    BlocContext *bloc();
    InstrContext *instr();
    virtual antlrcpp::Any accept(antlr4::tree::ParseTreeVisitor *visitor) override;
  };

  ControleContext* controle();

  class  BlocContext : public antlr4::ParserRuleContext {
  public:
    BlocContext(antlr4::ParserRuleContext *parent, size_t invokingState);
    virtual size_t getRuleIndex() const override;
    ContenuBlocContext *contenuBloc();

    virtual antlrcpp::Any accept(antlr4::tree::ParseTreeVisitor *visitor) override;
   
  };

  BlocContext* bloc();

  class  ContenuBlocContext : public antlr4::ParserRuleContext {
  public:
    ContenuBlocContext(antlr4::ParserRuleContext *parent, size_t invokingState);
    virtual size_t getRuleIndex() const override;
    InstrContext *instr();
    ContenuBlocContext *contenuBloc();
    BlocContext *bloc();

    virtual antlrcpp::Any accept(antlr4::tree::ParseTreeVisitor *visitor) override;
   
  };

  ContenuBlocContext* contenuBloc();


  virtual bool sempred(antlr4::RuleContext *_localctx, size_t ruleIndex, size_t predicateIndex) override;
  bool exprSempred(ExprContext *_localctx, size_t predicateIndex);

private:
  static std::vector<antlr4::dfa::DFA> _decisionToDFA;
  static antlr4::atn::PredictionContextCache _sharedContextCache;
  static std::vector<std::string> _ruleNames;
  static std::vector<std::string> _tokenNames;

  static std::vector<std::string> _literalNames;
  static std::vector<std::string> _symbolicNames;
  static antlr4::dfa::Vocabulary _vocabulary;
  static antlr4::atn::ATN _atn;
  static std::vector<uint16_t> _serializedATN;


  struct Initializer {
    Initializer();
  };
  static Initializer _init;
};

