using namespace std;

#include <iostream>
#include <sstream>
#include <string>
#include <limits>
#include "StructureDonnees/Lvalue.h"
#include "StructureDonnees/Affectation.h"
#include "StructureDonnees/AppelFonction.h"
#include "StructureDonnees/VariableSimple.h"
#include "StructureDonnees/VariableTableau.h"
#include "StructureDonnees/Instruction.h"
#include "StructureDonnees/Bloc.h"
#include "StructureDonnees/Programme.h"
#include "StructureDonnees/Declaration.h"
#include "StructureDonnees/Fonction.h"
#include "StructureDonnees/ConstanteI32.h"
#include "StructureDonnees/ConstanteI64.h"
#include "StructureDonnees/ConstanteChar.h"
#include "StructureDonnees/ExpBinaire.h"
#include "StructureDonnees/ExpUnaire.h"
#include "StructureDonnees/ExpUnaireType.h"
#include "StructureDonnees/ExpTernaire.h"
#include "StructureDonnees/ExpCast.h"
#include "StructureDonnees/DeclarationTableau.h"
#include "StructureDonnees/If.h"
#include "StructureDonnees/Return.h"
#include "StructureDonnees/While.h"
#include "MainVisitor.h"
#include "../antlr/runtime_src/runtime/src/ParserRuleContext.h"
#include "../antlr/runtime_src/runtime/src/tree/TerminalNodeImpl.h"

/*

*----------------------*
|       Visiteurs      |
*----------------------*

*/

Any MainVisitor::visitProgc(GrammaireParser::ProgcContext *ctx) {
    Programme programme;
    vector<Any> shell = visitChildren(ctx); // Contient juste un "prog" (= coquille)
    vector<Any> enfants = shell[0];         // Contient le contenu dudit "prog" (les trucs intéressants)

    for (int i = 0; i < enfants.size(); i++) {
        if (isType<Fonction*>(enfants[i])) {
            Fonction *f = enfants[i];
            programme.addFonction(f);
        } else if (isType<Instruction*>(enfants[i])) {
			Instruction* inst = enfants[i];
			if (inst->isDeclaration ())
			{
				Declaration* d = static_cast<Declaration*>(inst);
				programme.addDeclaration (d);
			}
        } else {
            // Element non reconnu 
        }
    }

    return programme;
}

Any MainVisitor::visitProgDecl(GrammaireParser::ProgDeclContext *ctx) {
    vector<Any> enfants = visitChildren(ctx);
    enfants = unwrapVector(enfants);

    return enfants;
}

Any MainVisitor::visitProgFunc(GrammaireParser::ProgFuncContext *ctx) {
    vector<Any> enfants = visitChildren(ctx);
    enfants = unwrapVector(enfants);

    return enfants;
}

Any MainVisitor::visitProgEOF(GrammaireParser::ProgEOFContext *ctx) {
    return vector<Any>(); // L'équivalent de rien dans notre cas
}

Any MainVisitor::visitFonction(GrammaireParser::FonctionContext *ctx) {
    string nom(ctx->VAR()->getText());
    Fonction *f = new Fonction(nom);

    vector<Any> enfants = (visitChildren(ctx));
    //type de retour
    Type type;
    enfants = unwrapVector(enfants);
    for(int i = 0; i < enfants.size(); i++) {
        if (isType<Type>(enfants[i])) {
            type = enfants[i];
            f->setTypeRetour(type);
        } else if (isType<vector<Declaration*>>(enfants[i])) {
            vector<Declaration*> v = enfants[i];
            for(int j = 0; j < v.size(); j++) {
                if (isType<Declaration*>(v[j])) {
                    Declaration* dec = v[j];
                    f->addParam(dec);
                }
            }
        } else if (isType<Instruction*>(enfants[i])) {
            Instruction * corps = enfants[i];
            Bloc* corpsBloc = (Bloc*)corps;
            Bloc vraiCorps = *corpsBloc;
            f->setCorps(vraiCorps);
            // set le type de l'instruction return
            f->getCorps().setTypeRetourFonction(type);
        } else {
            cout << "Pas reconnu " << endl;
            // Element non reconnu 
        }
    }


    return f;
}

Any MainVisitor::visitDeclDef(GrammaireParser::DeclDefContext *ctx) {
    Type type = getType(string(ctx->TYPE()->getText()));
    vector<Any> enfants = visitChildren(ctx);
    enfants = unwrapVector(enfants);
    Expression *expr = enfants[0];
    Affectation *affect = dynamic_cast<Affectation *>(expr);
    Declaration *declaration = new Declaration(type, affect->getVariable()->getNomVariable(), affect->getValeur());
    delete affect->getVariable();
    delete affect;

    return (Instruction *) declaration;
}

Any MainVisitor::visitDeclLval(GrammaireParser::DeclLvalContext *ctx) {
    Type type = getType(string(ctx->TYPE()->getText()));
    vector<Any> enfants = visitChildren(ctx);
    enfants = unwrapVector(enfants);
	vector<Instruction*> vec;
	Expression *expr = (Expression*) enfants[0];
	VariableSimple *variableSimple = dynamic_cast<VariableSimple*>(expr);
	if (variableSimple != nullptr)
		return (Instruction*)new Declaration(type, variableSimple->getNomVariable());
	return(Instruction*) new DeclarationTableau(type, (VariableTableau*)expr, vector<Expression*>());
}


Any MainVisitor::visitDeclTabDef(GrammaireParser::DeclTabDefContext *ctx) {
    Type type = getType(string(ctx->TYPE()->getText()));
    vector<Any> enfants = visitChildren(ctx);
    enfants = unwrapVector(enfants);
    string a = ctx->VAR()->getText();
    VariableTableau *tableau = new VariableTableau(a, (Expression *) new ConstanteI64(enfants.size()));
    vector<Expression *> vecteur;
    for (auto it = enfants.cbegin(); it != enfants.cend(); ++it) {
        Any any = *it;
        vecteur.push_back((Expression *) any);
    }
    return (Instruction *) new DeclarationTableau(type, tableau, vecteur);
}

Any MainVisitor::visitElementTabV(GrammaireParser::ElementTabVContext *ctx) {
    vector<Any> v = visitChildren(ctx);
    v = unwrapVector(v);
    return v;
}

Any MainVisitor::visitElementsTab(GrammaireParser::ElementsTabContext *ctx) {
    vector<Any> v = visitChildren(ctx);
    v = unwrapVector(v);
    return v;
}

Any MainVisitor::visitTypeRetType(GrammaireParser::TypeRetTypeContext *ctx) {
    return getType(ctx->TYPE()->getText());
}

Any MainVisitor::visitTypeRetVoid(GrammaireParser::TypeRetVoidContext *ctx) {
    return VOID;
}

/*
*----------------------*
|	     BLOCS         |
*----------------------*
*/
Any MainVisitor::visitBloc(GrammaireParser::BlocContext *ctx) {
    vector<Any> enfants = visitChildren(ctx);
    string str = ctx->getText();
    enfants = unwrapVector(enfants);
    vector<Instruction *> instructions;
    for (auto it = enfants.cbegin(); it != enfants.cend(); ++it) {
        Any any = *it;
        if(isType<vector<Instruction*>>(any)) {
            vector<Instruction*> vecAny = any;
            for(auto it2 : vecAny) {
                Instruction * any2 = it2;
                instructions.push_back((Instruction *) any2);
            }
        }
        else
            instructions.push_back((Instruction *) any);
    }
    return (Instruction *) new Bloc(instructions);
}


Any MainVisitor::visitContenuBloc(GrammaireParser::ContenuBlocContext *ctx) {
    vector<Any> enfants = visitChildren(ctx);
    string str = ctx->getText();
    enfants = unwrapVector(enfants);

    return enfants;
}

Any MainVisitor::visitInstrReturn(GrammaireParser::InstrReturnContext *ctx) {
    string str = ctx->getText();
    vector<Any> enfants = visitChildren(ctx);
    enfants = unwrapVector(enfants);
    Expression *exp = enfants[0];
    return (Instruction *) new Return(exp);
}

Any MainVisitor::visitInstrExp(GrammaireParser::InstrExpContext *ctx) {
    string str = ctx->getText();
    vector<Any> enfants = visitChildren(ctx);
    enfants = unwrapVector(enfants);
    Expression *exp = enfants[0];
    return (Instruction *) exp;
}

Any MainVisitor::visitInstrDecl(GrammaireParser::InstrDeclContext *ctx) {
    string str = ctx->getText();
    vector<Any> enfants = visitChildren(ctx);
    enfants = unwrapVector(enfants);
    return (Instruction *) enfants[0];
}

Any MainVisitor::visitInstrControle(GrammaireParser::InstrControleContext *ctx) {
    string str = ctx->getText();
    vector<Any> enfants = visitChildren(ctx);
    enfants = unwrapVector(enfants);
    return (Instruction *) enfants[0];
}


/*
*----------------------*
|	   CONTROLES       |
*----------------------*
*/

Any MainVisitor::visitControleWhile(GrammaireParser::ControleWhileContext *ctx) {
    vector<Any> enfants = visitChildren(ctx);
    enfants = unwrapVector(enfants);
    Expression *clause = (Expression *) enfants[0];
    Instruction *instr = (Instruction *) enfants[1];
    return (Instruction *) new While(clause, instr);
}

Any MainVisitor::visitControleIf(GrammaireParser::ControleIfContext *ctx) {
    vector<Any> enfants = visitChildren(ctx);
    enfants = unwrapVector(enfants);
    Expression *clause = (Expression *) enfants[0];
    Instruction *instr = (Instruction *) enfants[1];
    return (Instruction *) new If(clause, instr);
}

Any MainVisitor::visitControleIfElse(GrammaireParser::ControleIfElseContext *ctx) {
    vector<Any> enfants = visitChildren(ctx);
    enfants = unwrapVector(enfants);
    Expression *clause = (Expression *) enfants[0];
    Instruction *instr = (Instruction *) enfants[1];
    Instruction *instrElse = (Instruction *) enfants[2];
    return (Instruction *) new If(clause, instr, instrElse);
}

/*
*------------------------*
|   APPELS DE FONCTION   |
*------------------------*
*/

Any MainVisitor::visitParamAppelV(GrammaireParser::ParamAppelVContext *ctx) {
    vector<Any> v = visitChildren(ctx);
    v = unwrapVector(v);
    return v;
}

Any MainVisitor::visitParamAppel(GrammaireParser::ParamAppelContext *ctx) {
    vector<Any> v = visitChildren(ctx);
    v = unwrapVector(v);
    return v;
}

Any MainVisitor::visitAppelFonction(GrammaireParser::AppelFonctionContext *ctx) {
    AppelFonction *appelFonction = new AppelFonction(ctx->VAR()->getText());
    vector<Any> v = visitChildren(ctx);

    v = unwrapVector(v);

    for (auto paramAppel : v) {
        appelFonction->addParam(paramAppel);
    }

    return (Expression *) appelFonction;
}

/*
*----------------------*
|	     PARAMS        |
*----------------------*
*/


Any MainVisitor::visitParamV(GrammaireParser::ParamVContext *ctx) {
    vector<Any> v = visitChildren(ctx);
    v = unwrapVector(v);
    return v;
}

Any MainVisitor::visitParamSimple(GrammaireParser::ParamSimpleContext *ctx) {
    Type type = getType(ctx->TYPE()->getText());
    string nom = ctx->VAR()->getText();
    Declaration* d = new Declaration(type, nom);
    vector<Declaration*> v;
    v.push_back((Declaration*)d);
    vector<Any> children = visitChildren(ctx);
    children = unwrapVector(children);
	for (auto it : children) {
		try {
			v.push_back((Declaration*)it);
		}
		catch (const std::bad_cast &e) {
			vector<Declaration*> declarations = it;
			for (auto it2 : declarations) {
				v.push_back((Declaration*)it2);
			}
		}
	}
    return v;
}

Any MainVisitor::visitParamTab(GrammaireParser::ParamTabContext *ctx) {

	Type type = getType(ctx->TYPE()->getText());
	string nom = ctx->VAR()->getText();
	Declaration* d = new DeclarationTableau(type, new VariableTableau(nom, nullptr));
	vector<Declaration*> v;
	v.push_back((Declaration*)d);
	vector<Any> children = visitChildren(ctx);
	children = unwrapVector(children);
	for (auto it : children) {
		try {
			v.push_back((Declaration*)it);
		}
		catch (const std::bad_cast &e) {
			vector<Declaration*> declarations = it;
			for (auto it2 : declarations) {
				v.push_back((Declaration*)it2);
			}
		}
	}
	return v;
}

Any MainVisitor::visitParamVide(GrammaireParser::ParamVideContext *ctx) {
    return vector<Any>();
}

/*
*----------------------*
|      DEFINITION      |
*----------------------*
*/


Any MainVisitor::visitDef(GrammaireParser::DefContext *ctx) {
    vector<Any> v = visitChildren(ctx);

    v = unwrapVector(v);

    Expression *lvalExpr = v[0];
    Expression *expr = v[1];

    Lvalue *lval = dynamic_cast<Lvalue *> (lvalExpr);

    Affectation *aff = new Affectation(lval, expr);

    return (Expression *) aff;
}

/*
*----------------------*
|      EXPRESSIONS     |
*----------------------*
*/

Any MainVisitor::visitExprAddMoins (GrammaireParser::ExprAddMoinsContext * ctx)
{
	Expression* exprGauche = visit (ctx->expr (0));
	Expression* exprDroite = visit (ctx->expr (1));
	Expression* ret;
	string op = ctx->op->getText ();
	if (op == "-")
	{
		ret = new ExpBinaire (exprGauche, MOINS, exprDroite);
	}
	else
	{
		ret = new ExpBinaire (exprGauche, PLUS, exprDroite);
	}
	return ret;
}

Any MainVisitor::visitExprMultDiv (GrammaireParser::ExprMultDivContext * ctx)
{
	Expression* exprGauche;
	Expression* exprDroite;
	try {
		exprGauche = visit(ctx->expr(0));
		exprDroite = visit(ctx->expr(1));
	}
	catch (const std::bad_cast &e) {
		vector<Any> left = visit(ctx->expr(0));
		vector<Any> right = visit(ctx->expr(1));
		exprGauche = left[0];
		exprDroite = right[0];
	}
	Expression* ret;
	string op = ctx->op->getText ();
	if (op == "/")
	{
		ret = new ExpBinaire (exprGauche, DIV, exprDroite);
	}
	else
	{
		ret = new ExpBinaire (exprGauche, MULT, exprDroite);
	}
	return ret;
}

Any MainVisitor::visitExprMod(GrammaireParser::ExprModContext *ctx) {
	Expression* exprGauche = visit (ctx->expr (0));
	Expression* exprDroite = visit (ctx->expr (1));
	return (Expression *) new ExpBinaire (exprGauche, MODULO, exprDroite);
}

Any MainVisitor::visitExprLt(GrammaireParser::ExprLtContext *ctx) {
	Expression* exprGauche = visit (ctx->expr (0));
	Expression* exprDroite = visit (ctx->expr (1));
	return (Expression *) new ExpBinaire (exprGauche, LT, exprDroite);
}

Any MainVisitor::visitExprLeq(GrammaireParser::ExprLeqContext *ctx) {
	Expression* exprGauche = visit (ctx->expr (0));
	Expression* exprDroite = visit (ctx->expr (1));
	return (Expression *) new ExpBinaire (exprGauche, LEQ, exprDroite);
}

Any MainVisitor::visitExprNeq (GrammaireParser::ExprNeqContext *ctx) {
	Expression* exprGauche = visit (ctx->expr (0));
	Expression* exprDroite = visit (ctx->expr (1));
	return (Expression *) new ExpBinaire (exprGauche, NEQ, exprDroite);
}

Any MainVisitor::visitExprEq(GrammaireParser::ExprEqContext *ctx) {

	Expression* exprGauche = visit (ctx->expr (0));
	Expression* exprDroite = visit (ctx->expr (1));
	return (Expression *) new ExpBinaire (exprGauche, EQ, exprDroite);
}

Any MainVisitor::visitExprGt(GrammaireParser::ExprGtContext *ctx) {
	Expression* exprGauche = visit (ctx->expr (0));
	Expression* exprDroite = visit (ctx->expr (1));
	return (Expression *) new ExpBinaire (exprGauche, GT, exprDroite);
}

Any MainVisitor::visitExprGeq(GrammaireParser::ExprGeqContext *ctx) {
	Expression* exprGauche = visit (ctx->expr (0));
	Expression* exprDroite = visit (ctx->expr (1));
	return (Expression *) new ExpBinaire (exprGauche, GEQ, exprDroite);
}

Any MainVisitor::visitExprOrBit(GrammaireParser::ExprOrBitContext *ctx) {
	Expression* exprGauche = visit (ctx->expr (0));
	Expression* exprDroite = visit (ctx->expr (1));
	return (Expression *) new ExpBinaire (exprGauche, ORB, exprDroite);
}
Any MainVisitor::visitExprXorBit(GrammaireParser::ExprXorBitContext *ctx) {
	Expression* exprGauche = visit (ctx->expr (0));
	Expression* exprDroite = visit (ctx->expr (1));
	return (Expression *) new ExpBinaire (exprGauche, XORB, exprDroite);
}

Any MainVisitor::visitExprAnd(GrammaireParser::ExprAndContext *ctx) {
	Expression* exprGauche = visit (ctx->expr (0));
	Expression* exprDroite = visit (ctx->expr (1));
	return (Expression *) new ExpBinaire (exprGauche, AND, exprDroite);
}

Any MainVisitor::visitExprOr(GrammaireParser::ExprOrContext *ctx) {
	Expression* exprGauche = visit (ctx->expr (0));
	Expression* exprDroite = visit (ctx->expr (1));
	return (Expression *) new ExpBinaire (exprGauche, OR, exprDroite);
}

Any MainVisitor::visitExprAndBit(GrammaireParser::ExprAndBitContext *ctx) {
	Expression* exprGauche = visit (ctx->expr (0));
	Expression* exprDroite = visit (ctx->expr (1));
	return (Expression *) new ExpBinaire (exprGauche, ANDB, exprDroite);
}

Any MainVisitor::visitExprDec(GrammaireParser::ExprDecContext *ctx) {
    return (Expression *) getExpressionUnaire(DEC, ctx);
}

Any MainVisitor::visitExprInc(GrammaireParser::ExprIncContext *ctx) {
    return (Expression *) getExpressionUnaire(INC, ctx);
}

Any MainVisitor::visitExprSizeE(GrammaireParser::ExprSizeEContext *ctx) {
    return (Expression *) getExpressionUnaire(SIZEE, ctx);
}

Any MainVisitor::visitExprSizeT(GrammaireParser::ExprSizeTContext *ctx) {
    return (Expression *) getSizeOfType(SIZET, ctx);
}

Any MainVisitor::visitExprNeg(GrammaireParser::ExprNegContext *ctx) {
    return (Expression *) getExpressionUnaire(NEG, ctx);
}

Any MainVisitor::visitExprNegBin(GrammaireParser::ExprNegBinContext *ctx) {
    return (Expression *) getExpressionUnaire(NEGBIN, ctx);
}

Any MainVisitor::visitExprNon(GrammaireParser::ExprNonContext *ctx) {
    return (Expression *) getExpressionUnaire(NON, ctx);
}

Any MainVisitor::visitExprLinc(GrammaireParser::ExprLincContext *ctx) {
    return (Expression *) getExpressionUnaire(LINC, ctx);
}

Any MainVisitor::visitExprLdec(GrammaireParser::ExprLdecContext *ctx) {
    return (Expression *) getExpressionUnaire(LDEC, ctx);
}

Any MainVisitor::visitExprTer(GrammaireParser::ExprTerContext *ctx) {
    vector<Any> enfants = visitChildren(ctx);
    enfants = unwrapVector(enfants);
    return (Expression *) new ExpTernaire(enfants[0], enfants[1], enfants[2]);
}

Any MainVisitor::visitExprPar(GrammaireParser::ExprParContext *ctx) {
    vector<Any> enfants = visitChildren(ctx);
    enfants = unwrapVector(enfants);
    return enfants[0];
}

Any MainVisitor::visitExprCast(GrammaireParser::ExprCastContext *ctx) {
    vector<Any> enfants = visitChildren(ctx);
    enfants = unwrapVector(enfants);
    return (Expression *) new ExpCast(enfants[0], getType(getTypeNamePar(ctx)));
}

Any MainVisitor::visitExprConst(GrammaireParser::ExprConstContext *ctx) {
    int64_t integer = stoll(ctx->INT()->getText());
    return (Expression *) new ConstanteI64(integer);
}

Any MainVisitor::visitExprChar(GrammaireParser::ExprCharContext *ctx) {
    string source = ctx->CHAR()->getText();
    char text = source[1];
    if(text == '\\') {
        switch (source[2]) {
            case 'r' :
                text = '\r';
                break;
            case 'n' :
                text = '\n';
                break;
            case 't' :
                text = '\t';
                break;
            case '\'' :
                text = '\'';
                break;
        }
    }

    return (Expression *) new ConstanteChar(text);
}

Any MainVisitor::visitExprFunc(GrammaireParser::ExprFuncContext *ctx) {
    vector<Any> enfants = visitChildren(ctx);
    enfants = unwrapVector(enfants);
    return enfants[0];
}

Any MainVisitor::visitExprDef(GrammaireParser::ExprDefContext *ctx) {
    vector<Any> v = visitChildren(ctx);
    Expression *expr = v[0];
    return expr;
}

Any MainVisitor::visitExprVar(GrammaireParser::ExprVarContext *ctx) {
    return visit(ctx->lval());
}



Any MainVisitor::visitLvalSimple(GrammaireParser::LvalSimpleContext *ctx) {
    string nom(ctx->VAR()->getText());
    return (Expression *) new VariableSimple(nom);
}

Any MainVisitor::visitLvalTab(GrammaireParser::LvalTabContext *ctx) {
    string nom(ctx->VAR()->getText());
    vector<Any> enfants = visitChildren(ctx);
    enfants = unwrapVector(enfants);
    return (Expression *) new VariableTableau(nom, (Expression *) enfants[0]);
}

/*
vector<Any> enfants = visitChildren(ctx);
enfants = unwrapVector(enfants);
Any a1 = enfants[0];
Any a2 = enfants[0];*/
/*
*----------------------*
|   Surcharges utiles  |
*----------------------*
*/

Any MainVisitor::defaultResult() {
    return vector<Any>();
}

Any MainVisitor::aggregateResult(Any aggregate, const Any &nextResult) {
    // Tous ces casts sont ridicules mais dans l'immédiat je sais pas comment faire autrement
    vector<Any> tmp = aggregate;
    tmp.push_back(nextResult);
    aggregate = tmp;

    return aggregate;
}

/*

*----------------------*
|   Fonctions random   |
*----------------------*

*/

vector<Any> MainVisitor::unwrapVector(vector<Any> &v) {
    int len = v.size();

    if (len <= 1) {
        return v;
    }

    vector<Any> result;
    for (int i = 0; i < len; i++) {
        if (isType<vector<Any>>(v[i])) {
            vector<Any> vv = v[i];
            for (int j = 0; j < vv.size(); j++) {
                result.push_back(vv[j]);
            }
        } else {
            result.push_back(v[i]);
        }
    }

    return result;
}

template<typename T>
bool MainVisitor::isType(Any a) {
    try { // C'est toujours un peu sale, mais à un seul endroit au moins
        T check = a;
        return true;
    } catch (const bad_cast &e) {
        return false;
    }

    return false;
}

Type MainVisitor::getType(string text) {
    for (int i = 0; i < (sizeof(Types) / sizeof(*Types)); i++) {
        if (Types[i] == text) {
            return Type(i);
        }
    }
}

pair<Expression *, Expression *> MainVisitor::getOperandesBinaires(vector<Any> &vect) {
    vect = unwrapVector(vect);
    return make_pair(vect[0], vect[1]);
}

Expression *MainVisitor::getExpressionUnaire(OpUnaire op, GrammaireParser::ExprContext *ctx) {
    vector<Any> enfants = visitChildren(ctx);
    Expression *exp = (Expression *) unwrapVector(enfants)[0];
    return (Expression *) new ExpUnaire(op, exp);
}

//amis de la qualité logicielle, au revoir.
string MainVisitor::getTypeNamePar(antlr4::ParserRuleContext *ctx) {
    string typeStr = ((ctx)->getText()).substr((ctx->getText()).find('(') + 1, ((ctx)->getText()).size());
    return typeStr.substr(0, typeStr.find(')'));
}

Expression *MainVisitor::getSizeOfType(OpUnaire op, GrammaireParser::ExprContext *ctx) {

    // pour les expressions unaires ciblant des types => sizeof(TYPE)
    // oui c'est très sale mais cette règle n'est que valable pour sizeof(type) !
    //(je n'arrive pas à récupérer le texte source en descendant plus bas dans l'arbre)
    return (Expression *) new ExpUnaireType(op, getType(getTypeNamePar(ctx)));
}


