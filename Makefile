.DEFAULT_GOAL := help
.PHONY: help antlr

help:		## Affiche l'aide
	@fgrep -h "##" $(MAKEFILE_LIST) | fgrep -v fgrep | sed -e 's/\\$$//' | sed -e 's/##//'

build:		## Lance antlr, cmake puis make dans le dossier src
build: antlr cmake

antlr:		## Lance antlr dans le dossier src
	cd src; \
	java -Xmx500M -cp "../antlr/antlr-4.7.1-complete.jar:$CLASSPATH" org.antlr.v4.Tool -visitor -no-listener -Dlanguage=Cpp Grammaire.g4

cmake:		## Lance cmake puis make dans le dossier src
	cd src; \
	./cmake.sh
	
genasm:		## Génère le fichier 'testInput.s' avec notre compilateur
	./bin/compilatif -c testInput.c

link:		## Génère et lance l'exécutable à partir de 'testInput.s'
	as -o testInput.o testInput.s
	 # L'option -no-pie ne devrait pas être nécessaire 
	 # → voir sur les PC d'IF comment ça se passe 
	gcc testInput.o -no-pie
	./a.out

exec_o:		## Lance notre compilateur avec les options -v et -o, assemble et link le fichier généré
	./bin/compilatif -a -c -v -o && as -o testInput.o testInput.s && gcc testInput.o -no-pie

exec:		## Lance notre compilateur, assemble et link le fichier généré
	./bin/compilatif -a -c && as -o testInput.o testInput.s && gcc testInput.o -no-pie

clean:		## Nettoye le dossier src des différents fichiers compilés
	rm src/Makefile
	rm src/CMakeCache.txt
	rm src/cmake_install.cmake
	rm -r src/dist
	rm -r src/CMakeFiles